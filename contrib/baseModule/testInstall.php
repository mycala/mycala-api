<?php

/**
 * hook_testInstall()
 */
function baseModule_testInstall()
{

    # All tests
    $allTests = array();

    #
    # Lets run the tests
    #

	# Run a test like checking if a directory exists.
	# You can see this function in the modules/cala/testInstall.php file
	# for ideas on how to do this.

    # Each test will return
    $allTests['testName'] = array(
        'name' => 'Name or description if you want to',
        'result' => 'true', //true | false
        'good' => 'Commentary if test was good/passed'
        'fail' => 'Commentary if test was bad/failed'
    );

    return $allTests;

}

