/**
 * Locale en_UK
 */
var _l = {

	/******************************
	 * Cala
	 */
	cala_online: 'En Línea/Global',
	cala_allCountries: 'En Línea / Global',
	cala_other: 'Otro',
	cala_working: 'Trabajando...',
	cala_alert: '¡Alerta!',
	cala_brilliant: 'Brillante ;)',
	cala_err: 'Err!',
	cala_warning: '¬Alerta!',
	cala_userPwdSent: 'Favor revisar su correo electrónico para la clave.. <br /> Recuerde revisar en el <strong>Correo no deseado</strong> por si acaso.',
	cala_userPwdSentErr: 'Se produjo un error, favor revise su dirección de coreo y el nombre de usuario.',
	users_regUserExists: 'Este correo o nombre de usuario ya está tomado. Intente con uno nuevo.',
	users_regFillAll: 'SE produjo un error, favor revise todos los campos requeridos e intente nuevamente.',

	/******************************
	 * Manager
	 */
	manager_nonlisted: 'Otro / No-Listado',
	manager_other: 'Otro/No-Listado',

	manager_editGood: 'Modificaciones listas',

	manager_errImageUploadFormat: 'Solo se permiten imágenes jpg',
	manager_errFileTooBig: 'El archivo es muy grande',

	k_other: 'Oro/No-Listado',

	// Services
	services_beauty: 'Belleza/Spa',
	services_computers: 'Computadoras/Programación',
	services_health: 'Salud',
	services_creative: 'Creativos',
	services_auto: 'Automotores',
	services_house: 'Casa/Jardín',
	services_write: 'Escritores',
	services_teach: 'Profesores/Clases',
	services_pets: 'Mascotas',
	services_marine: 'Marítimo',
	services_legal: 'Legal',
	services_finance: 'Finanzas/Contabilidad',
	services_adult: 'Para Adultos',

	// Health
	services_med: 'Medicina',
	services_nur: 'Enfermería',
	services_mas: 'Masaje',
	services_pth: 'Terapia Física',
	services_pde: 'Dentistas',
	services_ptr: 'Entrenadores Personales',
	services_pyo: 'Yoga/Pilates',
	services_psp: 'Terapia de Lenguaje',
	services_pli: 'Life Coach',
	services_acu: 'Acupuntura/Medicina China',
	// Computers
	services_pro: 'Programación',
	services_fix: 'Mantenimiento/Arreglos',
	services_hos: 'Servicios de Hosteo',
	services_seo: 'Optimización de SEO',
	// Beauty
	services_nai: 'Uñas',
	services_hai: 'Cabello/Estilistas',
	services_fas: 'Moda',
	services_wax: 'Depilación',
	// Creative
	services_des: 'Diseño/Diseño Web',
	services_pho: 'Fotografía/Video',
	services_adv: 'Publicidad',
	services_pai: 'Pintura',
	services_ppo: 'Cerámica',
	services_pme: 'Trabajos en Metal',
	services_pmu: 'Músicos',
	services_mod: 'Modelaje',
	services_nmo: 'Modelaje Desnudos',
	// Auto
	services_aur: 'Reparaciones',
	services_aup: 'Partes',
	services_aub: 'Mejoras/Pintura',
	services_cin: 'Seguros',
	services_ctr: 'Transporte',
	services_cre: 'Alquileres',
	services_cus: 'Compra/Venta',
	// Housing
	services_hbs: 'Compra/Venta - Bienes Raíces',
	services_hin: 'Seguros',
	services_hre: 'Reparaciones',
	services_hpa: 'Pintura',
	services_hse: 'Seguridad',
	services_hga: 'Jardinería',
	services_hmv: 'Mudanzas',
	services_hde: 'Decoración',
	services_hst: 'Bodegaje',
	// Writting
	services_wwr: 'Escritores',
	services_wtr: 'Tranducción',
	services_wed: 'Ediciones',
	// Teaching
	services_ttu: 'Tutorías',
	services_tps: 'Oradores Públicos',
	services_tla: 'Lenguajes',
	services_tsp: 'Deportes',
	services_tda: 'Danza',
	services_tsk: 'Habilidades Específicas',
	// Pets
	services_pwa: 'Caminadores de Perros',
	services_ptr: 'Entrenamiento',
	services_pca: 'Cuido',
	services_pgr: 'Cuidados/Corte Pelo',
	services_phe: 'Saludo/Veterinaria',
	// Marine
	services_brp: 'Reparaciones',
	services_bbs: 'Compra/Venta',
	services_bre: 'Restauraciones',
	services_bst: 'Almacenaje',
	services_btr: 'Transporte',
	services_brn: 'Alquileres',
	// Legal
	services_lso: 'Abogados',
	services_lle: 'Servicios Legales',
	services_lfa: 'Derecho de Familia',

	// Finance
	services_fac: 'Contabilidad',
	services_fin: 'Seguros',
	services_fad: 'Consejos de Finanzas',
	services_fiv: 'Inversiones',

	// Adult
	services_ada: 'Danza',
	services_awe: 'Webcams',
	services_ach: 'Chats',
	services_amo: 'Modelos',
	services_afi: 'Industria de Películas',
	services_aiv: 'Imagen y Video',

	// Other
	services_ski: 'Servicios Especiales',

};
