<!-- Menu Main -->
<div id="users_menuMain">

	<div class="users_menuUser">

		<?php if (_users_loggedIn()): ?>

		<div style="margin-bottom: 5px;">
			<a href="?w=users_profile_page" class="pure-button primary small full" style="overflow: hidden;">
				<span class="icon-user4"></span> <?= $skin['fullName'] ?>
			</a>
		</div>

		<button onClick="Cala.usersLogOut()" class="pure-button warning small full">
			<span class="icon-switch2"></span> {users_logoutButton}
		</button>

		<?php else: ?>
		<a href="?w=users_login_page" class="pure-button success full">
			<span class="icon-user4"></span> {users_loginButton}
		</a>
		<?php endif; ?>
	</div>

</div>
<!-- //Menu Main -->

