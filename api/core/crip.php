<?php


class KEYS{

	private $privKey;
	private $pubKey;
	private $outDir;
	private $bytes;

	# Use trailing slash for the $outDir!!!
	public function __construct($keyName, $outDir = 'keys/', $bytes = 1024){
		$this->privKey = $keyName . '_priv.pem';
		$this->pubKey  = $keyName . '_pub.pem';
		$this->outDir  = $outDir;
		$this->bytes   = $bytes;

		if($outDir != ''){
		if(!is_dir($outDir)){
			mkdir($outDir);
			if(!is_dir($outDir)){
				print "I was not able to make the directory\n";
				exit;
			}else{
				print "New directory created: $outDir\n";
			}
		}
		}
	}

	public function gen(){

		# Does it exist?
		if(file_exists($this->outDir . $this->privKey)){
			print "Error: Private key already exists \n";
			return false;
		}

		# First we make the priv key (which contains the public key)
		print "Making private key: $this->privKey \n";
		$e = "openssl genrsa -out $this->outDir$this->privKey 1024";
		echo $e . "\n";
		exec($e);

		# Exctract the public key and leave the previous file with just the private key
		print "Extract public key $this->pubKey \n";
		$ep = "openssl rsa -pubout -in $this->outDir$this->privKey -out $this->outDir$this->pubKey";
		echo $ep . "\n";
		exec($ep);
	}

	public function viewPriv(){
		print "View private Key \n";
		$out = array();
		echo $e = "cat $this->outDir$this->privKey";
		echo "\n";
		exec($e, $out);
		print_r($out);
		echo "\n";
	}

	public function viewPub(){
		print "View public Key \n";
		$out = array();
		echo $e = "cat $this->outDir$this->pubKey";
		echo "\n";
		exec($e, $out);
		print_r($out);
		echo "\n";

		$e = "cat $this->outDir$this->pubKey";
		exec($e);
	}

}

$keys = new KEYS('a');
//$keys->gen();
$keys->viewPriv();
$keys->viewPub();


