<?php
include_once('settings.php');

$files = file_get_contents("$server?w=cala_grace_logs_list&cronToken=$token&graceSkip");
$files = JSON_decode($files);
$files = $files->resp;

$out = '';

foreach ($files as $f) {

    # Clean the name a little
    $fv = substr($f, 0, strpos($f, ']_'));
    $fv = substr($fv, strpos($fv, '_[') + 2);

    $out .= "<a href='logs.php?log=$f&site=".$_GET['site']."&t=".$_GET['t']."' target='logs' class='sideMenuLinks'>$fv</a><hr class='sideMenuLinksSep' />";
}

?>

<!DOCTYPE html>
<html>
	 <head>
	 <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css.css" />
	 <title>Grace Viewer</title>
<style>
body{
	font-size: 24px;
	background: #494747;
	color: #e5e5e5;
}

a, a:hover, a:visited, a:active{
	color: white
}

code{
	border: 1px solid white;
	background: #a09999;
	display: block;
	padding: 6px;
}

.query, .req, .resp{
	border: 1px solid white;
	background: #a09999;
	display: block;
	padding: 6px;
}

#menu{
	word-wrap: break-word;
}

</style>
	 </head>
	 <body>
		<div align="center">
		<a href="index.php?site=<?= $_GET['site']?>&t=<?= $_GET['t'] ?>" target="_parent">Reload all</a>
			<br/>
		  <?php print '<a href="' . $server . '?w=cala_grace_clear_logs&iam=felipe&cronToken='.$token.'&sessionKey=1&graceSkip=1" target="logs">' ?>Clear logs</a>
			<br/>
			<a href="menu.php?site=<?= $_GET['site']?>" target="menu">Reload Menu</a>
		</div>
		<hr />
		<div id='menu'>
		  <?php print $out ?>
		</div>
	  </body>
</html>

