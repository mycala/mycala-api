/**
 * Locale en_UK
 */
var _l = {

	/******************************
	 * Cala
	 */
	cala_online: 'Online/Global',
	cala_allCountries: 'Online / Everywhere',
	cala_other: 'Other',
	cala_working: 'Working on it...',
	cala_alert: 'Alert!',
	cala_brilliant: 'Brilliant ;)',
	cala_err: 'Err!',
	cala_warning: 'Warning!',
	cala_userPwdSent: 'Please check your email for your new password. <br /> Remember to look in your <strong>JUNK</strong> mail just in case.',
	cala_userPwdSentErr: 'There was an error, please check your email address or username.',
	users_regUserExists: 'This user name or email is already taken. Please select a different one.',
	users_regFillAll: 'There was a problem, please check all required fields and try again.',

	/******************************
	 * Manager
	 */
	manager_nonlisted: 'Other non-listed',
	manager_other: 'Other/Non-Listed',

	manager_editGood: 'Your modifications where done',

	manager_errImageUploadFormat: 'Only jpg images are allowed',
	manager_errFileTooBig: 'This file is too big',

	k_other: 'Other/Non-Listed',

	// Services
	services_beauty: 'Beauty/Spa',
	services_computers: 'Computers/Programming',
	services_health: 'Health',
	services_creative: 'Creative',
	services_auto: 'Automotive',
	services_house: 'Household/Gardening',
	services_write: 'Writting',
	services_teach: 'Teaching/Lessons',
	services_pets: 'Pets',
	services_marine: 'Marine',
	services_legal: 'Legal',
	services_finance: 'Financial/Accounting',
	services_adult: 'Adult World',

	// Health
	services_med: 'Medicine',
	services_nur: 'Nurses',
	services_mas: 'Massage',
	services_pth: 'Physical Therapy',
	services_pde: 'Dentists',
	services_ptr: 'Personal Trainer',
	services_pyo: 'Yoga/Pilates',
	services_psp: 'Speach Therapy',
	services_pli: 'Life Coach',
	services_acu: 'Acupunture/Chinese Medicine',
	// Computers
	services_pro: 'Programing',
	services_fix: 'Fix/Maintenance',
	services_hos: 'Hosting Services',
	services_seo: 'SEO Optimization',
	// Beauty
	services_nai: 'Nails',
	services_hai: 'Hair/Stylists',
	services_fas: 'Fashion',
	services_wax: 'Waxing/Depilation',
	// Creative
	services_des: 'Design/Web Design',
	services_pho: 'Photograpy/Video',
	services_adv: 'Advertising',
	services_pai: 'Painting',
	services_ppo: 'Potters',
	services_pme: 'Metal Workers',
	services_pmu: 'Musicians',
	services_mod: 'Modeling',
	services_nmo: 'Nude Modeling',
	// Auto
	services_aur: 'Auto Repair',
	services_aup: 'Auto Parts',
	services_aub: 'Customization/Painting',
	services_cin: 'Auto Insurance',
	services_ctr: 'Transportation',
	services_cre: 'Rentals',
	services_cus: 'Buy/Sell',
	// Housing
	services_hbs: 'Buy/Sell - Real Estate',
	services_hin: 'House Insurance',
	services_hre: 'House Repairs',
	services_hpa: 'House Painting',
	services_hse: 'House Security',
	services_hga: 'Gardening',
	services_hmv: 'Moving',
	services_hde: 'Decoration',
	services_hst: 'Storage',
	// Writting
	services_wwr: 'Writting',
	services_wtr: 'Translations',
	services_wed: 'Editing',
	// Teaching
	services_ttu: 'Tutoring',
	services_tps: 'Public Speaking',
	services_tla: 'Languages',
	services_tsp: 'Sports',
	services_tda: 'Dance',
	services_tsk: 'Specific Skills',
	// Pets
	services_pwa: 'Dog Walking',
	services_ptr: 'Training',
	services_pca: 'Care',
	services_pgr: 'Grooming',
	services_phe: 'Health/Veterinarians',
	// Marine
	services_brp: 'Boat Repairs',
	services_bbs: 'Buy/Sell',
	services_bre: 'Restoraions',
	services_bst: 'Storage',
	services_btr: 'Transportation',
	services_brn: 'Rentals',
	// Legal
	services_lso: 'Solicitors',
	services_lle: 'Legal Services',
	services_lfa: 'Family Law',

	// Finance
	services_fac: 'Accounting',
	services_fin: 'Insurance',
	services_fad: 'Finance Advisories',
	services_fiv: 'Investments',

	// Adult
	services_ada: 'Dancing',
	services_awe: 'Webcams',
	services_ach: 'Chats',
	services_amo: 'Models',
	services_afi: 'Movie Industry',
	services_aiv: 'Image and Video',

	// Other
	services_ski: 'Skilled Services',

};
