<?php

/** @file module.php
 * Placeholder for sqilite functions, I started them, but now I don't use them
 * I keep them here for now, but I don't think they work very well.
 *
 * @author Twisted Head
 */

#####################################################################
#
# Sqlite3
#
#####################################################################

/** \addtogroup Modules
 *  @{
 */
/**
 * \defgroup Sqlite3
 * @{
 */

/**
 * Boot up procedure
 */
function sqlite_bootMeUp()
{
    //
}

/**
 * Init function
 */
function sqlite_init()
{
    //
}


/**
 * Use SQLite3 databases
 */

class MyDBLite extends SQLite3
{
    public $dbName = '';

    public function __construct($dbName, $mode = SQLITE3_OPEN_READWRITE)
    {
        $this->dbName = $dbName;

        grace_debug('Open db: ' . $this->dbName);

        if (!file_exists($dbName)) {
            grace_error('The database does not seem to exist.');
            return false;
        }

        if (!$this->open($dbName, $mode)) {
            grace_error('Error in sqlite: ' . $this->lastErrorMsg());
            return false;
        }

        self::exec('PRAGMA journal_mode=WAL');
        self::busyTimeout(10000);

        return true;
    }

    public function getName()
    {
        return $this->dbName;
    }

    ## Escape strings to be safely inserted
    ## They MUST come json encoded otherwise it returns an empty string
    public function escape($string)
    {
        return self::escapeString(json_decode($string, ENT_QUOTES));
    }

    /**
     * Execute a single result query.
     * Returns an array.
     */
    public function qSingle($q)
    {
        grace_debug("Execute query single sqlite: $this->dbName -> [query] $q [/query]");

        $result = $this->query($q);

        if ($this->lastErrorMsg() !== 'not an error') {
            grace_error('Some error in query: ' . $this->lastErrorMsg());
            return false;
        }

        # Check for empty result
        if ($result == false) {
            grace_debug('Nothing found apparently');
            return 0;
        }

        ## @bug Sqlite3 numRows is not working, this is a workarround
        ## https://stackoverflow.com/questions/2586598/using-sqlite3-in-php-how-to-count-the-number-of-rows-in-a-result-set/11589902#11589902
        $r = [];
        while ($row = $result->fetchArray()) {
            $r[] = $row;
        }
        if (count($r) > 0) {
            grace_debug('Found some results in the query');
            return @$r[0];
        } else {
            grace_debug('Nothing found in this query');
            return false;
        }
    }
}

function sqllite_dbExec(
    $q,
    $db
) {
    grace_debug("Execute query: $db->dbName -> [query] $q [/query]");

    $db->exec($q);

    $error = $db->lastErrorMsg();

    if ($error != 'not an error' || $db->changes() == 0) {
        grace_error('Error in Sqlite exec query: ' . $error);
        return $error;
    } else {
        grace_debug('Total affected rows: ' . $db->changes());
        return true;
    }
}

function sqllite_dbQuery($q, $db)
{
    grace_debug("Execute query: $db->dbName -> [query] $q [/query]");

    $result = $db->query($q);

    if ($result == false) {
        grace_debug('Err in query: ' . $db->lastErrorMsg());
        return $db->lastErrorMsg();
    }

    ## @bug Sqlite3 numRows is not working, this is a workarround
    ## https://stackoverflow.com/questions/2586598/using-sqlite3-in-php-how-to-count-the-number-of-rows-in-a-result-set/11589902#11589902

    // check for empty result
    $rows = $result->fetchArray(SQLITE3_ASSOC);
    if ($rows != false) {
        $result->reset();
        grace_debug('Found some results in the query');

        ## Lets move this to an array
        $allResults = array();
        while ($r = $result->fetchArray(SQLITE3_ASSOC)) {
            array_push($allResults, $r);
        }
        return $allResults;
    }

    grace_debug('No results found');
    return false;
}

function sqllite_dbQuerySingle($q, $db)
{
    grace_debug("Execute query single sqlite: $db->dbName -> [query] $q [/query]");

    $result = $db->querySingle($q, true);

    if ($db->lastErrorMsg() !== 'not an error') {
        grace_error('Some error in query: ' . $db->lastErrorMsg());
        return false;
    }

    // check for empty result
    if ($result == false) {
        grace_debug('Nothing found apparently');
        return 0;
    }

    ## @bug Sqlite3 numRows is not working, this is a workarround
    ## https://stackoverflow.com/questions/2586598/using-sqlite3-in-php-how-to-count-the-number-of-rows-in-a-result-set/11589902#11589902

    grace_debug('Found some results in the query');
    return $result;
}
