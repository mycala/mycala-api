<?php

/**
 * Handle cache
 */

/**
 * Store cache
 */
function cache_store(
    $content,
    $pageName
) {
    grace_debug('Save cache');

    if (($path = conf_get('tmpDir', 'core', '')) === '') {
        grace_error('No cache dir');
        return false;
    }

    $path .= 'cache/';

    file_put_contents($path.base64_encode($pageName), $content);
}

/**
 * Get a cached page
 */
function cache_get(
    $pageName,
    $life = 1000
) {
    grace_debug('Get cache, max lifespan: (seconds)' . $life);

    if (($path = conf_get('tmpDir', 'core', '')) === '') {
        grace_error('No cache dir');
        return false;
    }

    $path .= 'cache/';

    if (!file_exists($path.base64_encode($pageName))) {
        return false;
    }
     
    grace_debug('Page taken from the cache');
     
    return file_get_contents($path.base64_encode($pageName));
}
