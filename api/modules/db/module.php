<?php

/** @ingroup Constants
 *  @{
 */

# Could not find restults in the db
define('ERROR_DB_NO_RESULTS_FOUND', '-2.00');

# Errors in the query
define('ERROR_DB_ERROR', '-2.01');

# No db connection
define('ERROR_DB_NOT_CONNECTED', '-2.02');


# What do you expect to get from the database?
# Retrieve nothing: updates, inserts and so on
define('DB_RETRIEVE_NONE', '201');
# Select where you expect only one result
define('DB_RETRIEVE_ONE', '202');
# Select where you expect many results
define('DB_RETRIEVE_MANY', '203');

/** @}*/

/* Global Vars */
global $dbConn;

/**
 * Boot up procedure.
 *
 * @todo die if this fails?
 */
function db_bootMeUp()
{
    db_Connect();
}

//! Test if the connection is good, just for debug
function db_allGood()
{
    global $dbConn;

    if ($dbConn->connect_error) {
        grace_error("DB Connection error: " . $dbConn->connect_error);
        return $dbConn->connect_error;
    }

    return true;
}

/**
 * I connect to the default database, maybe in the future I can have multiple
 * connections. But not likely to be honest.
 */
function db_Connect()
{
    global $dbConn;

    # Create connection

    @$dbConn = new mysqli(conf_get('host', 'db'), conf_get('user', 'db'), conf_get('pwd', 'db'), conf_get('name', 'db'));

    # Check connection
    if ($dbConn->connect_error) {
        grace_error("Connection failed: " . $dbConn->connect_error);
        return false;
    } else {
        $dbConn->set_charset("utf8mb4");
        grace_info("Connected to Db: " . conf_get('name', 'db'));
        return true;
    }
}

/**
 * I make queries, use only for 'selects', anything no-return queries use
 * db_exec()
 * @param $q The query to be excecuted
 * @param $return The number of return items you want 0: none, 1: Just one, >1: All that you have
 * @todo use _db_query
 */
function db_query($q, $return = 1)
{
    global $dbConn;

    $qLog = str_replace("\n", "", $q);
    grace_debug("Query MariaDB: [query] $qLog [/query]");

    if (db_allGood() === true) {
        grace_queries('add');

        $r = $dbConn->query($q);
        $result = array();

        if ($dbConn->error) {
            grace_error("Error: " . $dbConn->error);
            return ERROR_DB_ERROR;
        }

        if ($dbConn->affected_rows > 0) {
            @grace_debug("Affected (selected) rows: " + $dbConn->affected_rows);

            # Move all results to an array
            while ($row = $r->fetch_object()) {
                $result[] = $row;
            }
            if ($return == 2) {
                grace_debug("Looking for more than 1 result");
                return $result;
            } else {
                grace_debug("Looking for just one result");
                return $result[0];
            }
        } else {
            grace_debug("Nothing found in the database");
            return false;
        }
    }
    grace_debug("Database not connected");
    return false;
}

/**
 * NEW IT IS THE FUTURE!
 * I make queries, use only for 'selects', any no-return queries use db_exec()
 * @param $q The query to be excecuted
 *
 * @return The information found (array), false on error, 0 on nothing found.
 * @todo use _db_query
 */
function db_q(
    $q,
    $array = true //!< Return values in an array, if false I will return the query result buffer
) {
    global $dbConn;

    $qLog = str_replace("\n", "", $q);
    grace_debug("Query -multi- MariaDB: [query] $qLog [/query]");

    if (db_allGood() === true) {
        grace_queries('add');

        $r = $dbConn->query($q);
        $result = array();

        if ($dbConn->error) {
            grace_error("Error: " . $dbConn->error);
            return false;
        }

        if ($dbConn->affected_rows > 0) {
            @grace_debug("Affected (selected) rows: " . $dbConn->affected_rows);

            # Move all results to an array
            if ($array) {
                while ($row = $r->fetch_assoc()) {
                    $result[] = $row;
                }
                $r->free();
                return $result;
            }
            return $r;
        } else {
            grace_debug("Nothing found in the database");
            return false;
        }
    }
    grace_debug("Database not connected");
    return false;
}


/**
 * I make queries that expect a single result, use only for 'selects',
 * any no-return queries use db_exec().
 *
 * @param $q The query to be excecuted
 *
 * @return The information found, false on error, 0 on nothing found.
 */
function db_querySingle($q)
{
    $r = _db_query($q);

    # Error
    if ($r === false) {
        return false;
    }
    if ($r === 0) {
        return 0;
    }

    return $r[0];
}

/**
 * I actually make queries, never call me!
 */
function _db_query($q)
{
    global $dbConn;

    $qLog = str_replace("\n", "", $q);
    grace_debug("Query MariaDB: [query] $qLog [/query]");

    if (db_allGood() === true) {
        grace_queries('add');

        $r = $dbConn->query($q);
        $result = array();

        if ($dbConn->error) {
            grace_error("Error: " . $dbConn->error);
            return false;
        }

        if ($dbConn->affected_rows > 0) {
            @grace_debug("Affected (selected) rows: " . $dbConn->affected_rows);

            # Move all results to an array
            while ($row = $r->fetch_assoc()) {
                $result[] = $row;
            }
            return $result;
        } else {
            grace_debug("Nothing found in the database");
            return 0;
        }
    }

    grace_debug("Database not connected");

    return false;
}

/**
 * I make queries that do not return values such as inserts and deletes
 * @param $q The query to be excecuted
 * @todo use _db_query ?
 */
function db_exec($q)
{
    global $dbConn;

    $qLog = str_replace("\n", "", $q);
    grace_debug("Exec query MariaDB: [query] $qLog [/query]");

    if (db_allGood() === true) {
        grace_queries('add');

        $r = $dbConn->query($q);

        grace_queries();

        if ($dbConn->error) {
            grace_error("Database error: " . $dbConn->error);
            # Database errors can not be safely sent to the database,
            # I need to log them somewhere else.
            return 'ERROR_DB_ERROR';
        }

        grace_debug("Query affected: " . $dbConn->affected_rows);

        if ($dbConn->affected_rows > 0) {
            return $dbConn->affected_rows;
        } else {
            return 0;
        }
    } else {
        return false;
    }

    return 'ERROR_DB_ERROR';
}

/**
 * Multi-Query
 * @param $q Los queries a ejecutar en un arreglo
 */
function db_execMulti($q)
{
    global $dbConn;

    // grace_debug("Exec query MariaDB: [query] $q [/query]");

    if (db_allGood() === true) {
        //grace_queries('add');

        $r = $dbConn->multi_query(join('; ', $q));

        //grace_queries();

        if ($dbConn->error) {
            grace_error("Database error: " . $dbConn->error);
            # Database errors can not be safely sent to the database,
            # I need to log them somewhere else.
            return 'ERROR_DB_ERROR';
        }

        grace_debug("Query affected: " . $dbConn->affected_rows);

        if ($dbConn->affected_rows > 0) {
            return $dbConn->affected_rows;
        } else {
            return 0;
        }
    } else {
        return false;
    }

    return 'ERROR_DB_ERROR';
}

/**
 * Protect strings to insert in db
 */
function db_escape($str)
{
    global $dbConn;
    return $dbConn->real_escape_string($str);
}

/**
 * Get the last error in the database.
 */
function db_lastError()
{
    global $dbConn;

    return $dbConn->error;
}

/**
 * Check and see if the response was an error or empty
 *
 * I will see if the query was good aka if no errors where produced, even if no results where given
 */
function _db_queryGood(
    $r
) {
    if ($r && $r !== 'ERROR_DB_ERROR') {
        return true;
    }
    return false;
}
