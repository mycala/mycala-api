
var Cala = {

	v: '0.7.5',

	FORM_STATUS_EDIT: 'edit	',

	pathInternal: '',

	// Talk to the screen for modules.
	talkScreen: true,

	// Print CalaJs related stuff to the screen, you may turn this off and still get information from
	// other modules.
	calaTalkScreen: false,
	// Different levels of print
	printInfo: true,
	printWarn: true,
	printErr: true,

	/***************************************************************************
	 *
	 * Menu
	 *
	 */

	menuOpen: function(d) {
		d.src = d.src === undefined ? '#cala_menuMain' : d.src;
		d.dest = d.dest === undefined ? '#cala_menuSmall' : d.dest;
		d.extras = d.extras === undefined ? '#cala_menuExtras' : d.dest;
		// Menu is closed
		if($(d.dest).css('display') == 'none'){
			$(d.dest).html($(d.src).html());
			$(d.dest).slideDown();
			$(d.extras).slideDown();
		}else{
			$(d.dest).slideUp();
			$(d.dest).html('');
			$(d.extras).slideUp();
		}
	},

	/***************************************************************************
	 *
	 * Keep on talking
	 *
	 */

	/**
	 * Say stuff.
	 */
	say: function (what, type = 'm', w = ''){
		if(!Cala.talkScreen){ return false;	}
		if(w == 'cala' && !Cala.calaTalkScreen){ return false; }
		// General messages
		if(type == "m"){ console.log(what);
		// General information
		}else if(type == "i"){ console.info(what);
		// Warnings
		}else if(type == "w"){ console.warn(what);
		// Errors
		}else if(type == "e"){ console.error(what); }
		$('#cala_debug').append(what + '<br />');
	},

	/**
	 * Helper function to make my life easier. Err messages.
	 */
	err: function(w, b = ''){
		if(!Cala.printErr){return false;}
		if(b == 'cala' && !Cala.calaTalkScreen){return false}
		Cala.say('[e]' + w, 'e');
	},

	/**
	 * Helper function to make my life easier. Info messages.
	 */
	info: function(w, b = ''){
		if(!Cala.printInfo){return false;}
		if(b == 'cala' && !Cala.calaTalkScreen){return false}
		Cala.say('[i]' + w, 'i');
	},

	/**
	 * Helper function to make my life easier. Warn messages.
	 */
	warn: function(w, b = ''){
		if(!Cala.printWarn){return false;}
		if(b == 'cala' && !Cala.calaTalkScreen){return false}
		Cala.say('[w]' + w, 'w');
	},

	/**
	 * Inspired by print_r (PHP)
	 */
	r: function(w, s = ''){
		Cala.say(s + ': '); for(var z in w){Cala.say(z + ' => ' + w[z]); }
	},

	/**
	 * Nice notifications.
	 *
	 * Floating on the top right.
	 *
	 * d = {
	 *		timeOut: 2000
	 *		type: info|success|warning
	 *		id: id
	 *		close: true|false
	 * };
	 *
	 */
	notify: function(w, d = {}){

		Cala.say('Notify');

		d.timeOut = d.timeOut == undefined ? 2000 : d.timeOut;
		d.type = d.type == undefined ? 'info' : d.type;
		d.id = 'not_' + Math.round(Math.random() * 15784);
		d.close = d.close == undefined ? false : true;
		d.size = d.size == undefined ? 'notNormal' : d.size;
		d.where = d.where == undefined ? 'notifications' : d.where;
		w = w === undefined ? Cala.l.cala_brilliant : w;

		if(d.close == true){
			var notification = `<aside class="notification ${d.size} ${d.type}" id="${d.id}">
				<div onClick="Cala.notifyClose('${d.id}');" style="padding-top: 4px; float: right;" class="xsmall">
				&nbsp;&nbsp;&nbsp;<span class="icon-cross"></span>
				</div>
				${w}
				</aside>`;
		}else{
			var notification = `<aside class="notification ${d.size} ${d.type}" id="${d.id}">${w}</aside>`;
		}

		$('#'+d.where).append(notification)
			.slideDown(
				'slow',
				function(){
					setTimeout(
						function(){
							Cala.notifyClose(d.id);
						},
						d.timeOut)
				});

	},

	/**
	 * Close notifications
	 */
	notifyClose(id){
		Cala.say('I will close notification: ' + id);
		$(`#${id}`).fadeOut('slow',
			function(){
				$(`#${id}`).remove();
			});
	},

		/**
	 * Present an alert message.
	 */
	_alert: function(b, t = false, then){

		if(!t){
			t = _l.cala_alert;
		}

		Swal.fire(
			t,
			b,
			'info'
		).then((result) => {
			if(then != undefined)
				then(result.value);
		});

	},

	/**
	 * Present an success message.
	 */
	_alertSuccess: function(b, t = false, then){

		if(!t){
			t = _l.cala_brilliant;
		}

		Swal.fire(
			t,
			b,
			'success'
		).then((result) => {
			if(then != undefined)
				then(result.value);
		});
	},

	/**
	 * Present an warning message.
	 */
	_alertWarning: function(b, t = false, then){
		if(!t)
			t = _l.cala_warning;
		Swal.fire(
			t,
			b,
			'warning'
		).then((result) => {
			if(result.value){
				if(then != undefined)
					then(result.value);
			}
		});
	},

	/**
	 * Present an alert error message.
	 */
	_alertErr: function(b, t = false, then){

		if(!t){ t = _l.cala_err}
		Swal.fire(
			t,
			b,
			'error'
		).then((result) => { if(then != undefined){ then(result.value); } });

	},

	/**
	 * Present an info message.
	 */
	_alertInfo: function(b, t = false, then){

		if(!t){ t = _l.cala_info}
		Swal.fire(
			t,
			b,
			'info'
		).then((result) => { if(then != undefined){ then(result.value); } });

	},

	/***************************************************************************
	 *
	 * Paths
	 *
	 */
	go(where = false){

		// Front
		if(!where){
			where = window.location;
		}else if(where == '<front>'){
			var a = window.location;
			where = a.pathname;
		}else	if(where.indexOf('http') == -1){
			where = '?w=' + where;
		}

		Cala.say('Go: ' + where);

		window.location = where;
	},

	/**
	 * Get the variables from the url.
	 * Thanks to: https://html-online.com/articles/get-url-parameters-javascript/
	 */
	routesUrlGetVars(text = false) {
		text = !text ? document.URL : text;
		Cala.say('Extracting vars from a string' + text);
		var vars = {};
		text.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) { vars[key] = value });
		return vars;
	},

	/***************************************************************************
	 *
	 * Loaders
	 *
	 */

	/**
	 * Loading indicator.
	 */
	_loaderIndicator: 'web/img/ajax-loader.gif',
	_loaderButton: 'web/img/loader_button.gif',

	_loadIni: function(
		dets = {}
	){

		dets.where = dets.where === undefined ? 'loader_global' : dets.where;
		dets.img   = Cala.pathInternal + (dets.img == undefined ? Cala._loaderIndicator : dets.img);
		dets.text  = dets.text == undefined ? _l.cala_working : dets.text;

		// Block buttons
		if(dets.block !== undefined){
			Cala.info('Block buttons');
			$(dets.block).prop('disabled', true);
		}

		$('#'+dets.where).html(`<img src="${dets.img}" />`).show();

		$(`#${dets.where}Info`).html(dets.text);

		// Button
		if(dets.button !== undefined){
			$(dets.button).attr('disabled', true);
		}

		// Global loader is an overlay
		if(dets.where == 'loader_global'){
			$('#loader_global').show();
		}

	},

	_loadEnd: function(
		dets = {}
	){
		dets.where = dets.where === undefined ? 'loader_global' : dets.where;
		$('#'+dets.where).hide();
		$(`#${dets.where}Info`).html('');

		// Button
		if(dets.button !== undefined){
			$(dets.button).attr('disabled', false);
		}

		// Global loader is an overlay
		if(dets.where == 'loader_global'){
			$('#loader_global').hide();
		}

	},

	_loadButtonIni: function(
		dets = {}
	){
		Cala.say('Load ini in: ' + dets.button);
		dets.img  = Cala.pathInternal + (dets.img == undefined ? Cala._loaderButton : dets.img);

		$('#'+dets.button).attr('disabled', true);
		//$('#'+dets.button).insertAfter('<img src="'+dets.img+'" id="'+dets.button+'_loader" />');
		$('<img src="'+dets.img+'" id="'+dets.button+'_loader" />').insertBefore('#'+dets.button);

	},

	_loadButtonEnd: function(
		dets = {}
	){

		$('#'+dets.button).attr('disabled', false);
		$('#'+dets.button+'_loader').remove();

	},

	/***********************************************************************************************
	 * Users
	 */

	usersLogin(){

		Cala.say('Login');

		Cala._loadIni({button: '#users_loginButton'});

		Cala.toolsConnect(
			Cala.server,
			{
				w: 'users_log_me_in',
				userName: $('#userName').val(),
				pwd: $('#pwd').val(),
				method: 'php'
			},
			function(){
				Cala.say('I am in');
				Cala.go('<front>');
				Cala._loadEnd({button: '#users_loginButton'});
			},
			function(){
				Cala.err('Something happened');
				Cala._alertErr(_l.cala_userPwdSentErr);
				Cala._loadEnd({button: '#users_loginButton'});
			}
		);

		return false;
	},

	usersLogOut(){

		Cala.say('Lets go');

		Cala._loadIni();

		Cala.toolsConnect(
			Cala.server,
			{
				w: 'users_log_me_out'
			},
			function(){
				Cala.say('I am out');
				Cala.go('<front>');
				Cala._loadEnd();
			},
			function(){
				Cala.err('Something happened');
				Cala._loadEnd();
			}
		);
	},

	/**
	 * Recover password.
	 */
	usersPwdRecover(){

		var userName = $('#users_userNameRecovery').val();

		if(userName == ''){ return false }

		Cala._loadIni();
		Cala.toolsConnect(
			Cala.server,
			{
				w: 'users_recover_pwd',
				userName: userName
			},
			function(){
				Cala._alertSuccess(_l.cala_userPwdSent);
				Cala._loadEnd();
			},
			function(){
				Cala._alertErr(_l.cala_userPwdSentErr);
				Cala._loadEnd();
			}
		);

		return false;
	},

	usersRegister(){

		Cala.say('Register a new user');
		var dets = Cala.formGetValues('form_userRegister');
		if(dets.pwd == ''){ return false }
		if(dets.email == ''){ return false }

		Cala._loadIni({button: '#users_registerButton'});

		dets.w = 'users_register';
		dets.fullName = dets.email;
		dets.userName = dets.email;
		dets.method = 'php',
		Cala.toolsConnect(
			Cala.server,
			dets,
			function(r){
				Cala.info('Registration was good.');
				Cala.go('<front>');
				Cala._loadEnd({button: '#users_registerButton'});
			},
			function(e){
				Cala.err('Unable to register the account: ' + e);
				if(e === 'ERR_USERS_EXISTS'){
					Cala._alertErr(_l.users_regUserExists);
				}else{
					Cala._alertErr(_l.users_regFillAll);
				}
				Cala._loadEnd({button: '#users_registerButton'});

			}
		);

		return false;
	},

	usersEdit(){

		Cala.say('Update the profile');

		var dets = Cala.formGetValues('form_userEdit');
		dets.w = 'users_update_profile';

		Cala._loadIni();

		Cala.toolsConnect(
			Cala.server,
			dets,
			function(r){
				Cala._alertSuccess();
				Cala._loadEnd();
			},
			function(e){
				Cala.err('Error updating your profile');
				Cala._alertErr();
				Cala._loadEnd();
			}
		);

		return false;

	},

	usersAvatarUpload(){

		Cala.say('Upload an avatar');

		// Format?
		var file = $('#users_avatarUpload').val();
		var ext = file.slice((file.lastIndexOf(".") - 1 >>> 0) + 2);

		if(ext != 'jpg' && ext != 'png'){
			Cala._alertErr(_l.users_errAvatarUploadFormat);
			return false;
		}

		Cala._loadIni({where: 'loader_avatarUpload'});

		//https://stopbyte.com/t/how-to-sending-multipart-formdata-with-jquery-ajax/58
		var data = new FormData();
		$.each($('#users_avatarUpload')[0].files, function(i, file){
			data.append('image', file);
		});

		data.append('w', 'users_avatar_upload');

		jQuery.ajax({
			url: Manager.server,
			data: data,
			cache: false,
			contentType: false,
			processData: false,
			dataType: 'text',
			type: 'POST',
			success: function(r){
				r = JSON.parse(r);
				// Was there an error?
				var r = Cala.toolsCheckRespErr(r);
				if(r.err !== undefined){
					Cala.err('There was an erorr');
					if(r.err == 'ERROR_FILES_EXT_NOT_ALLOWED'){
						Cala._alertErr(_l.users_errAvatarUploadFormat);
					}else	if(r.err == 'ERROR_FILE_TOO_BIG'){
						Cala._alertErr(_l.users_errAvatarBig);
					}else{
						Cala._alertErr();
					}
				}else{
					Cala.info('All good, image is up');
					Cala.usersAvatarsSet(true);
				}
				Cala._loadEnd({where: 'loader_avatarUpload'});
			}
		});

	},

	usersAvatarsSet(force = false){
		$('.users_avatar').each(function(){
			var src = $(this).attr('avatar-source');
			var path = users_paths_avatars + src + '_avatar.jpg';
			if(force){
				path =  Cala.randLink(path);
			}
			$(this).attr('src', path);
		})

	},

	usersAvatarErr(t){
		Cala.err('Error with user avatar: ' + t.id + ' . ' + $('#'+t.id).attr('src'));
		$('#'+t.id).attr('src', users_avatar_default)

	},

	/***************************************************************************
	 *
	 * Templates
	 *
	 */

	/**
	 * I will add a random number to links to avoid problems with cache.
	 */
	randLink: function(l){
		var link = l +
			(l.indexOf('?') > 0 ? '&' : '?') +
			'calaLinkRand=' +
			Math.random();
		return link;
	},

	/**
	 *  I parse a tpl and replace it with some values.
	 *
	 *  @param tpl The template
	 *  @param values An object with the values to replace
	 *  @param bl Do you want me to replace the breaklines (\n) with <br />
	 */
	tpl: function(tpl, v, bl = false){

		// Loop each value
		for (var k in v) {
			var theKey = "_%" + k + "%_";
			var re = new RegExp(theKey, "g");
			tpl = tpl.replace(re, v[k]);
		}

		// Should I add html breakLines?
		if(bl === true){
			tpl = Cala.textAddBreakLines(tpl);
		}

		return tpl;

	},

	/**
	 * Template something multiple times, loop
	 */
	tplMulti: function(tpl, v, bl = false){
		var t = '';
		for(var z in v){t += Cala.tpl(tpl, v[z], bl); }
		return t;
	},

	/**
	 * Get a template from a div.
	 */
	tplFromDiv: function(id){
		return $(`#${id}`).html();
	},

	/**
	 * Add html break lines to the text
	 */
	textAddBreakLines: function(text){
		var theKey = "\n";
		var re = new RegExp(theKey, "g");
		text = text.replace(re, "<br />");
		return text;
	},

	/**
	 * Get a template from an html file
	 */
	tplFromFile(d, doThis = {}){

		$.ajax({
			url: Cala.pathInternal + 'web/'+Cala.randLink(d.name),
			dataType: "html",
		}).done(function(tpl){

			Cala.info('Got your template');

			var parsed = '';

			if(d.multi !== undefined){
				parsed = Cala.tplMulti(tpl, d.v);
			}else{
				parsed = Cala.tpl(tpl, d.v);
			}

			// Translate?
			if(d.translate !== undefined){ parsed = Cala.tpl(parsed, _l, false) }

			if(doThis.post !== undefined){ parsed = doThis.post(parsed) }
			$('#'+d.where).html(parsed);
			if(doThis.success !== undefined){ doThis.success() }

		}).fail(function(s){
			Cala.err('Error getting your template: ' + d.name);
			if(doThis.err != undefined){ doThis.err(s) }
		});

	},

	/**
	 * Display/Hide a password field`s contents
	 */
	pwdToggle: function(id){
		var x = document.getElementById(id);
		if (x.type === "password") {
			x.type = "text";
		} else {
			x.type = "password";
		}
		return false;
	},

	/***************************************************************************
	 *
	 * Forms
	 *
	 */
	formSubmit(
		formId = '',
		success = false
	){
		Cala.say('Submit form: ' + formId);

		if(formId == ''){
			return false;
		}

		Cala._loadButtonIni({button: formId+'_submit'});

		var formValues = Cala.formGetValues(formId, formId);
		formValues.w = $('#'+formId).attr('action');
		formValues.form = formId;
		Cala.toolsConnect(
			'',
			formValues,
			function(r){
				Cala.info('Form sent correctly');
				var r = $('#'+formId+'_redirect').val();
				if(r != ''){
					Cala.go(r);
				}else{
					Cala._alertSuccess(_l.manager_editGood);
					if(success){ success() }
				}
				Cala._loadButtonEnd({button: formId+'_submit'});
			},
			function(e){
				Cala.err('Error with the form: ' + JSON.stringify(e))
				e = JSON.parse(e);
				if(e.required !== undefined){
					Cala.err('Required fields are missing')
					for(var ee in e.required){
						$('#'+e.required[ee]).addClass('cala_form_err');
					}
				}
				Cala._loadButtonEnd({button: formId+'_submit'});
			});
		return false;
	},

	/**
	 * Get values from a form
	 */
	formGetValues: function(id, pre = ''){

		Cala.say('Getting values from a form: ' + id);

		var $inputs = $(`#${id} :input`);
		var values = {};
		var id = '';

		$inputs.each(function() {
			if(this.id != ''){
				id = this.id;
				// Ignore those that begging with '_'
				if(id.indexOf('_') != 0){
					if(pre != ''){ id = id.replace(pre+'_', '');	}
					var type = $('#'+id).attr('type');
					if(type == 'checkbox'){	values[id] = $('#'+id).prop('checked');
					}else{ values[id] = $(this).val(); }
				}
			}
		});

		return values;

	},

	/**
	 * Clear values from a form
	 */
	formClearValues: function(id){
		Cala.say('Clear values from a form: ' + id);
		$(`#${id} :input`).each(function() { $(this).val(''); 	});
		return this;
	},

	/**
	 * Load a form with values.
	 *
	 * @todo set them inside an specific form without using extra
	 */
	formSetValues: function(v, extra = ''){

		Cala.say('Load a form with values');

		var parts = Object.keys(v);

		for(var x in v){
			var elId = extra != '' ? extra + '_' + x : x;
			var elType = $('#'+elId).attr('type');
			if(elType == 'checkbox'){ $('#'+elId).prop('checked', v[x]);
			}else{ $(`#${elId}`).val(v[x]); }
		}

	},

	/***************************************************************************
	 *
	 * Tools (Remote connections)
	 *
	 */

	/**
	 * Connect to a remote server (CalaApi).
	 *
	 * @todo I am adding '//' at the beggining of the url, not the best idea, but
	 * it should work for now.
	 *
	 */
	toolsConnect: function(
		url, /**< Where am I connecting to? */
		data, /**< Data ({key: 'value'...}) I will send*/
		success, /**< What to do if successful and no error is given */
		err /**< What to do if connection err or err is response is given */
	){

		Cala.say('Connecting to a remote server: ' + url);

		$.ajax({
			url: url,
			method: 'POST',
			dataType: data.rType === undefined ? 'json' : data.rType,
			data: data,
		})
			.done(function(s){

				Cala.info('Success in this call');

				// Was there an error?
				var r = Cala.toolsCheckRespErr(s);

				if(r.err !== undefined){
					// Loged out?
					// @todo I think this is not the case for php based
					if(r.err == 'USERS_ERR_NOT_LOGGED_IN'){
						Cala.err('User is loged out, I shall log out here too');
						//Cala.userRemoteLogout(false);
						//Cala.routes.goHome();
						return false;
					}

					if(err !== undefined){ err(r.err) }
				}else{
					if(success !== undefined){ success(r) }
				}
			}).fail(function(s){
				Cala.err('Error in this call: ' + JSON.stringify(s));
				if(err !== undefined){ err(s) }
			});

	},

	/**
	 * Check if response is error.
	 *
	 * Returns false if it is an error or the parsed json if not.
	 */
	toolsCheckRespErr: function(d){

		Cala.info('Was there an error in the response?');

		if(d.err !== undefined){
			Cala.err('Err: ' + d.err);
			return d;
		}

		Cala.info('No errors found');

		return d.resp;
	},

	/**
	 * Parse a date from a UTC timestamp
	 * Read more https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Text_formatting
	 * IMPORTANT! This takes seconds instead of milliseconds
	 * @todo This is a mess in regards to locale settings
	 */
	toolsDateParse: function(
		timestamp,
		format = 'short'
	){
		//var utcSeconds = 1234567890;
		var d = new Date(0);
		d.setUTCSeconds(timestamp);

		switch(format){
			case 'full':
				t = d.toString();
				break;
			case 'short':
				t = d.toLocaleDateString();
				break;
			case 'localeTime':
				t = d.toLocaleTimeString();
				break;
			case 'med':
				t = d.toLocaleDateString() + ' ' + d.toLocaleTimeString();
				break;
			default:
				t = d.toString();
				break;
		}

		return t;

	},

	toolsNumberParse(num){
		var num_parts = num.toString().split(".");
		num_parts[0] = num_parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ",");
		return num_parts.join(".");
	},

	/** Get the variables from the url. Thanks to: https://html-online.com/articles/get-url-parameters-javascript/  */
	toolsUrlGetVars(text) {
		Cala.say('Extracting vars from a string' + text);
		var vars = {};
		text.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) { vars[key] = value });
		return vars;
	},

	/***************************************************************************
	 *
	 * Location, countries and so on.
	 *
	 */

	/**
	 * Generate a 'select list' with countries.
	 * @deprecated
	 */
	toolsCountriesGen: function(dets, success, err){

		Cala.say('Generating a country list in: ' + dets.where);

		Cala._loadIni();

		$.ajax({
			url: Cala.pathInternal + 'web/countries.json',
			dataType: 'json',
			success: function(c){

				Cala.info('Got the list of countries');

				var select = '';
				if(dets.online == true){
					select += `<option value="onl" selected>${_l.cala_online}</option>`;
				}

				if(dets.all == true){
					select += `<option value="0" selected>${_l.cala_allCountries}</option>`;
				}

				for(var i = 0; i < c.length; i++){
					select = select + '<option value="'+c[i][2]+'">'+c[i][1]+'</option>';
				}

				select = select + `<option value="oth">${_l.cala_other}</option>`;

				$(dets.where).html(select);

				Cala._loadEnd();

				if(success !== undefined){
					success();
				}
			},
			error: function(e){
				Cala.err('I did not find the country list ' + JSON.stringify(e));
				Cala._loadEnd();
				if(err != undefined)
					err();
			}
		});
	},

	/**
	 * Parses ALL bbcodes in a page
	 */
	bbParse(c = '.bbCodeMe'){

		console.log('I shall bbcode things');
		$.each($(c), function( index, value ) {
			var result = XBBCODE.process({
				text: $(this).html(),
				removeMisalignedTags: false,
				addInLineBreaks: false
			});
			$(this).html(result.html)
		});

	},

	/***********************************************************************************************
	 * Countries
	 */

	c_selects: {
		world: 'world',
		region: 'region',
		city: 'city'
	},

	/**
	 * Generate a select list with countries.
	 */
	c_countriesGen(dets, success, err){

		Cala.say('Generating a country list for: ' + dets.where);

		Cala._loadIni();

		$.ajax({
			url: Cala.pathInternal + 'web/world/countries.js',
			success: function(){

				var c = JSON.parse(allCountries);
				Cala.info('Got the list of countries');

				var select = '';
				if(dets.online == true){
					select += `<option value="onl" selected>${_l.cala_online}</option>`;
				}

				if(dets.all == true){
					select += `<option value="0" selected>${_l.cala_allCountries}</option>`;
				}

				for(var i = 0; i < c.length; i++){
					select = select + '<option value="'+c[i][0]+'">'+c[i][1]+'</option>';
				}

				select = select + `<option value="oth">${_l.cala_other}</option>`;

				$(dets.where).html(select);

				if(dets.regions){
					Cala.c_worldSetWorldForChange();
				}

				if(dets.cities){
					Cala.c_worldSetRegionForchange();
				}

				Cala._loadEnd();

				if(success !== undefined){
					success();
				}

			},
			error: function(e){
				Cala.err('I did not find the country list ' + JSON.stringify(e));
				Cala._loadEnd();
				if(err !== undefined) err();
			}
		});

	},

	/**
	 * Generate a select list with regions.
	 */
	c_regionsGen(r, success){

		Cala.say('Generating a region list for country: ' + r);

		Cala._loadIni();

		$.ajax({
			url: 'web/world/regions/'+r+'.json',
			dataType: 'json',
			success: function(d){
				$('#locationDiv').slideDown();
				//d = JSON.parse(d);
				var select = '<option value="#">--</option>';
				for(var i = 0; i < d.length; i++){
					select = select + '<option value="'+d[i][0]+'|'+d[i][1]+'">'+d[i][1]+'</option>';
				}
				success(select);
				Cala._loadEnd();

			},
			error: function(){
				Cala._loadEnd();
			}
		}).done(function(d) {
			Cala._loadEnd();
		});

	},

	/**
	 * Generate a select list with cities.
	 */
	c_cityGen(c, success){

		Cala.say('Generating a city list for region: ' + c);

		Cala._loadIni();

		var country = $('#country').val();

		$.ajax({
			url: 'web/world/cities/'+country+'/'+c+'.json',
			dataType: 'json',
			success: function(d){
				var select = '';
				for(var i = 0; i < d.length; i++){
					select = select + '<option value="'+d[i][0]+'">'+d[i][0]+'</option>';
				}

				Cala._loadEnd();

				success(select);

			},
			error: function(){
				Cala._loadEnd();
			}
		}).done(function(d) {
		});

	},

	// This is hard coded, it will evolve
	c_worldSetWorldForChange(where = '#country', regionSelect = '#region'){
		Cala.say('Auto set regions on country change');
		// Generate the regions list based in the country
		$(where).change(function(){
			Cala.info("Selected a new country");

			$('#city').html('<option value="#">--</option>');

			if($('#country').val() == 0){
				Cala.info('Tis online');
				$('#locationDiv').slideUp();
				$('#region').html('');
				$('#city').html('');
			}

			Cala.c_regionsGen(
				$(where).val(),
				function(options){
					Cala.say("Got the regions");
					$(regionSelect).html(options);
				}
			);
		});
	},

	// This is hard coded, it will evolve
	c_worldSetRegionForchange(
		citySelect = '#city',
		regionSelect = '#region',
		countrySelect = '#country'
	){
		Cala.say('Auto set cities on region change');
		// Generate the cities list based in the country/region
		$(regionSelect).change(function(){
			Cala.info("New region selected");

			var region = $(regionSelect).val();
			if(region == '#'){
				$('#city').html('<option value="#">--</option>');
				return false;
			}

			region = region.split('|');

			Cala.c_cityGen(
				$(countrySelect).val() +"_"+ region[0],
				function(cities){
					Cala.info("Got them cities!");
					$(citySelect).html(cities);
				});
		});
	},

}

var World = {

	s_countries: '#country',
	s_regions: '#region',
	s_cities: '#city',
	onlineText: 'Online',
	otherText: 'Other/Non Listed',

	say(w){
		console.log('World: ' + w);
	},

	selectGenWorld(d = {}){

		World.s_countries = d.countries !== undefined ? '#'+d.countries : World.s_countries;
		World.s_regions   = d.regions   !== undefined ? '#'+d.regions   : World.s_regions;
		World.s_cities    = d.cities    !== undefined ? '#'+d.cities    : World.s_cities;

		World.say('Generating a country list in: ' + World.s_countries);

		var select = '';
		if(d.online !== undefined){ select += `<option value="onl" selected>${World.onlineText}</option>` }

		for(var i = 0; i < allCountries.length; i++){ select = select + '<option value="'+allCountries[i][0]+'">'+allCountries[i][1]+'</option>' }

		select = select + `<option value="oth">${World.otherText}</option>`;

		$(World.s_countries).html(select);

		// Generate the regions list based in the country
		$(World.s_countries).on('change', function(){

			World.say("Selected a new country");

			$(World.s_regions).html('<option value="#">--</option>');

			if(d.onChange !== undefined) d.onChange()

			if($(World.s_countries).val() == 'onl'){
				World.say('Tis online');
				$(World.s_regions).html('<option value="#">--</option>');
				$(World.s_cities).html('<option value="#">--</option>');
				return false;
			}

			World.selectGenRegions(d);
		});

		// Auto generate a cities list on regions change
		$(World.s_regions).on('change', function(){
			World.say('Region changed');
			if($(World.s_regions).val() == '#'){
				$(World.s_cities).html('<option value="#">--</option>');
				return false;
			}

			World.selectGenCities();
		});

		if(d.success !== undefined) d.success()

	},

	selectGenRegions(d){

		World.s_countries = d.countries !== undefined ? '#'+d.countries : World.s_countries;
		World.s_regions   = d.regions   !== undefined ? '#'+d.regions   : World.s_regions;
		World.s_cities    = d.cities    !== undefined ? '#'+d.cities    : World.s_cities;

		World.say('Generate regions list in: ' + World.s_regions);

		// Clean cities

		$(World.s_cities).html('<option value="#">--</option>');

		var r = $(World.s_countries).val();

		if(r == 'onl'){return false}

		$.ajax({
			url: 'web/world/regions/'+r+'.json',
			dataType: 'json',
			success: function(d){
				var select = '<option value="#">--</option>';
				for(var i = 0; i < d.length; i++){
					select = select + '<option value="'+d[i][0]+'|'+d[i][1]+'">'+d[i][1]+'</option>';
				}
				$(World.s_regions).html(select);
				if(d.success !== undefined) d.success(select);
			},
			error: function(){
				if(d.err !== undefined) d.err();
			}
		}).done(function(d) {
			if(d.success !== undefined) d.success(select);
		});
	},

	selectGenCities(success){
		World.say('Generate a cities list');
		var c = $(World.s_countries).val();
		var r = $(World.s_regions).val();
		r  = r.split('|');

		$.ajax({
			url: 'web/world/cities/'+c+'/'+c+"_"+r[0]+'.json',
			dataType: 'json',
			success: function(d){
				var select = '<option value="#">--</option>';
				for(var i = 0; i < d.length; i++){
					select = select + '<option value="'+d[i][0]+'">'+d[i][0]+'</option>';
				}
				$(World.s_cities).html(select);
				if(success !== undefined) success(select);
			},
			error: function(){
				if(err !== undefined) err();
			}
		}).done(function(d) {
			if(success !== undefined) success(select);
		});
	},

}

/***************************************************************************
 *
 * Local storage
 *
 * @todo Move inside Cala ?
 *
 */
var Keys = {

	// Store keys
	store: function(key, value){
		Cala.info("Storing key: " + key, 'cala');
		window.localStorage.setItem(key, value);
		return true;
	},

	// Get key from local storage
	get: function(key, defaultValue){
		Cala.say("Loading key: " + key);
		var value = window.localStorage.getItem(key);
		if(value === null){
			Cala.warn("No value found, I will use the default", 'cala');
			value = defaultValue;
		}
		return value;
	},

	// Remove key from local storage
	remove: function(theKey){

		// Remove them all
		if(theKey === ''){
			Cala.info("Removing all keys", 'cala');
			localStorage.clear();
		}else{
			Cala.info("Removing key: " + theKey, 'cala');
			window.localStorage.removeItem(theKey);
		}
	},
}

