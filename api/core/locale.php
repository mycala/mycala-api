<?php

/**
 * Locale functions
 */

# The global object with the translations
global $locale;

# Pseudo-Boot
_locale_loadCurrent();

/**
 * Load locale files
 */
function locale_load(
    $module = false
) {
    grace_debug('Load locale file for: ' . $module);
    $locale = conf_get('locale', 'core', 'en_UK');
     
    # Try to get the file
    if (modules_loader($module, "locale/{$locale}.php", false)) {
        return true;
    } else {
        # Get the default language if not found
        if (!modules_loader($module, "locale/{$locale}.php", false)) {
            return false;
        }
    }
    return true;
}

/**
 * Load the default language packs
 */
function locale_loadDefault()
{
    $packs = conf_get('langsDefault', 'core', []);

    foreach ($packs as $l) {
        locale_load($l);
    }
}

/**
 * Translate a file
 */
function locale_trans(
    $module,
    &$c
) {
    global $locale;

    # Load the locale, just in case
    locale_load($module);

    # Find all translatable patterns
    preg_match_all("/\{.*?\}/", $c, $matches);
     
    $m = $matches[0];
    foreach ($m as $match) {
        $key = str_replace('{', '', $match);
        $key = str_replace('}', '', $key);
        $parts = explode('_', $key);

        # It should have an specific form
        if (count($parts) > 1) {
            if ($string = locale_l($key)) {
                # They might not be translation strings, just text. Lets avoid error/warning messages
                $c = @str_replace($match, $string, $c);
            }
        }
        //$c = @str_replace($match, $locale[$parts[0]][$parts[1]], $c);
    }
}

/**
 * Translate a string, or use a translated string
 */
function locale_l(
    $s
) {
    global $locale;

    $parts = explode('_', $s);

    if (isset($locale[$parts[0]][$parts[1]])) {
        return $locale[$parts[0]][$parts[1]];
    }

    return $s;
}

/**
 * Load current locale in the system
 */
function _locale_loadCurrent()
{

    # Check for cookie
    if ($cookie = _users_cookieGet('locale')) {
        if (isset($cookie['locale'])) {
            conf_set('locale', 'core', $cookie['locale'], true);
        }
    }

    $locale = conf_get('locale', 'core', 'en_UK');

    # This has to be a reference
    $l = [];
    hooks_meUp('locale_init', [&$l]);
}

/**
 * Set a new locale
 */
function _locale_setCurrent(
    $locale
) {

    # First run the cookie is not ready
    conf_set('locale', 'core', $locale);

    _users_cookieUpdate(['locale' => $locale], 'locale', true);
}

/**
 * Get current locale
 */
function _locale_getCurrent()
{
    if ($cookie = _users_cookieGet('locale')) {
        if (isset($cookie['locale'])) {
            return $cookie['locale'];
        }
    }

    return conf_get('locale', 'core', 'en_UK');
}
