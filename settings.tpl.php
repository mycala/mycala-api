<?php

# Turn off all error reporting
ini_set('display_errors', 0);
error_reporting(0);

#
# If everything is fine, all you need to configure for a basic, not so secure
# installation is the database details.
# You may start there and once you feel comfortable, you may adjust
# other aspects of the installation for security.
#

# Declare it as global, but never use it as global
global $config;

################################################################################
#
# Database
#
################################################################################
# Database name
$config['db']['name'] = '';
# Database user name
$config['db']['user'] = '';
# Database password
$config['db']['pwd'] = '';
# Database host
$config['db']['host'] = '';

################################################################################
#
# Core and Modules
#
################################################################################
#
# Paths USE TRAILING SLASHES!!!
#
# By default I will assume that core modules and contrib are located in the
# same directory as the Api, but they can be placed anywhere else
# This is just for simplicity

$baseDir = __DIR__ . '/';

# This domain
# @optimize If you want to save a few milliseconds just set the correct path here
$subDir = dirname($_SERVER['SCRIPT_NAME']);
$subDir = $subDir == '/' ? '/' :  "$subDir/";
$config['core']['thisDomain'] = "{$_SERVER['REQUEST_SCHEME']}://{$_SERVER['HTTP_HOST']}{$subDir}";

# Absolute path to the core MyCala_Api installation.
# Is the directory called 'api' just like that.
# Use trailing slash!
$config['modules']['coreInstall'] = $baseDir . 'api/';

# Time in seconds for the lifetime of a session, after this time, the user must
# log back in. 1 hour is default. 0 means ethernal (NOT recomended).
$config['users']['sessionLifetime'] = 0;

# If you want to allow CORS requests
$config['core']['cors'] = true;

# TMP directory, for diverse uses, it will be emptied every now and then
# @todo empty this directory
$config['core']['tmpDir'] = $baseDir . 'tmp/';

# A private token in order to run cron or other protected calls
# Change it!!!!!
$config['core']['cronToken'] = 'ItIsGoodIfThisIsBigAndHasW3irDLeeT3rsAnd$ymb0lz.IniT';

# Location for PHP`s error messages
# The file will be called php_err.log
$config['core']['phpErrDir'] = $config['core']['tmpDir'];

# Default call
$config['core']['defaultCall'] = 'cala_default';

# Run mode
# Constants are not available yet
$config['core']['runningMode'] = 'json'; //CALA_MODE_RUN_JSON;

##############################################################################
#
# Skin
#
#####################################################################################

/**
 * The default skin name
 */
$config['core']['skin'] = 'basic';

/**
 * The skins path
 */
# The default skin
$config['core']['skin'] = 'b';
$config['core']['skinPath'] = $config['modules']['coreInstall'] . 'skin/';
# You may use a subdomain or other setting you may like
$config['core']['skinPublicPath'] = $config['core']['thisDomain'] . 'skins/';

# Avatars
$config['users']['avatarPublicPath'] = $config['core']['thisDomain'] . 'files_public/avatars/';

# Cookie
$config['core']['cookie'] = [
    'name'     => 'cala',
    'expires'  => 3600,
    'path'     => $subDir,
    'domain'   => $_SERVER['HTTP_HOST'],
    'secure'   => false,
    'httponly' => true,
    'samesite' => 'Strict'
];

##############################################################################
#
# Grace
#
#####################################################################################

# Set to 'true' if you actually want me to store a log of errors/messages
$config['grace']['logs'] = false;

# Where do you want me to store the logs? USE TRAILING SLASH!
# Log path. This does not matter if 'logs' is false.
$config['grace']['logPath'] = $baseDir . 'logs/';

# You may also have logs per user in individual folders with their userId
$config['grace']['logPerUser'] = true;

# Should I display log messages on the screen?
$config['grace']['display'] = false;

# Core modules location, you probably do not need to touch this.
$config['modules']['corePath'] = $config['modules']['coreInstall'] . "modules/";
# Contributed modules, they can stay where they are.
$config['modules']['contribPath'] = $config['modules']['coreInstall'] . "contrib/";
# Resources such as 404 not found images and such, they are not really used at the moment.
$config['core']['resourcesPath'] = $config['modules']['coreInstall'] . 'resources/';

# Location to upload files, USE TRAILING SLASH!!
# Each user will have its own directory within this path
$config['files']['dir'] = $config['modules']['coreInstall'] . 'files/';

# Public files
$config['files']['public'] = $config['modules']['coreInstall'] . 'files_public/';

# The cost for user password hassing
# This value MUST be adjusted, 10 is a good start usually.
$config['core']['hash_cost'] = 10;

/*******************************************************************************
 * You should not need to touch anything beyond this point.
 */

# List of core modules
$config['modules']['core'] = array('cala','db','users','files','logger','sqlite');
# List of core modules to load always, you can overide this list
$config['modules']['coreLoad'] = array('cala','db','logger','users','files');
# List of libraries to load
# Remove skin if you will use it as a REST application
$config['libraries']['coreLoad'] = ['locale', 'skin'];
