<?php

/** @file examples_help.php
 * Examples for the code
 *
 * @author Twisted Head
 */

/**
 * \defgroup Examples
 * @{
 */

/**
 * Example form
 *
 * This is an example form.
 * You can actually use any name that you want, standard is _form_FORMNAME_get()
 *
 */
function _form_formName_get(
    $values = false //!< An array with the values, use 'false' if it is an empty form
) {
    global $locale, $user;

    /**
     * The entire form in an array
     */
    $form = array(

         //<! The main form information
         'form' => array(
             // Method used, default (recommended) is POST
             'method' => 'POST',
             // Action to take when the form is submmited
             'action' => 'manager_business',
             // Id of the form, use this to verify in your hooks if this is the form you are looking for
             'id' => 'manager_business',
             // The main id column in the table, this is used for updates
             'idColumn' => 'idBusiness',
             // The table for this form
             'table' => 'k_business',
             // Class(es) to put in the form
             'class' => 'pure-form',
             // Submit via ajax? I will use the function Cala.formSubmit
             'ajax' => true
         ),

            // The values, they will be automatically put by Cala
            'values' => $values,
            'fields' => array(
                // The name of the input field, must be THE SAME as the column in the db
                'name' => array(
                    // Type of input input(which is actually text, but I screwed up), textarea, hidden, select
                    'type' => 'input',
                    // The maximum allowed size, characters
                    'maxsize' => '250',
                    // Add one or more clases
                    'class' => 'full',
                    // Required? ANY value will make it look like true, default is false
                    'required' => true,
                ),
                'name' => array(
                    'type' => 'textarea',
                    'class' => 'full',
                    'required' => true
                ),
                'contact' => array(
                    'type' => 'textarea',
                    'required' => true,
                    'class' => 'full'
                ),
                'postCode' => array(
                    'type' => 'input',
                    'maxsize' => '25',
                    'value' => 0,
                    'required' => true,
                ),
                'country' => array(
                    'type' => 'select',
                    'maxsize' => '50',
                    'style' => 'width: 300px;',
                    'required' => true
                ),
                'region' => array(
                    'type' => 'select',
                    'required' => false,
                    'options' => ['-|-'],
                    'style' => 'width: 250px;',
                    'value' => ''
                ),
                'city' => array(
                    'type' => 'select',
                    'required' => false,
                    'options' => ['-|-'],
                    'style' => 'width: 250px;',
                    'value' => ''
                ),
                'created' => array(
                    'type' => 'hidden',
                    'required' => false,
                    'value' => time()
                ),
                'lastUpdate' => array(
                    'type' => 'hidden',
                    'required' => false,
                    'value' => time()
                ),
                'category' => array(
                    'type' => 'select',
                    'required' => true,
                    'options' => []
                ),
                'subCategory' => array(
                    'type' => 'select',
                    'required' => true,
                    'options' => []
                ),
                'contactHidden' => array(
                    'type' => 'select',
                    'required' => false,
                    'options' => [
                        '1|'.$locale['manager']['contactHidden'],
                        '0|'.$locale['manager']['contactNotHidden']
                    ]
                ),
                'lang' => array(
                    'type' => 'select',
                    'required' => true,
                    'options' => [
                        'es_ES|Español',
                        'en_UK|English'
                    ]
                ),
                'status' => array(
                    'type' => 'select',
                    'required' => true,
                    'options' => [
                        '0|'.$locale['manager']['statusInactive'],
                        '1|'.$locale['manager']['statusActive']
                    ]
                ),
                'video' => array(
                    'type' => 'input',
                    'maxsize' => '250',
                    'placeholder' => 'https://vimeo.com/474275730'
                ),
                'idUser' => array(
                    'type' => 'hidden',
                    'required' => false,
                    'value' => $user['idUser']
                ),
                'code' => array(
                    'type' => 'hidden',
                    'required' => false,
                    'value' => '0'
                ),
                'idBusiness' => array(
                    'type' => 'hidden',
                    'required' => false,
                    'ignore' => true,
                    'value' => 0
                ),
                'submit' => array(
                    'type' => 'submit',
                    'class' => 'pure-button success block_on_send',
                    'value' =>  $locale['manager']['editButton']
                )
            )
        );

    # Edit form
    if ($values) {
        //$form['form']['action'] = 'manager_edit_business';
    }

    # Parse and skin the form
    $form = forms_get($form, modules_getPath('manager') . 'skins/businessEditForm', 'manager');

    # Countries
    skin_scriptAdd('
		$(document).ready(function(){
			Cala.say("Countries");
			Enmilista.worldGen();	
		});
	', 'text', 'footer');

    return $form;
}

/**
 * Init function/hook.
 * Each module needs this function in order to indicate its paths
 */
function MODULE_NAME_init()
{

     # A list of paths that your module will use
    # they are done like this: http://yoursite/?w=baseModule_say_hello&param1=val
    return [
          [
                # Request parameter like 'MODULE_NAME_blog_number'
                # They ALLWAYS start with 'MODULE_NAME_' because that is how
                # MyCala knows to look for your module
                'r' => 'MODULE_NAME_say_hello',

                # The action to perform, aka the function I will call
                # I will pass the params (see bellow) to this function
                'action' => 'MODULE_NAME_sayHello',

                # Access function to call and check for this request
                # You may define your own or use the ones provided by MyCala
                # users_openAccess lets ANYONE access this request
                 # users_isLoggedIn checks if the request is done by a logged in
                # user, in this case extra params 'iam' and 'token' are required,
                # but you do not need to indicate them here, that is automatic
                'access' => 'users_openAccess',

                # Any parameters you wish to send to the access function
                'access_params' => 'accessName',

                # None, one or more params that you need for this request
                # They will be passed IN THIS SAME ORDER to the function that you indicate in 'action'
                'params' => [
                     [
                          # Parameter name
                          'key' => '',

                          # Default value if one is not supplied, only applies if not required
                          'def' => '',

                          # Required? If false and not provided in request I will
                          # use the default value, if true and not provided in
                          # request I will return error and not proceed with the request
                          'req' => true]
                             ],

                     # File where the function to call is located
                     # Always put the functions in a different file, at least as much
                     # as possible
                     'file' => 'file.php'
                ]
          ];
}

/**
 * Get the perms for this module.
 * Not fully implemented yet.
 */
function MODULENAME_access()
{
    $perms = array(
          array(
                // A human readable name
                'name'        => 'Do something with this module',
                // Something to remember what it is for
                'description' => 'What can be achieved with this permission',
                // Internal machine name, no spaces, no funny symbols, same rules as a variable
                // Use yourmodule_ prefix
                'code'        => 'mymodule_access_one',
                // Default value in case it is not set
                'def'        => false, //Or true, you decide
          ),
     );
}



/** @} */
