<?php

global $colours;

/**
 * Cli colours
 * Thanks! https://www.if-not-true-then-false.com/2010/php-class-for-coloring-php-command-line-cli-scripts-output-php-output-colorizing-using-bash-shell-colors/?PageSpeed=noscript
 */
class Colours
{
    private $f_colours = array();
    private $b_colours = array();

    public function __construct()
    {
        // Set up shell colours
        $this->f_colours['black'] = '0;30';
        $this->f_colours['dark_gray'] = '1;30';
        $this->f_colours['blue'] = '0;34';
        $this->f_colours['light_blue'] = '1;34';
        $this->f_colours['green'] = '0;32';
        $this->f_colours['light_green'] = '1;32';
        $this->f_colours['cyan'] = '0;36';
        $this->f_colours['light_cyan'] = '1;36';
        $this->f_colours['red'] = '0;31';
        $this->f_colours['light_red'] = '1;31';
        $this->f_colours['purple'] = '0;35';
        $this->f_colours['light_purple'] = '1;35';
        $this->f_colours['brown'] = '0;33';
        $this->f_colours['yellow'] = '1;33';
        $this->f_colours['light_gray'] = '0;37';
        $this->f_colours['white'] = '1;37';

        $this->b_colours['black'] = '40';
        $this->b_colours['red'] = '41';
        $this->b_colours['green'] = '42';
        $this->b_colours['yellow'] = '43';
        $this->b_colours['blue'] = '44';
        $this->b_colours['magenta'] = '45';
        $this->b_colours['cyan'] = '46';
        $this->b_colours['light_gray'] = '47';
    }

    /**
     * Returns coloured string.
     */
    public function c(
        $string,
        $f_colour = null,
        $b_colour = null
    ) {
        $c_string = "";

        // Check if given foreground colour found
        if (isset($this->f_colours[$f_colour])) {
            $c_string .= "\033[" . $this->f_colours[$f_colour] . "m";
        }
        // Check if given background colour found
        if (isset($this->b_colours[$b_colour])) {
            $c_string .= "\033[" . $this->b_colours[$b_colour] . "m";
        }

        // Add string and end colouring
        $c_string .=  $string . "\033[0m";

        return $c_string;
    }

    // Returns all foreground colour names
    public function getFColours()
    {
        return array_keys($this->f_colours);
    }

    // Returns all background colour names
    public function getBColours()
    {
        return array_keys($this->b_colours);
    }

    /**
     * I will print all possible combinations.
     */
    public function testAll()
    {
        print_r($this->b_colours);
        foreach ($this->b_colours as $bKey => $bValue) {
            foreach ($this->f_colours as $fKey => $fValue) {
                print($this->c("Testing colours [$fKey] on ($bKey)   ...", $fKey, $bKey));
                print("\n");
            }
        }
    }

    /**
     * Print stuff (plain and boring) in the screen.
     */
    public function p($w, $nl = true)
    {
        print($w . ($nl ? "\n" : ''));
    }

    /**
     * Print coloured stuff in the screen.
     */
    public function pc(
        $w, /**< What will you say */
        $c = 'white', /**< Text colour */
        $b = 'black' /**< Background colour */
    ) {
        print($this->c("$w", $c, $b));
        print("\n");
    }

    /**
     * Print a warning message.
     */
    public function warning($w)
    {
        print($this->c("$w", 'white', 'red'));
        print("\n");
    }

    /**
     * Print a warning message.
     */
    public function info($w)
    {
        print($this->c("$w", 'white', 'blue'));
        print("\n");
    }

    /**
     * Print an alert message.
     */
    public function alert($w)
    {
        print($this->c("$w", 'black', 'yellow'));
        print("\n");
    }
}

# Make them accessible via functions
$colours = new Colours();
