/**

**/

# MyCala Framework (Like)

It is slowly growing to be something like a Framework...

Developed with mobile apps in mind but it can also work for stand alone sites.

## Dependencies
* PHP: 7.2 (Maybe less, but this will keep you safe)
* Percona, MariaDB or Mysql
* ImageResize by Eventviva https://github.com/eventviva/php-image-resize

How to call via URL

w = request to do something

?w=cala_default

If you need to provide more information go like this:

?w=cala_default&customParam_1=info&customParam_2=moreInfo

Use as many params as you need.



