<?php

/** @file core/tools.php
 * Tools
 */

/** \addtogroup Core
 *  @{
 */

/**
 * \defgroup Tools
 * @{
 */

/**
 * Reply 'to the world' the results of this path.
 * I will finish/exit.
 */
function tools_reply(
    $response = '',
    $killMe = false
) {
    if ($killMe) {
        /* do something some day */
    }

    # I will reply in json by default
    $rt = params_get('replyType', CALA_REPLY_JSON);

    # A bit tricky, but if the error is a 404 || access_denied then the reply might need to be
    # skinned, even if the request
    if ($err = tools_errIsSet()) {
        if ($err == CALA_ERR_PATH_NOT_FOUND
             || $err == CALA_ERR
             || $err == CALA_ERR_BAD_REQUEST) {
            if (conf_get('runningMode', 'core', CALA_MODE_RUN_JSON) == CALA_MODE_RUN_SKIN) {
                params_set('replyType', CALA_REPLY_SKIN);
                $rt = CALA_REPLY_SKIN;
            }
        }
    }

    if ($rt === CALA_REPLY_JSON) {
        if (tools_errIsSet()) {
            _tools_reply(tools_returnJson(array('err' => tools_errIsSet())));
        } else {
            _tools_reply(tools_returnJson(array('resp' => $response)));
        }
    }
    if ($rt === CALA_REPLY_PLAIN) {
        if (tools_errIsSet()) {
            $response .= tools_errIsSet();
        }
        $response .= conf_get('mode', 'core', 'cli') == 'cli' ? PHP_EOL : '';
        _tools_reply($response);
    }
    if ($rt === CALA_REPLY_SKIN) {
        if ($err = tools_errIsSet()) {
            if ($err == USERS_ERR_NOT_LOGGED_IN) {
                tools_goto('users_login_page');
                exit;
            }
            $response = skin_err($err);
        }

        $response = skin_me($response);

        # Cache?
        if (($cacheName = conf_get('cacheThis', 'cache', 0)) !== 0) {
            grace_debug('I shall cache this');
            include_once("cache.php");
            cache_store($response, $cacheName);
        }

        _tools_reply($response);
    }

    #@todo Create function for this
    if ($rt === CALA_REPLY_ATTACH) {
        if (tools_errIsSet()) {
            $response = tools_errIsSet();
            _tools_reply($response);
        }

        _tools_reply($response, true);

        @header('Access-Control-Allow-Origin: *');
        @header('Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With');
        @header('Access-Control-Allow-Methods: GET, PUT, POST');
        header('Content-Disposition: attachment; filename='.basename($response));
        header('Content-Type: application/octet-stream');
        header('Content-Length: ' . filesize($response));
        readfile($response);
        exit;
    }

    # Actually reply
    _tools_reply($response);
}

/**
 * Helper function to actually reply.
 * Do not call directly.
 */
function _tools_reply(
    $response, //!< The response to send
    $closeOnly = false //!< Only close, but do not actually reply
) {
    global $totalQueries, $user, $myCala_request;

    # Print the response
    if (!$closeOnly) {
        print $response;
    }


    # Some loging information
    # HTML replies are TOO big for a log
    if (params_get('replyType', 'core', CALA_REPLY_JSON) == CALA_REPLY_JSON) {
        grace_debug('I will reply [resp]'  . $response . '[/resp]]');
    } else {
        grace_debug('I will reply htmlContent or plain (literally)');
    }

    $totTime = microtime(true) - $_SERVER["REQUEST_TIME_FLOAT"];
    $q = grace_queries('tot');

    $memoryUsage = (memory_get_peak_usage() / 1000000);

    grace_debug("Finished! Memory: $memoryUsage Mb | Time: $totTime Seconds | Queries: $q \n<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<");

    # Nothing else will go in the log
    grace_talkStop();

    # Debuging, performance and testing information.
    # @todo this should have its own function
    #
    # mb_strlen would be better to get the actual size in bytes, but those functions
    # are a lot slower than just strlen, it is better to get those calculations
    # afterwards and not affect the response times here.
    $f = conf_get('mode', 'core', 'web');
    $fp = conf_get('tmpDir', 'core', '');
    //$req = conf_set('mode', 'core', 'web') ? $_SERVER['QUERY_STRING'] : '';
    if ($fp != '') {
        file_put_contents(
            $fp . $f.'_debug.log',
            sprintf(
                "%s|%s|%s|%s|%s|%s|%s|%s|%s|%s\n",
                time(),
                params_get('w', ''),
                $memoryUsage,
                $totTime,
                $totalQueries,
                tools_errIsSet(),
                $user['idUser'],
                strlen(JSON_encode($myCala_request)),
                strlen($response),
                isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : ''
            ),
            FILE_APPEND
        );
    }

    if (!$closeOnly) {
        exit;
    }
}

/**
 * Return a json of anything you want
 */
function tools_returnJson($r, $addHeaders = true)
{
    if ($addHeaders && conf_get('mode', 'core', 'web') != 'cli') {
        @header('Content-Type: application/json');
        tools_addHeaders(true);
    }
    return json_encode($r);
}

/**
 * Add headers to the response.
 * Use this if you need cors (cross domain)
 */
function tools_addHeaders($cors = false)
{
    @header('Content-Type: text/html; charset=utf-8');

    if ($cors) {
        /*CORS to the app access*/
        @header('Access-Control-Allow-Origin: *');
        @header('Access-Control-Allow-Headers: Cache-Control, Pragma, Origin, Authorization, Content-Type, X-Requested-With');
        @header('Access-Control-Allow-Methods: GET, PUT, POST');
    }
}

/**
 * Process the path according to what was requested.
 * @todo Setting for the function to recieve the params as a single array
 * makes it easier for functions with a lot of them.
 */
function tools_proccesPath(
    $paths,
    $request,
    $module
) {
    grace_debug("Looking for path: $request");

    foreach ($paths as $p) {
        if ($p['r'] == $request) {
            grace_debug("Found path: " . $p['r']);

            # Change the reply type
            if (isset($p['replyType'])) {
                grace_debug('Reply type: ' . $p['replyType']);
                params_set('replyType', $p['replyType']);
                # @todo this might become a module
                if ($p['replyType'] == 'skin') {
                    include_once("skin.php");
                    # Title
                    skin_setTitle(isset($p['title']) ? $p['title'] : '');
                }
            }

            # Check for permissions
            # If a required access_param was not sent then I will stop here
            $p['access_params'] = isset($p['access_params'])
                ? $p['access_params']
                : false;

            if (isset($p['access'])) {
                grace_debug("Access parameter is: " . $p['access']);

                $access = call_user_func($p['access'], $p['access_params']);

                # Stop if the access failed
                if (tools_errIsSet()) {
                    return false;
                }
                /*
                                     # Prepare the access params for the function according
                                     # to what was provided in the request settings
                                     $args = array();

                                     # Almost certain that this is wrong, they should just be a param that is set
                                     # and not recieved, can't remember why I did it like this
                                     if ($p['access_params']) {
                                           grace_info('There are access params: ' . JSON_encode($p['access_params']));
                                           # Check access params and die if a required one is missing
                                           # @todo maybe I should die here, not there
                                           params_verifyRequest($p['access_params']);

                                           # Die if any params is missing
                                           if (tools_errIsSet()) {
                                                return false;
                                           }

                                           # Access params where sent, lets prepare them to call the function
                                           foreach ($p['access_params'] as $param => $pp) {
                                                $args[] = params_get($pp['key'], '');
                                           }
                                     }

                                     $access = call_user_func_array($p['access'], $args);

                                     # Stop if the access failed
                                     if (tools_errIsSet()) {
                                           return false;
                                            }
                 */
            }

            # Confirm the params required for this request.
            # Add the default ones or stop if required ones are missing.
            if (isset($p['params'])) {
                grace_info('There are params indicated for this request');
                # Check params and die if a required one is missing
                # @todo maybe I should die here, not there
                params_verifyRequest($p['params']);

                # Stop if there are missing params
                if (tools_errIsSet()) {
                    return false;
                }
            }

            # Check for token
            if (isset($p['useToken'])) {
                grace_debug('Token required');

                tools_confirmToken();

                # Stop if there are errors with the token
                if (tools_errIsSet()) {
                    return false;
                }
            }

            // Load the correct file
            if (isset($p['file'])) {
                modules_loader($module, $p['file']);
            }

            if (function_exists($p['action'])) {
                grace_debug("Found function: " . $p['action']);

                # Prepare the params for the function according to what was
                # indicated in the request settings
                $args = array();
                if (isset($p['params'])) {
                    foreach ($p['params'] as $param => $pp) {
                        $args[] = params_get($pp['key'], '');
                    }
                }

                # Lets set a good mark so it is easy to see in the log
                grace_info(
                    '<<<<<<<<<<<<---------------Process start----------------->>>>>>>>>>>>'
                );
                grace_debug('Requested process' . $p['action'] . '/');

                return call_user_func_array($p['action'], $args);
            }
            # @todo: continue; break; return true; ??
        }
    }

    return tools_errSet(
        'Function does not exist. Path not found.',
        CALA_ERR_PATH_NOT_FOUND
      );
}

/**
 * Load tools from the toolbox
 */
function tools_useTool($which)
{
    static $toolsLoaded = array();
    $path = conf_get("coreInstall", "modules", "") . "tools/$which";

    # Do not load it again if it was already loaded
    if (in_array($path, $toolsLoaded)) {
        grace_info('Tool already loaded, nothing else to do.');
        return true;
    }

    # Store the module in the loaded list
    $toolsLoaded[] = $path;

    grace_info("Loading tool: " . $path);

    if (file_exists($path)) {
        include_once($path);
        return true;
    } else {
        grace_error('I could not find the tool');
        return false;
    }
}

/**
 * Load core libraries
 */
function tools_loadLibrary($which)
{
    global $config;

    $path = conf_get('coreInstall', 'modules', '') .  "core/" .$which;

    grace_debug("Loading library: " . $path);

    if (file_exists($path)) {
        grace_debug("Found library");
        include_once($which);
        return true;
    } else {
        return false;
    }
}

/**
 * Confirm the 'secret' token.
 *
 * Lets give a vague error you can check your logs for a more precise answer.
 */
function tools_confirmToken()
{
    grace_info('Confirm access/cron token');

    # At least for now, cli will bypass this
    if (conf_get('mode', 'core', 'web') == 'cli') {
        return true;
    }

    $t = conf_get(
        'cronToken',
        'core',
        'ItIsGoodIfThisIsBigAndHasW3irDLeeT3rsAnd$ymb0lz.IniT'
    );

    # Default token is insecure, it should never be used.
    if ($t == 'ItIsGoodIfThisIsBigAndHasW3irDLeeT3rsAnd$ymb0lz.IniT'
        || $t == '') {
        return tools_errSet(
            'You either do not have a secret token or have not changed the default one! That is not good.',
            'ERR_USERS_ACCESS_DENIED'
        );
    }

    # Now confirm the one sent
    if (params_get('cronToken', '') != $t) {
        return tools_errSet(
            'Wrong token sent in the request',
            'ERR_USERS_ACCESS_DENIED'
        );
    }

    return true;
}

/**
 * Set the response to be an error.
 *
 * @e error message that you can log
 * @err an error code that you may set to be returned
 *
 * So you can do one single step: return tools_errSet('Function does not exist.', 'ERROR_FUNCTION_NOT_FOUND');
 *
 */
function tools_errSet(
    $e = false,
    $err = 'ERR'
) {
    grace_error('This request ended in err: ' . $e . ' && ' . $err);
    conf_set('errTrue', 'core', $err);
    return $err;
}

/**
 * Is this response an error?
 */
function tools_errIsSet()
{
    $err = conf_get('errTrue', 'core', false);

    if ($err != false) {
        return $err;
    }
    return false;
}

/**
 * Go to a different page.
 */
function tools_goto(
    $where = ''
) {
    grace_debug('Going away...' . $where);

    params_set('replyType', 'plain');

    # Internal?
    if (strpos($where, 'http') == 0) {
        $where = '?w=' . $where;
    }

    # Close the call
    _tools_reply('', true);

    # I will just print a 'html' page with a refresh code
    echo('<html><meta http-equiv="refresh" content="0; url='.$where.'"></html>');
    exit;
}

/**
 * Check for errors in responses.
 *
 * @param $r the response
 * @return true if it was an err, in the new system array('err' => 'the_err') or false if no err
 */
function tools_connectCheckErr($r)
{
    grace_debug('Lets see if this is an actual response or an error');

    # This must happen before parsing it with JSON
    if (trim($r) == '') {
        grace_error('Error in response, nothing came back: ' . $r);
        return true;
    }

    $r = JSON_decode($r);

    # Old system
    if (is_numeric($r) && $r < 0) {
        grace_error('Error in response, numeric: ' . $r);
        return true;
    }

    # New system
    if (isset($r->err)) {
        grace_error('Error in response: ' . $r->err);
        return $r;
    }

    return false;
}

/**
 * Filter text to make it secure.
 *
 * This is similar to tools_stripTags and it calls that function, but in this case the entities may
 * be reverted with tools_textUnFilterSafe()
 */
function tools_textFilterSafe(
    $text,
    $db = false, # Prepare for db
    $tags = false # Strip tags
) {
    if ($tags) {
        $text = tools_stripTags($text, false, $db);
    } elseif ($db) {
        $text = db_escape($text);
    }

    $text = htmlentities($text);

    return $text;
}

/**
 * Unfilter text to make it unsecure.
 */
function tools_textUnFilterSafe(
    $text,
    $db = false
) {
    return html_entity_decode($text);
}

/**
 * Remove html tags, with exceptions
 */
function tools_stripTags(
    $text,
    $tags = false,
    $db = false // Also prepare for DB?
) {
    grace_debug('Strip tags');

    # Default tags
    if (!$tags) {
        $tags = '<p><a><i><ul><li><h1><h2><h3><u><b>';
    }

    $text = strip_tags($text, $tags);

    return $db ? db_escape($text) : $tex;
}

/**
 * Generate a secret.
 *
 * @todo use php's built in function maybe?
 * https://www.php.net/manual/en/function.array-rand.php
 */
function tools_secretGenerate(
    $size = 16,
    $letters = false
) {

    # A set of letters and symbols.
    $_letters = [
        'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
        'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','y','Z',
        '1','2','3','4','5','6','7','8','9','0',
        '!','¡','(',')','*','/','%','@','#',']','[','{','}','-','_','~','.','$'
    ];

    $letters = !$letters ? $_letters : $letters;

    $pos = 0;
    $secret = '';
    for ($i = 0; $i < $size; $i++) {
        $pos = rand(0, (count($letters) - 1));
        $secret .= $letters[$pos];
    }

    return $secret;
}

/**
 * Encrypt/decrypt
 */
function tools_enc(
    $text,
    $what = 'enc',
    $key = false
) {
    $key = !$key ? conf_get('encSecret', 'core', '123') : $key;

    if ($what == 'enc') {
        $cipher_method = 'aes-128-ctr';
        $enc_key = openssl_digest($key, 'SHA256', true);
        $enc_iv = openssl_random_pseudo_bytes(
            openssl_cipher_iv_length($cipher_method)
        );

        $cryptedText = openssl_encrypt(
            $text,
            $cipher_method,
            $enc_key,
            0,
            $enc_iv
        )
        . "::" . bin2hex($enc_iv);

        unset($text, $cipher_method, $enc_key, $enc_iv);

        return $cryptedText;
    } else {
        list($text, $enc_iv) = explode("::", $text);

        $cipher_method = 'aes-128-ctr';

        $enc_key = openssl_digest($key, 'SHA256', true);

        $textClean = openssl_decrypt(
            $text,
            $cipher_method,
            $enc_key,
            0,
            @hex2bin($enc_iv)
        );

        unset($text, $cipher_method, $enc_key, $enc_iv);

        return $textClean;
    }
}

/**
 * Check and see if this is the front page
 */
function tools_isFront()
{
    global $myCala_requestW;
  
    if (_users_isLoggedIn()) {
        if ($myCala_requestW == conf_get('defaultCall', 'core', 'cala_default')) {
            return true;
        }
    } else {
        if ($myCala_requestW == conf_get('defaultCallLoggedIn', 'core', 'cala_default')) {
            return true;
        }
    }
    return false;
}
/**@}*/
/** @}*/
