# Cala Framework Like

Version: 1.1

It is slowly growing to be something like a Framework...
It will most likely remain as a micro-framework at most.

It is oriented mostly as a backend api that you can use via apps and similar things.

# Dependencies
* PHP: 7.x

## Optional (but they come with the distribution anyway)
* ImageResize by Eventviva https://github.com/eventviva/php-image-resize

Only if you use the 'visual' interface
* JQuery
* JQueryUi

# Basic usage

How to call via URL:

```w = request to do something```

```?w=cala_default```

If you need to provide more information go like this:

```?w=cala_default&customParam_1=info&customParam_2=moreInfo```

Use as many params as you need.

There is more information in the installation and developing documentation.

