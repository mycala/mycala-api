<?php

/**
 * Boot me up
 */
function cala_bootMeUp()
{
    grace_info("Cala was booted...");
}

/**
 * Init
 */
function cala_init()
{
    grace_debug("Cala is in the house!");

    $paths = array(
        array(
            # The requested action as it is sent to the url, this is open to anyone
            # it is actually requested with w=something, but here it is an 'r'
            'r' => 'cala_core',
            # The action to be done internally, the name of the function to execute
            'action' => 'cala_core',
            # Q
            # Type of access
            'access' => 'users_openAccess',
            # Params expected for this action
            # key = the key
            # def default value provided only if this is not a required param
            # req true|false if this value is omited and it is required I will exit with error
            # cli the cli key if this is available via cli
            'params' => array(
                array('key' => "w", 'def' => "cala", 'req' => true, 'cli' => "w"),
                array('key' => "iam", 'def' => '', 'req' => true, 'cli' => "i"),
                array('key' => "sessionKey", 'def' => '', 'req' => true, 'cli' => 's'),
                //array('key' => "replyType",  'def' => "json", 'req' => false, 'cli' => 't'),
                array('key' => "r", 'def' => "cala_empty", 'req' => false, 'cli' => "r")
            ),
        ),

          # A test call to see if I am correctly installed
          # at least functional.
          # You should better use `cala_test_install` but that one requires the
          # token and that might be more difficult at first.
          array(
            'r' => 'cala_hello',
            'action' => 'cala_hello',
            'access' => 'users_openAccess',
            'params' => array(
                array('key' => "default", 'def' => 'Default value', 'req' => false, 'cli' => "d"),
                array('key' => "test", 'def' => '', 'req' => true, 'cli' => "d")
            )
        ),

          # Default call to make if nothing else is indicated.
          array(
            'r' => 'cala_default',
            'action' => 'cala_default',
            'access' => 'users_openAccess',
            'params' => array()
            ),

        # This is an open access call, but you will need a special key to use it
        array(
            'r' => 'cala_test_install',
            'action' => 'cala_testInstallStart',
            'useToken' => true,
            'access' => 'users_openAccess',
            'params' => array(),
            'file' => 'testInstall.php'

        ),
          
          # Testing: open the testInstall in the browser
        array(
            'r' => 'cala_test_install_browser',
            'action' => 'cala_testInstallStartBrowser',
            'access' => 'users_openAccess',
            'file' => 'testInstall.php'
        ),

        # Clear the logs
        # @todo add maybe a time limit for only old logs
        # @todo move to a different file
        array(
            'r' => 'cala_grace_clear_logs',
            'action' => 'cala_graceClearLogs',
            'useToken' => true,
            'params' => array(
                array('key' => "graceSkip", 'def' => 'TRUE', 'req' => false)
            )
        ),

        # Grace view logs list
        array(
            'r' => 'cala_grace_logs_list',
            'action' => 'cala_graceLogsList',
            'useToken' => true,
            'params' => array(
                array('key' => "graceSkip", 'def' => 'TRUE', 'req' => false)
            )
        ),

        # Grace view log
        array(
            'r' => 'cala_grace_log_view',
            'action' => 'cala_graceLogView',
            'useToken' => true,
            'params' => array(
                array('key' => 'log', 'def' => 'latest', 'req' => false),
                array('key' => 'graceSkip', 'def' => true, 'req' => false)
            )
        ),

    );
    return $paths;
}

/**
 * Test call for n00bies.
 */
function cala_hello(
    $default,
    $test
) {
    if (params_get('replyType', 'json') == 'plain') {
        return "Your default value was: $default and Your test was: $test";
    }

    return array(
        'default' => "Your default value was: $default",
        'test' => "Your test was: $test");
}

/**
 * Default function to execute if nothing else was specified.
 * @todo reply with version number and other information.
 */
function cala_default()
{
    return 'ALL_GOOD';
}

/**
 * Dummy func.
 */
function cala_core()
{
    return "I don't actually do anything :(";
}

/**
 * Just for testing stuff, ignore this or use it as you wish.
 *
 * @todo Move to Grace.
 */
function cala_graceClearLogs()
{

    # I will just talk, I can't clear logs while I make them!
    print "Clearing the logs...\n";

    $dir = conf_get('logPath', 'grace');

    if (!$dir) {
        //print "No dir found $dir";
        return tools_errSet('No dir found', 'ERR_GENERAL');
    }

    //echo "Found dir: $dir \n";
    if (!file_exists($dir)) {
        //print "Directory does not exist \n";
        return tools_errSet('No dir found', 'ERR_GENERAL');
    }

    # Scan the dir
    $files = scandir($dir);
    if ($files) {
        //echo "Found files: " . count($files) . "\n";
        foreach ($files as $file) {
            # I will not delete the index.php, security
            if ($file === 'index.php' || $file === '.' || $file === '..') {
                continue;
            }
            unlink($dir.$file);
        }
    }
    # I need to exit in order to avoid creating more files
    exit;
}

/**
 * View a list of grace logs.
 *
 * @todo move this somewhere else, makes no sense here
 */
function cala_graceLogsList()
{
    $logPath = conf_get('logPath', 'grace', '');

    $files = array_diff(scandir($logPath, SCANDIR_SORT_DESCENDING), array('..', '.'));

    return $files;
}

/**
 * View an specific log.
 */
function cala_graceLogView($log)
{
    grace_debug('View a file log: ' . $log);

    $filePath = conf_get('logPath', 'grace', '');

    grace_debug("File requested: $filePath$log");

    if ($log == 'latest') {
        $files = scandir($filePath, SCANDIR_SORT_DESCENDING);
        $log = $files[0];
    }

    if (file_exists("$filePath$log")) {
        return file_get_contents("$filePath$log");
    }

    return tools_errSet(
        'File not found',
        'ERR_NOTHING_FOUND'
     );
}
