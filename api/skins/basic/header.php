<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML+RDFa 1.0//EN"
"http://www.w3.org/MarkUp/DTD/xhtml-rdfa-1.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" version="XHTML+RDFa 1.0" dir="ltr">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="description" content="">
		<link rel="shortcut icon" href="/favicon.ico"/>
		<title><?php print $skin['title'] ?></title>
		<!-- scripts -->
		<?php print $skin['header']['fileScripts'] ?>
	</head>
	<body>
