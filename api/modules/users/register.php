<?php

/**
 * Helper function to actually create a new user and register it in the db.
 *
 * @todo db_escape the settings, there should be a function to do this one by one or just do it
 * after it was json_encoded. But settings is not user-input so it could be a safe thing to just
 * let it be.
 */
function _users_register($u)
{
    grace_info('I will actually register someone');

    $q = sprintf(
        "INSERT INTO `cala_users` (`fullName`, `userName`, `email`, `about`,
		 `status`, `timestamp`, `lastAccess`, `pwd`, `avatar`,`settings`)
		 VALUES('%s','%s','%s','%s','%s','%s','%s','%s','%s','%s')",
        tools_textFilterSafe($u['fullName'], true, true),
        users_cleanName(tools_textFilterSafe($u['userName'], true, true)),
        db_escape($u['email']),
        tools_textFilterSafe($u['about'], true, true),
        $u['status'],
        $u['timestamp'],
        $u['lastAccess'],
        users_hash($u['pwd']),
        $u['avatar'],
        JSON_encode($u['settings'])
     );

    $r = db_exec($q);

    if ($r === 'ERROR_DB_ERROR') {
        return false;
    }
    return true;
}

/**
 * Register a new user.
 *
 * External call.
 */
function users_registerNew(
    $fullName,
    $userName,
    $email,
    $pwd,
    $about,
    $method
) {
    global $user;

    grace_debug("Register a user");

    # Does this account exit?
    $u = users_load($email);

    if (!$u) {

        # Wait! What about the username?
        $u = users_load($userName);

        if (!$u) {
            grace_debug("New user does not exist");
            $user = _users_register(
                array(
                    'fullName'   => $fullName,
                    'userName'   => $userName,
                    'email'      => $email,
                    'about'      => $about,
                    'status'     => 1,
                    'timestamp'  => time(),
                    'lastAccess' => time(),
                    'pwd'        => $pwd,
                    'avatar'     => '',
                    'settings'   => array()
                )
            );
            if (!$user) {
                return tools_errSet(
                    'There was an error with the new user',
                    'ERR_ERR'
                );
            }

            modules_loader('users', 'login.php', false);

            $r = users_logMeIn($userName, $pwd);

            // Hook users register
            // @deprecated
            //hooks_call('users_register', array(&$user));

            # Hook user_register V2
            # This has to be a reference
            hooks_meUp('users_register', [&$user]);

            if ($method == 'js') {
                return $r;
            } else {
                _users_cookieUpdate(_users_logIn($user), 'users', true);

                return 'ALL_GOOD';
            }
        } else {
            return tools_errSet(
                'User exists (userName)',
                'ERR_USERS_EXISTS'
              );
        }
    } else {
        return tools_errSet(
            'User exists (email)',
            'ERR_USERS_EXISTS'
         );
    }
}
