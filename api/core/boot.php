<?php

/** @file core/params.php
 * Params tools
 */

/**
 * \defgroup Core
 * @{
 */

define('CALA_VERSION', 0.7);

/**
 * @page
 */

include_once("grace.php");
include_once("params.php");
include_once("modules.php");
include_once("conf.php");
include_once("tools.php");
include_once("hooks.php");

/** @defgroup Constants
 *  @{
 */

//! Bad request done
define('CALA_ERR_BAD_REQUEST', 'ERR_BAD_REQUEST');

//!< Reply error, path not found
define('CALA_ERR_PATH_NOT_FOUND', 'ERR_PATH_NOT_FOUND');

//! Some generic error, if no real error happened, but nothing was found or the process did not go as espected
define('CALA_ERR', 'ERR_ERR');

# Reply types
//!< Reply in plain text
define('CALA_REPLY_PLAIN', 'plain');
//!< Reply in json form
define('CALA_REPLY_JSON', 'json');
//!< Reply in a skin
define('CALA_REPLY_SKIN', 'skin');
//!< Reply as attachment
define('CALA_REPLY_ATTACH', 'attach');

//!< Running mode is json
define('CALA_MODE_RUN_JSON', 'json');

//!< Running mode is skinned
define('CALA_MODE_RUN_SKIN', 'skin');

/** @}*/

//!< Global variable to hold the original request, the whole thing: ?w=a&=param=1...
global $myCala_request;
//!< The requested action ?w=what_what
global $myCala_requestW;

/**
 * Boot me up!
 */
function boot_engage(
    $mode = 'web'
) {
    global $myCala_request, /* @myCala_request The entire request ?w=x&foo=bar...*/
        $myCala_requestW; /* @myCala_requestW Just the `w` part of the request ?w=cala_default */

    # Let`s handle php`s errors requests
    # Grace will do this for now
    set_error_handler('grace_php_errors');

    # This will separate the logs since there are errors with permissions when runing on web and cli mode
    conf_set('mode', 'core', $mode);

    # Lets hold grace for now
    conf_set('buffer', 'grace', true);

    grace_debug(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
    grace_debug("Booting up V:" . CALA_VERSION);
    # @bug for some reason this always gives a warning
    # @todo add the ip to the log's name, so you can filter the logs
    @grace_debug("Request from ip: " . isset($_SERVER["REMOTE_ADDR"]) ? $_SERVER["REMOTE_ADDR"] : '127.0.0.1');

    # Find out the request made
    boot_getRequest();

    grace_debug('Request made [req]' . json_encode($myCala_request) . '[/req]');

    # Load all core modules
    # @todo Call the current requested module first in case it wants to change the core modules to be loaded
    boot_loadAllCoreModules();

    # Load all extra/core libraries
    boot_loadAllCoreLibraries();

    # Load all default locales
    boot_loadAllCoreLocales();

    if ($mode == 'web') {
        // nothing to do
        grace_debug("Running from web");
    } else {
        // Set mode
        grace_debug("Running on cli mode");
        // conf_set('cli', 'core', true);
        // Correctly load the params sent via cli
        params_cliLoadOpts(cala_init());
    }
     
    # Lets see if there is a request, otherwise I will assume cala_default
    $request = params_get('w', conf_get('defaultCall', 'core', 'cala_default'));

    # This is a bit messy, do not have time to do it better.
    $myCala_requestW = $request;

    # Grace can now talk
    grace_talkStart();

    # Get the module name and the actuall request
    $requestParts = explode("_", $request);

    /*
     * I will do this later!
    # Special case for cli, this makes it easier to call stuff without the prefix
    # for example you may call `cala_hello` with just `hello`

    if(count($requestParts) == 1){
        $requestParts[1] = $requestParts;
        $defCall = conf_get('defautCall', 'core', 'cala_default');
        $defParts = explode("_", $defCall);
        $requestParts[0] = $defParts[0];
    }
      */
    grace_debug('Requested module: ' . $requestParts[0]);
    grace_debug('Requested process: [proc]' . $requestParts[0] . '[/proc]');

    # @todo if the module is core and it was already loaded, don't do it again :)

    if (modules_loader($requestParts[0])) {
        # Init this call
        boot_initThisPath($requestParts, $request);
        return true;
    }

    # Better to provide a generic error, sorry, check the logs.
    # @todo This should not really be here, everything should always end in the
    # same place, which is boot_initThisPath, but for now, lets not make
    # a bigger mess.
    tools_errSet('An error occured', CALA_ERR);
    tools_reply();
}

/**
 * Find out the request made
 */
function boot_getRequest()
{
    global $myCala_request, $argv;

    if (isset($_POST['w'])) {
        params_set('w', $_POST);
        $myCala_request = $_POST;
    } elseif (isset($_GET['w'])) {
        params_set('w', $_GET);
        $myCala_request = $_GET;
    } elseif (isset($_PUT)) {
        params_set('w', $_PUT);
        $myCala_request = $_PUT;
    } elseif (isset($argv)) {
        grace_debug("Booting from cli...");
        # Lets get rid of the first one, usually php.cli
        array_shift($argv);
        $myCala_request = $argv;
        foreach ($argv as $r) {
            if (strpos($r, '=')) {
                list($key, $val) = explode("=", $r);
                params_set($key, $val);
            }
        }
    }
}

/**
 * Call the init function in the correct module
 */
function boot_initThisPath($parts, $r)
{
    grace_debug("I will now actually init the path requested.");


    /** @bug Apparently post requests (at least from Java) come with a
     *  \n (breakline) in each post request parameter, which breaks everything
     *  this issue is still under investigation
     */
    $f = preg_replace("/[\n\r\f]+/m", "", $parts[0] . "_init");

    ## I will try to load a fuction called moduleName_init based on the module requested,
    ## this function will find out all possible requests to the module so I can load
    ## the appropriated one.

    grace_debug("Looking for function: " . $f);

    if (function_exists($f)) {
        grace_debug("Function found.");
        $response = tools_proccesPath(call_user_func($f), $r, $parts[0]);
    } elseif (params_get("w", "--") == "--") {
        # Does this even happen?
        $response = CALA_ERR_PATH_NOT_FOUND; //'ERROR_MODULE_UNDEFINED';
    } else {
        $response = tools_errSet(
            'Function does not exist',
            CALA_ERR_PATH_NOT_FOUND
         );
    }

    # Return reply
    tools_reply($response);
}

/**
 * Load all core modules
 * @bug If the called module is a core module, it will get booted twice
 */
function boot_loadAllCoreModules()
{
    grace_info('Loading all core modules');

    foreach (conf_get('coreLoad', 'modules') as $module) {
        grace_debug("Loading module: >>($module)<<");
        modules_loader($module);
    }
    grace_debug('Done loading core modules.');
}

/**
 * Load all core libraries
 */
function boot_loadAllCoreLibraries()
{
    grace_info('Loading all core libraries');

    foreach (conf_get('coreLoad', 'libraries', []) as $l) {
        grace_debug("Loading library >>($l)<<");
        tools_loadLibrary($l.'.php');
    }
    grace_debug('Done loading libraries.');
}

/**
 * Load all default locales
 *
 * Remember to load 'locale' in the libraries!!
 */
function boot_loadAllCoreLocales()
{
    grace_info('Loading all core libraries');

    foreach (conf_get('default', 'locales', []) as $l) {
        locale_load($l);
    }
    grace_debug('Done loading locales.');
}


/**
 * Load boot up modules
 */
function boot_loadBootModules()
{
    //Todo
}

/** @}*/
