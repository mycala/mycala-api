<?php

/**
 * Send emails
 * @params info an array with:
 * to=mail@example.com
 * replyTo=replyTo@example.com (I will use the default mail in config if not)
 * from=from@example.com (I will use the default mail in config if not)
 * subject=the subject
 * message=the message
 */
function mailer_sendEmail($info)
{
    $headers = sprintf(
        "From: %s \r\n" .
        "Reply-To: %s \r\n" .
        "Content-type: text/html; charset=utf-8 \r\n" .
        'X-Mailer: PHP/ %s',
        conf_get('defaultMail', 'core', 'info@e2go.org'),
        $info['replyTo'] != ''
            ? $info['replyTo']
            : conf_get('defaultMail', 'core', 'info@e2go.org'),
        phpversion()
    );

    if (mail(
        $info['to'],
        $info['subject'],
        $info['message'],
        $headers
     )) {
        grace_debug('Mail sent');
        return true;
    } else {
        grace_error('Unable to send email');
        return false;
    }
}
