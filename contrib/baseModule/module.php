<?php
/** @file module.php
 * A brief file description.
 * A more elaborated file description.
 */

/** \addtogroup Contrib
 *  @{
 */

/**
 * \defgroup ModuleName
 * @{
 */


/**
 * Boot up procedure
 */
function MODULE_NAME_bootMeUp()
{
    // Booting up routines that you require
}

/**
 * Init function/hook.
 * \sa MODULE_NAME_init()
 */
function MODULE_NAME_init()
{
    return [
        [
            'r' => 'MODULE_NAME_say_hello',
            'action' => 'MODULE_NAME_sayHello',
            'access' => 'users_openAccess',
            'access_params' => 'accessName',
            'params' => [
                ['key' => '', 'def' => '', 'req' => true]
            ],
            'file' => 'file.php'
        ]
    ];
}

/**
 * Get the perms for this module.
 * Not fully implemented yet.
 */
function MODULENAME_access()
{
    $perms = array(
        array(
            // A human readable name
            'name'        => 'Do something with this module',
            // Something to remember what it is for
            'description' => 'What can be achieved with this permission',
            // Internal machine name, no spaces, no funny symbols, same rules as a variable
            // Use yourmodule_ prefix
            'code'        => 'mymodule_access_one',
            // Default value in case it is not set
            'def'        => false, //Or true, you decide
        ),
    );
}

/**@}*/
/** @}*/
