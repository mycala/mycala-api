<?php

/**
 * Logs users in.
 *
 * External call
 *
 * Reply codes are not very clear intentionally in order to avoid providing
 * with specific information to potential attackers.
 */
function users_logMeIn(
    $userName,
    $pwd,
    $method = 'js'
) {
    global $user;

    grace_debug("Log in this person: $userName");

    $user = users_load($userName);

    if ($user) {
        grace_info("I found a user, is it active? $pwd");
        if ($user['status'] > 0) {
            $q = sprintf(
                "SELECT `pwd` 
				 FROM `cala_users` 
				 WHERE `userName` = '%s'",
                $user['userName']
             );

            $r = db_querySingle($q);

            if (password_verify($pwd, $r['pwd'])) {
                grace_info("Password checks");
                if ($method == 'js') {
                    return _users_logIn($user);
                } else {
                    $_user = _users_logIn($user);
                    _users_cookieUpdate($_user, 'users', true);
                    return 'ALL_GOOD';
                }
            } else {
                return tools_errSet(
                    'Unable to log in this user, wrong login information',
                    'USERS_ERR_WRONG_LOGIN_INFO'
                 );
            }
        } else {
            # The user is inactive, but I will just say this to avoid exposing too much information
            return tools_errSet(
                'This user is not active.',
                'USERS_ERR_WRONG_USER'
              );
        }
    }
    return tools_errSet(
        'I did not find this user.',
        'USERS_ERR_WRONG_LOGIN_INFO'
     );
}

/**
 * Helper function to actually log in a user.
 *
 * DO NOT call me until you are certain about this. Check password or whatever
 * first. That is on you!
 * @user a fully fleshed user
 */
function _users_logIn($user)
{
    grace_debug('Login user:' . $user['userName']);

    logger_l([
        'who' => 'users',
        'title' => 'New login session for: ' . $user['userName'],
        'text' => sprintf('User `%s` started a new session', $user['userName']),
        'type' => 'info',
        'idUser' => $user['idUser']]);

    $user = array(
        'sessionKey' => users_generateSessionKey($user['idUser']),
        'userName'   => $user['userName'],
        'idUser'     => $user['idUser'],
        'userAvatar' => $user['avatar'],
        'fullName'   => $user['fullName'],
        'email'      => $user['email'],
        'about'      => $user['about'],
        'roles'      => $user['roles'],
    );

    $user['sso'] = tools_enc(
        JSON_encode(
            [
                'idUser'    => $user['idUser'],
                'sessionKey' => $user['sessionKey']
            ]
        )
    );

    hooks_call('users_login', [&$user]);

    return $user;
}

/**
 * Generates a session key.
 * @todo add a kill all sessions function
 */
function users_generateSessionKey($idUser)
{

    # Clean all sessions
    # @bug this will log out the user from everywhere!
    /*
                $q = sprintf("DELETE FROM `cala_sessions`
                               WHERE `idUser`='" . $idUser . "'
                               AND `ip`='" . $_SERVER['REMOTE_ADDR'] . "'");

    db_exec($q);
     */

    # @bug this is NOT secure. While the result is quite random, it is a bit
    # 'guessable'. I need to add a flow control maybe, to avoid abuse.
    $sessionKey = users_hash(time() * rand(0, 100000000) . md5(time()*rand(0, 1000000)));

    # @todo I am getting rid of the ip, it does not work really.
    $q = sprintf("INSERT INTO `cala_sessions` (`idUser`, `sessionKey`, `ip`, `lastAccess`) "
        . "VALUES('%s', '%s', '0', '%s')", $idUser, $sessionKey, time());

    db_exec($q);

    return $sessionKey;
}

/**
 * Logout a user.
 *
 * External call
 */
function users_logMeOut(
    $sessionKey
) {
    global $user;

    grace_debug("Log out");

    # No sessionKey? Maybe a php based request

    if ($sessionKey == '') {
        $sessionKey = params_get('sessionKey', '');
    }

    users_destroySession($sessionKey);
    params_set('sessionKey', 'longGone');

    $cookieName = conf_get('cookieName', 'core', 'randomCookieWithNoName');
    if (isset($_COOKIE[$cookieName])) {
        setcookie($cookieName, '', time() - 3600);
    }
    return 'Good_Bye';
}
