<?php

/**
 * Login page
 */
function users_loginPage()
{
    global $locale, $user;

    if (_users_isLoggedIn()) {
        $page = conf_get('defaultCallLoggedIn', 'core', '');
        tools_goto($page);
    }
    skin_setTitle('Login');
    $mainSkin['header']['description'] = '';
    $mainSkin['header']['image'] = '';

    return skin_this([], dirname(__FILE__) . '/skins/login', 'users');
}

/**
 * Register page
 */
function users_registerPage()
{
    global $locale, $user;

    if (_users_isLoggedIn()) {
        $page = conf_get('defaultCallLoggedIn', 'core', '');
        tools_goto($page);
    }

    skin_setTitle('Register');
    $mainSkin['header']['description'] = '';
    $mainSkin['header']['image'] = '';

    return skin_this([], dirname(__FILE__) . '/skins/register', 'users');
}

/**
 * Profile page
 */
function users_profilePage()
{
    global $locale, $user;

    skin_setTitle($user['fullName']);
     
    $user['avatar'] = conf_get('avatarPublicPath', 'users', '') . $user['idUser'] . '_avatar.jpg';

    return skin_this($user, dirname(__FILE__) . '/skins/profile', 'users');
}
