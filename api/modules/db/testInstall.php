<?php

/**
 * hook_testInstall()
 */
function db_testInstall()
{

	global $dbConn;

    # All tests
    $allTests = array();

    #
    # Lets run the tests
    #

	 # Test the database connection.
	 #
    $result = db_Connect();
    $allTests['dbConnection'] = array(
        'name' => 'Database connection',
        'result' => $result,
        'good' => 'Your database connection is good.',
        'fail' => 'Your database connection failed => ' . $dbConn->connect_error
    );

	 return $allTests;
}

