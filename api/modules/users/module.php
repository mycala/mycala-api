<?php

/** @file users/module.php
 * Users module
 */

/** \addtogroup Modules
 *  @{
 */

/**
 * \defgroup Users
 * @{
 */

/** @ingroup GlobalVars
 *  @{
 */

//! The global User Variable
global $user;

/** @} */

/**
 * Boot up procedure
 */
function users_bootMeUp()
{
    global $user, $mainSkin;

    grace_debug('Users is in the house!');

    # Make sure there is a DB
    modules_loader('db');

    # Load the user, if they are logged in it is good to know, it is slower to
    # always do this, but I guess it is worth it.
    # If the information is wrong I will 'just' issue a warning, each request is
    # responsible for handling this situation.
    users_loadCurrentUser();

    # Session handlers
      /*
     session_set_save_handler(
          '_users_session_open',
          '_users_session_close',
          '_users_session_read',
          '_users_session_write',
          '_users_session_destroy',
          '_users_session_garbage_collection'
        );
        */
     //session_start();
}

function _users_session_open()
{
    return true;
}
function _users_session_close()
{
    return true;
}
function _users_session_read($sid)
{
    grace_debug('Session id: ' . $sid);
}
function _users_session_write($sid, $value)
{
    grace_debug('Session id: ' . $sid . JSON_encode($value));
}
function _users_session_destroy($sid)
{
}
function _users_session_garbage_collection($lifetime)
{
    grace_debug('Session lifetime: ' . $lifitime);
}

function users_init()
{
    $paths = array(

        /**
         * Login page
         */
        [
            'r' => 'users_login_page',
            'action' => 'users_loginPage',
            'access' => 'users_openAccess',
            'replyType' => 'skin',
            'file' => 'pages.php'
        ],

        /**
         * Register Page
         */
        [
            'r' => 'users_register_page',
            'action' => 'users_registerPage',
            'access' => 'users_openAccess',
            'replyType' => 'skin',
            'file' => 'pages.php'
        ],

        /**
         * User Profile Page
         */
        [
            'r' => 'users_profile_page',
            'action' => 'users_profilePage',
            'access' => 'users_loggedIn',
            'replyType' => 'skin',
            'file' => 'pages.php'
        ],

        # Register a new user
        # Method = POST
        # @return idUser
        # r=users_register
        # @name A name for you
        # @userName=me // no spaces, no weird symbols, same rules as an
        # email address
        # @email Optional email
        # @pwd=123 // plain text, unsafe I know
        # @about=json(longTextDescribingMe) // json encoded
        array(
            'r' => 'users_register',
            'action' => 'users_registerNew',
            'access' => 'users_openAccess',
            'params' => array(
                array('key' => 'fullName',  'def' => '', 'req' => true),
                array('key' => 'userName', 'def' => '', 'req' => true),
                array('key' => 'email', 'def' => '', 'req' => true),
                array('key' => 'pwd',   'def' => '', 'req' => true),
                array('key' => 'about', 'def' => '', 'req' => false),
                array('key' => 'method', 'def' => 'js', 'req' => false),
            ),
            'file' => 'register.php'
        ),

        # Get a list of users
        # @return A list of users that match your criteria
        # r=users_get_list
        # [&like=me]
        # @deprecated ?
        array(
            'r' => 'users_get_list',
            'action' => 'users_getList',
            'access' => 'users_openAccess',
            'params' => array(array('key' => 'like', 'def' => '', 'req' => false)),
            'file' => 'getList.php'
        ),

        # Log me in
        # @return sessionId
        # r=users_log_me_in
        # pwd=123
        array(
            'r' => 'users_log_me_in',
            'action' => 'users_logMeIn',
            'access' => 'users_openAccess',
            'params' => array(
                array('key' => 'userName', 'def' => '', 'req' => true),
                array('key' => 'pwd',      'def' => '', 'req' => true),
                array('key' => 'method',   'def' => 'js', 'req' => false)
            ),
            'file' => 'login.php'
        ),

        # Log me out
        # @return Success|Fail
        # r=users_log_me_out
        # @todo this requires sessionKey, but that is already checked in loggedIn
        array(
            'r' => 'users_log_me_out',
            'action' => 'users_logMeOut',
            'access' => 'users_loggedIn',
            'params' => array(
                array('key' => 'sessionKey', 'def' => '', 'req' => false)
            ),
            'file' => 'login.php'
        ),

        # Get/Display the avatar
        # @return An image that can be displayed in the browser
        # r=users_avatar_get
        # &size=25|50|100|250
        # &who=userName
        array(
            'r' => 'users_avatar_get',
            'action' => 'users_avatarGet',
            'access' => 'users_openAccess',
            'params' => array(
                array('key' => 'size', 'def' => '25', 'req' => false),
                array('key' => 'userName', 'def' => '', 'req' => true)
            ),
            'file' => 'avatarGet.php'
        ),

        # Upload an avatar
        # @return Success|Fail
        # r=users_avatar_upload
        array(
            'r' => 'users_avatar_upload',
            'action' => 'users_avatarUpload',
            'access' => 'users_loggedIn',
            'file' => 'avatarGet.php'
        ),
          
        # Get my details
        # @return I will just return the details about the currentlyi
        #  logged in user
        # r=users_get_my_details
        array(
            'r' => 'users_get_my_details',
            'action' => 'users_getMyDetails',
            'access' => 'users_loggedIn',
        ),

        # Password recovery either by email or username
        # @return success|fail
        # r=users_recover_password
        # &userName=username|email@email.com
        array(
            'r' => 'users_recover_pwd',
            'action' => 'users_recoverPwd',
            'access' => 'users_openAccess',
            'params' => array(
                array('key' => 'userName', 'def' => '', 'req' => true)
            ),
            'file' => 'pwdRecover.php'
        ),

        # Update profile
        # @return Success or fail
        # r=users_update_profile
        # [&fullName=newName
        # &userName=newName
        # &about=json(somehting about me)
        # &pwd=newPwd
        # All fields are tecnically optional, it would make no sense
        # to make such a call, but it would be valid.
        array(
            'r' => 'users_update_profile',
            'action' => 'users_updateProfile',
            'access' => 'users_loggedIn',
            'params' => array(
                array('key' => 'fullName', 'req' => true),
                array('key' => 'userName', 'req' => true),
                array('key' => 'about',  'def' => '', 'req' => false),
                array('key' => 'email',  'def' => '', 'req' => false),
                array('key' => 'pwd',    'def' => '', 'req' => false)
            ),
        ),

        # Get profile details about a user
        # r=users_profile_load
        array(
            'r' => 'users_profile_load',
            'action' => 'users_profileLoad',
            'access' => 'users_openAccess',
            'params' => array(
                array('key' => 'userName', 'req' => true)
            )
        ),

    );

    return $paths;
}

/**
 * Loads the current user according to sessionKey and iam provided.
 *
 * If this information is not provided or wrong I will load an empty user.
 */
function users_loadCurrentUser()
{
    global $user;

    grace_debug('I shall load the user by default if there is an active session: ' . params_get('iam', ''));

    $user = false;

    # If there is a cookie I shall assume we are not using CalaJS
    # @todo this should be a function

    if ($info = _users_cookieGet('users')) {
        params_set('iam', $info['userName'], true);
        params_set('sessionKey', $info['sessionKey'], true);
        params_set('sso', $info['sso'], true);

        # Keep the session alive
        _users_cookieUpdate($info, 'users');
    }

    if (params_get('iam', '') != '') {
        $user = users_load(params_get('iam', ''));
    }

    if (!$user) {
        $user = users_createBasic();
    } else {
        # Let's not trust what iam says, you also need a valid session key
        if (!users_confirmSessionKey(params_get('sessionKey', ''), $user)) {
            logger_l([
                'who' => 'users',
                'title' => 'Wrong session key',
                'text' => sprintf('User `%s` sent the wrong session key `%s`', params_get('iam', '0'), params_get('sessionKey', '')),
                'recom' => 'Check the ip in the logs and see if this same ip is doing more weird stuff',
                'type' => 'warn',
                'idUser' => $user['idUser'],
                'severity' => 3]);
            $user = users_createBasic();
        }
    }
    grace_debug('Current user [info]' . JSON_encode($user) . '[/info]');

    # Hook it
    hooks_meUp('users_loadCurrent', [&$user]);
}

/**
 * Check if the user is logged in
 */
function _users_loggedIn()
{
    global $user;

    if ($user['idUser'] == 0) {
        return false;
    }
    return true;
}

/******************************************************************
 *
 * Access and permissions
 *
 * ***************************************************************/

/**
 * Dummy function, just call me if you want to grant access to anyone
 */
function users_openAccess()
{
    return true;
}

/**
 * Check (permissions) if a user is logged in.
 *
 * Returns an error if not! This is for access control.
 *
 * Usually Cala would check to see if the required iam and sessionKey params
 * where sent, but this would be very tedious for programmers. So, this is a
 * special case. I will handle that my self.
 */
function users_loggedIn()
{
    global $user;

    grace_debug('Confirm that the user is logged in');

    # If there was a user logged in it should be by now, I will just confirm this.
    if ($user['idUser'] == 0) {
        logger_l([
            'who' => 'users',
            'title' => 'Access denied',
            'text' => 'Trying to acces a page not logged in',
            'type' => 'alert',
            'idUser' => $user['idUser'],
            'severity' => 8,
        ]);

        return tools_errSet(
            'I could not get the user indicated in `iam`',
            'USERS_ERR_NOT_LOGGED_IN'
        );
    }

    return true;
}

/**
 * Check for an SSO Login
 */
function users_sso()
{
    global $user;

    grace_debug('Check for SSO');

    $sso = tools_enc(
        params_get('sso', ''),
        'dec'
    );

    if ($sso == '' || params_get('sessionKey', '') == '') {
        return tools_errSet(
            'I could not verify sso',
            'USERS_ERR_NOT_LOGGED_IN'
        );
    }

    $sso = JSON_decode($sso);

    if ($sso->sessionKey == params_get('sessionKey', '')) {
        $user['idUser'] = $sso->idUser;
        return true;
    }

    return tools_errSet(
        'I could not verify sso',
        'USERS_ERR_NOT_LOGGED_IN'
    );
}

/**
 * Create a basic empty user.
 */
function users_createBasic()
{
    grace_debug('Creating a basic empty user');
    return array(
        'idUser' => 0,
        'pwd' => '',
        'userName' => '',
        'sessionKey' => '',
        'about' => ''
    );
}

/**
 * Generates a user hash (for passwords mostly).
 *
 * @todo Adjust the cost: http://php.net/manual/en/function.password-hash.php
 */
function users_hash($val)
{
    grace_info("Hashing $val");

    $hash = password_hash($val, PASSWORD_BCRYPT);

    if (!$hash) {
        grace_error('Unable to generate hash');
        return false;
    }

    grace_info("Hash: $hash");

    return $hash;
}

/**
 * Loads a user and return a fully fleshed user.
 *
 * email: includes @ somewhere
 * userName: if it is not an email!
 *
 * Return false if nothing found.
 */
function users_load($userName)
{
    grace_debug('Loading a user: ' . $userName);

    # Lowercase it all
    $userName = strtolower($userName);

    if (strpos($userName, '@') > 0) { # Is it an email based login?
        grace_info('Email based load');
        $where = sprintf('email = "%s"', db_escape($userName));
    } else {
        if (strpos($userName, '::') === 0) { # idUser based
            grace_info('IdUser based load');
            $where = sprintf('idUser = "%s"', db_escape(substr($userName, 2)));
        } else {
            grace_info('Username based load');
            $where = sprintf('userName = "%s"', db_escape($userName));
        }
    }

    $q = sprintf("SELECT `idUser`,`fullName`,`userName`,`email`,`about`,
		`status`,`timestamp`,`lastAccess`,`avatar`,`settings`
		FROM `cala_users`
		WHERE %s", $where);

    $u = db_querySingle($q);

    # Parse settings
    if ($u) {
        $u['settings'] = (array) JSON_decode($u['settings']);

        # Get the roles
        $roles = users_rolesLoadPerUser($u['idUser']);
        $u['roles'] = $roles;

        # Set the avatar
        modules_loader('users', 'avatarGet.php', false);
        $u['avatar'] = $u['avatar'] == '' ? '' :
            users_avatarPath($u['idUser'], true) . $u['avatar'];

        # Format back some fields
        $u['fullName'] = tools_textUnFilterSafe($u['fullName']);
        $u['about'] = tools_textUnFilterSafe($u['about']);
    }

    return $u;
}

/**
 * Confirm the validity of a session.
 *
 * @todo @bug if the ip changes, the users is logged out
 * not really a 'bug', but it is not what I need, mobile devices change ips
 * all the time.
 *
 * @todo add a setting to associate sessions with ips, there is a problem with
 * european laws and storing this information.
 */
function users_confirmSessionKey(
    $key, //!< The session key
    $user //!< The user to check for
) {
    grace_debug('Confirm the session for this user');

    $q = sprintf(
        "SELECT *
		FROM `cala_sessions`
		WHERE `sessionKey` = '%s'
		AND `idUser` = '%s'",
        $key,
        $user['idUser']
    );

    $r = db_querySingle($q);

    if (!$r) {
        grace_error('No results found');
        return false;
    }

    # Is it a limited session?
    # @bug I am not sure if sessions are getting expired because a bug in here
    # or my IP changes all the time, I will get here at some point unless
    # I implement the new system before that.
    if (conf_get('sessionLifetime', 'users', '3600') != 0) {
        if ((time() - $r['lastAccess']) > conf_get('sessionLifetime', 'users', '3600')) {
            grace_error('User last access is to old');
            return false;
        }
    }

    grace_info('The user is alive :) This is an ethernal session');
    grace_warn(['Vulnerable', 'Using an ethernal session, this is not really recommended']);
    # I think this is a good place for this,
    # if we already confirmed that the session is good,
    # lets give it some more time
    users_updateLastAccess();

    return true;
}

/**
 * Destroys a session.
 */
function users_destroySession($sessionKey)
{
    $q = sprintf(
        "DELETE FROM `cala_sessions` WHERE `sessionKey` = '%s'",
        $sessionKey
    );
    db_exec($q);
}

/**
 * Updates the last access with a session key.
 *
 * @todo Update only if the access was valid
 */
function users_updateLastAccess()
{
    global $user;

    grace_debug('Updating last acces to the session');

    if (params_get('sessionKey', '') != '' && $user['idUser'] > 0) {
        # Update in the session table
        $q = sprintf(
            "UPDATE `cala_sessions` SET `lastAccess` = '%s' WHERE `sessionKey` = '%s'",
            time(),
            params_get('sessionKey', '')
        );

        db_exec($q);

        # Update in the user`s table too, this could be a separate process
        $q = sprintf(
            "UPDATE `cala_users` SET `lastAccess` = '%s' WHERE `idUser` = '%s'",
            time(),
            $user['idUser']
        );

        db_exec($q);
    }
}

/**
 * Update the user profile.
 *
 * External call
 * @todo Move to register.php
 */
function users_updateProfile(
    $name,
    $userName,
    $about,
    $email,
    $pwd
) {
    global $user;

    # New pwd
    $dets = array(
        'fullName'   => $name,
        'userName'   => $userName,
        'email'      => $email,
        'about'      => $about,
        'status'     => 1,
        'lastAccess' => time(),
        'pwd'        => $pwd,
        'avatar'     => '',
        'idUser'     => $user['idUser']
    );

    $r = _users_update($dets);

    if ($r === 'ERROR_DB_ERROR') {
        return tools_errSet(
            "Unable to update profile see db errors: $r",
            'ERR_ERR'
        );
    }

    /**
     * Cookie?
     */

    _users_cookieUpdate([
        'sessionKey' => params_get('sessionKey', ''),
        'sso'        => params_get('sso', ''),
        'userName'   => $user['userName'],
        'idUser'     => $user['idUser'],
        'userAvatar' => $user['avatar'],
        'fullName'   => $user['fullName'],
        'email'      => $user['email'],
        'about'      => $user['about'],
        'roles'      => $user['roles'],
    ], 'users', false);

    return 'ALL_GOOD';
}

/**
 * Get the cookie for this module.
 */
function _users_cookieGet(
    $module /*! The module */
) {
    $conf = conf_get('cookie', 'core', false);
    if (!$conf) {
        return false;
    }

    $cookieName = $conf['name'].'_'.$module;

    if (!isset($_COOKIE[$cookieName])) {
        return false;
    }

    $info = (array) JSON_decode($_COOKIE[$cookieName]);

    return $info;
}

/**
 * Update the cookie, for example, after a user update.
 *
 * Each module will have its own cookie.
 */
function _users_cookieUpdate(
    $values,
    $module,
    $force = false /* Do not create if it does not exist */
) {
    grace_debug('Update cookie for: ' . $module);
    $conf = conf_get('cookie', 'core', [
        'name'     => 'randomCookieWithNoName',
        'exprires' => 3600, # 0 is never, negative is now
        'path'     => '/',
        'domain'   => '',    # leading dot for compatibility or use subdomain .example.com
        'secure'   => false, # true or false
        'httponly' => true,  # true or false
        'samesite' => 'Strict' # None || Lax || Strict
    ]);
    //print_r($conf);
    /*
    $options = [
        'expires'  => $conf['expires'],  # 0 is never, negative is now
        'path'     => $conf['path'],     #
        'domain'   => $conf['domain'],   # leading dot for compatibility or use subdomain
        'secure'   => $conf['secure'],   # true or false
        'httponly' => $conf['httponly'], # true or false
        'samesite' => $conf['samesite']  # None || Lax  || Strict
     ];

    $info = [];
    if (isset($_COOKIE[$cookieName])) {
         $info = (array) JSON_decode($_COOKIE[$cookieName]);
    } else {
         if (!$force) {
               return false;
         }
    }
    print_r($info);
    echo '---------------------------------------';
    $info[$module] = $values;

     echo $info = JSON_encode($info);
     */

    $cookieName = $conf['name'].'_'.$module;

    if (!isset($_COOKIE[$cookieName]) && !$force) {
        return false;
    }

    setcookie(
        $cookieName,
        JSON_encode($values),
        $conf['expires'] == 0 ? 0 : time() + $conf['expires'],
        $conf['path'],
        $conf['domain'],
        $conf['secure'],
        $conf['httponly']
    );

    /*
             session_set_cookie_params(
                    $conf['expires'] == 0 ? 0 : time() + $conf['expires'],
                    $conf['path'],
                    $conf['domain'],
                    $conf['secure'],
                    $conf['httponly']
               );

             session_name($conf['name']);
             session_start();

             setcookie(
                    $conf['name'],
                    JSON_encode($info)
               );
     */
/*
      session_set_cookie_params(
             $conf['expires'] == 0 ? 0 : time() + $conf['expires'],
             $conf['path'],
             $conf['domain'],
             $conf['secure'],
             $conf['httponly']
        );

      session_name($conf['name']);
      session_start();

      setcookie(
             $conf['name'],
             JSON_encode($info)
        );
 */
}

/**
 * Clean up user names, this function should not be here
 * @todo create a validation tool for this
 */
function users_cleanName($name)
{
    return trim($name);
}

/**
 * Helper function to actually update a user.
 *
 * @todo move to register.php
 */
function _users_update($dets)
{
    global $user;

    # If password is not set, I will keep it the same
    if (!isset($dets['pwd']) || trim($dets['pwd']) == '') {
        grace_debug('Password not set');
        $dets['pwd'] = 'pwd';
    } else {
        $dets['pwd'] = "'" . users_hash($dets['pwd']) . "'";
    }

    # If avatar is not set, I will keep it the same
    if (!isset($dets['avatar']) || trim($dets['avatar']) == '') {
        grace_debug('Avatar not set');
        $dets['avatar'] = 'avatar';
    } else {
        $dets['avatar'] = "'" . $dets['avatar'] . "'";
    }

    # If email is not set, I will keep it the same
    if (!isset($dets['email']) || trim($dets['email']) == '') {
        grace_debug('Email not set');
        $dets['email'] = 'email';
    } else {
        $dets['email'] = "'" . $dets['email'] . "'";
    }

    # Merge the current information about the user and the new information provided
    $newDets = array_replace((array) $user, $dets);

    $q = sprintf(
        "UPDATE `cala_users` SET
		`fullName` = '%s',
		`userName` = '%s',
		`email` = %s,
		`about` = '%s',
		`status` = '%s',
		`lastAccess` = '%s',
		`pwd` = %s,
		`avatar` = %s
		WHERE `idUser` = %s",
        tools_textFilterSafe($newDets['fullName'], true, true),
        users_cleanName(tools_textFilterSafe($newDets['userName'], true, true)),
        $newDets['email'],
        tools_textFilterSafe($newDets['about'], true, true),
        $newDets['status'],
        $newDets['lastAccess'],
        $newDets['pwd'],
        $newDets['avatar'],
        $newDets['idUser']
    );

    logger_l([
        'who' => 'users',
        'title' => 'Account updated',
        'text' => 'Your account was updated',
        'recom' => '',
        'type' => 'info',
        'idUser' => $newDets['idUser'],
        'severity' => 3]);

    $r = db_exec($q);

    if ($r === 'ERROR_DB_ERROR' || $r == false) {
        return false;
    }
    return true;
}

/**
 * Get MY details.
 *
 * I work on the currently logged in user.
 */
function users_getMyDetails()
{
    global $user;

    # @bug Why do I do this?
    modules_loader('users', 'avatarGet.php', false);

    grace_debug('Getting my details' . JSON_encode($user));

    $u = array();
    $u['userName']  = $user['userName'];
    $u['avatar']    = $user['avatar'];
    $u['name']      = $user['fullName'];
    $u['about']     = $user['about'];

    return $u;
}

/**
 * Create a basic empty user.
 *
 * @todo Add more profile parts to this user structure.
 */
function _userCreateBasic()
{
    return array('idUser' => 0, 'pwd' => '');
}

/**
 * Access permissions, verify if they exist.
 *
 * @todo Create an admin group
 */
function users_access(
    $perm,
    $theUser = false
) {
    global $user;

    if ($user == false) {
        $theUser = $user;
    }

    # The powers that be
    if ($user['idUser'] == 1) {
        return true;
    }

    grace_debug('Checking for perms: ' + $perm);

    #@todo Actually check the perms :)
    return true;
}

/**
 * Load a profile.
 *
 * Use this if you want a 'safe' profile information
 *
 * External call
 */
function users_profileLoad($userName)
{
    grace_debug('Load a profile');

    modules_loader('users', 'avatarGet.php', false);

    $user = users_load($userName);

    if (!$user) {
        return tools_errSet(
            'User does not exist',
            'ERR_ERR'
        );
    }

    return array(
        'userName' => $user['userName'],
        'fullName' => $user['fullName'],
        'avatar'   => $user['avatar'],
        'about'    => $user['about'],
    );
}

/**
 * Load roles per user.
 */
function users_rolesLoadPerUser($idUser)
{
    $q = "
	 SELECT r.name
	 FROM `cala_usersRoles` ur
	 INNER JOIN `cala_roles` r ON r.idRole = ur.idRole
	 WHERE ur.idUser = $idUser";

    $roles = db_q($q);

    if ($roles != 'ERROR_DB_ERROR' && $roles != false) {
        $_roles = [];
        foreach ($roles as $role) {
            $_roles[] = $role['name'];
        }
        return $_roles;
    }
    return false;
}

/**
 * Check if a user has a certain role.
 */
function users_checkRole(
    $role,
    &$user
) {
    grace_debug('Check for a group/role in user: ' . $role);

    if (!isset($user['roles'])) {
        return false;
    }

    if (!is_array($user['roles'])) {
        return false;
    }

    if (in_array($role, $user['roles'])) {
        return true;
    }

    return false;
}

/**
 * Is the user logged in?
 */
function _users_isLoggedIn()
{
    global $user;

    if ($user['idUser'] == 0) {
        return false;
    }
    return true;
}

/**
 * Check permission if user has a role
 */
function users_roleHas(
    $role
) {
    global $user;

    grace_debug('Check for role: ' . $role);

    if (users_checkRole($role, $user)) {
        return true;
    }
    return tools_errSet(
        'Access denied, no role',
        CALA_ERR
    );
}

/**
 * Create a side menu to login
 */
function users_menuLogin()
{
    global $user;
    return skin_this(
        [
            'fullName' => isset($user['fullName']) ? $user['fullName'] : ''
        ],
        dirname(__FILE__) . '/skins/menuUser',
        'users'
    );
}

/**@}*/
/** @}*/
