<?php

/**
 * Skin the output of the results.
 */
function cala_testInstallSkin($tests)
{
	grace_debug('Skining the results of the tests.');

	# Lets not print this in json
	params_set('replyType', 'plain');

	# Did I talk about the contributed modules?
	$contrib = false;

	# Cli mode
	if (conf_get('mode', 'core', 'web') == 'cli') {

		$allGoodMsg    = "All good :) ✔️";
		$allNotGoodMsg = "Errors found :( 💔";
		$output = "👋 MyCala Installation check proccess 👋 \n";
		$output .= "This is highly advanced installation check, please read carefully any errors found and correct them before using MyCala.";

		foreach ($tests as $test => $t) {
			if ($t['config']['group'] != 'Core' && !$contrib) {
				$output .= "\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++";
				$output .= "\n+ Contributed modules\n";
				$output .= "++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\n";
				$contrib = true;
			}

			$output .= "\n\n**************************************************************";
			$output .= "\n* - Module: " . $t['config']['name'];
			$output .= "\n* - About: " . $t['config']['about'];
			$output .= "\n* - Version: " . $t['config']['version'];
			$output .= "\n* - Group: " . $t['config']['group'];
			$output .= "\n**************************************************************\n";

			if (count($t['tests']) > 0) {
				foreach ($t['tests'] as $test => $r) {
					$result = $r['result'] == 'good' ? $allGoodMsg : $allNotGoodMsg;
					$msg = $r['result'] == 'good' ? $r['good'] : $r['fail'];

					$test = sprintf(
						"|Name: %s \n|Result: %s \n|Messages: %s \n--------------------------------------- \n",
						$r['name'],
						$result,
						str_replace('{_br_}', "\n", $msg)
					);
				$output .= $test;
				}
			} else {
				$output .= ' ⭐️ <<<<<<<<<< - No tests done - >>>>>>>>>> ⭐️';
			}
		}

		$output .= "\n\n--------------------------------------------------\n";
		$output .= "Please correct any errors found (💔) and go on with it!\n";
		$output .= '🖖 ';
		return $output;

	} else { # Web mode
		$allGoodMsg    = '<span class="resGood">All good :) ✅️ </span>';
		$allNotGoodMsg = '<span class="resFail">Errors found :( 💔 </span>';

		$output = "<h1>👋 MyCala Installation check proccess👋 </h1>";
		$output .= "<p>This is highly advanced installation check, please read carefully any errors found and correct them before using MyCala. </p> <hr />";

		foreach ($tests as $test => $t) {
			if ($t['config']['group'] != 'Core' && !$contrib) {
				$output .= '<br /><br />++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />';
				$output .= '+<br />';
				$output .= '+   Contributed modules<br />';
				$output .= '+<br />';
				$output .= '+<br />';
				$output .= '++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++<br />';
				$contrib = true;
			}

			$output .= '<br /><br />**************************************************************<br />';
			$output .= '<div><strong>Module: </strong>' . $t['config']['name'] . '</div>';
			$output .= '<div><strong>About:</strong> ' . $t['config']['about'] . '</div>';
			$output .= '<div><strong>Version:</strong> ' . $t['config']['version'] . '</div>';
			$output .= '<div><strong>Group:</strong> ' . $t['config']['group'] . '</div>';
			$output .= '**************************************************************<br />';

			if (count($t['tests']) > 0) {
				$table = '<table width="100%">';
				$table .= '<tr><td class="header" width="15%">Test</td><td class="header" width="15%">Result</td><td class="header">Comment</td></tr>';
				foreach ($t['tests'] as $test => $r) {
					$result = $r['result'] == 'good' ? $allGoodMsg : $allNotGoodMsg;
					$msg = $r['result'] == 'good' ? $r['good'] : $r['fail'];
					$msg .= isset($r['extraMsg']) ? '<div class="extraMsg">' . $r['extraMsg'] . '</div>': '';
					$table .= '<tr>';
					$table .= sprintf(
						'<td class="row" valign="top">%s</td><td class="row" valign="top">%s</td><td class="row" valign="top">%s</td>',
						$r['name'],
						$result,
						str_replace('{_br_}', '<br />', $msg)
					);
					$table .= '</tr>';
				}
				$table .= '</table>';
				$output .= $table;
			} else {
				$output .= ' ⭐️ <<<<<<<<<< - No tests done - >>>>>>>>>> ⭐️ ';
			}
		}

		$output .= "<br /><br />-------------------------------------------------- <br />";
		$output .= "Please correct any errors found (💔) and go on with it!<br />  🖖 ";

		$f = file_get_contents(__DIR__ . '/testInstall.html');
		$f = str_replace('{_BODY_}', $output, $f);
	return $f;
	}

}

/**
 * Test install in all modules.
 *
 * External call.
 */
function cala_testInstallStart()
{

	$allTests = [];

	grace_debug('Calling all modules to test their install.');

	hooks_call('testInstall', array(&$allTests));

	return cala_testInstallSkin($allTests);
}

/**
 * Testing: open the testInstall in the browser
 *
 * External call.
 */
function cala_testInstallStartBrowser()
{

	# This will kill your system!!!
	exit;
	$do = 'php -S localhost:8001 & firefox http://localhost:8001?w=cala_test_install_browser&cronToken='.conf_get('cronToken', 'core', '') ;
	echo 'I will do: ' . $do;
	exec($do);

	return false;
}

