<?php

$sitesLocal = [

    'mySite' => [
        'server' => 'http://localhost/',
        'token' => 'theToken'
    ],

];

$sitesRemote = [

    'mySite' => [
        'server' => 'http://localhost/',
        'token' => 'theToken'
    ],
];

if (isset($_GET['site'])) {
    if ($_GET['t'] == 'l') {
        $server = $sitesLocal[$_GET['site']]['server'];
        $token  = $sitesLocal[$_GET['site']]['token'];
    } else {
        $server = $sitesRemote[$_GET['site']]['server'];
        $token  = $sitesRemote[$_GET['site']]['token'];
    }
}
