# Installation instructions

It is actually very easy to do.

Step 1:

- Extract the contents of the compressed file (already done!)

Step 2:
- Create a new database and load the contents of: base_cala.sql
This is actually optional, you can use MyCala without a database depending on your needs.

Step 3:
- Rename or copy/paste settings.tpl.php to settings.php
- Change the settings accordingly, specially this:
```$config['core']['cronToken'] = 'PutANewToken_safe_andSecure_justForYou';```

Step 4:
- Run the installation check here:
```
http://path/to/install/?w=cala_test_install&cronToken=PutANewToken_safe_andSecure_justForYou
```

Notice that you must change this value!!!
```cronToken=PutANewToken_safe_andSecure_justForYou```

Correct any errors if you see them.

That should be about it.

# Configuration

MyCala Api can be used to serve several sites at the same time using the same core.

This is a recomended setting that can help you do that, if you will use it for just one site it is still very good to follow this configuration instructions for security.

Asumming that your public directory is in /var/www

## Insecure version, everything web accesible

```
/var/www/
- api
- logs
- tmp
- index.php
- settings.php
```

## More secure, the api is in a non web accesible directory

```
/path/to/non/web/accesible/dir
- api

/var/www/
- logs
- tmp
- index.php
- settings.php
```

## Even more secure, the api, settings, logs, tmp directories are located in a non web accesible directory.

```
/path/to/non/web/accesible/dir/
- api
- /sub/dir/with/site1
  - logs
  - tmp
  - settings.php

/var/www/
	- index.php
```

# Security tip

Change the `settings.php` file name to something different, anything will do.

There are hackers that introduce code (files) in your server that are programed to look for this types of files, specially from known cms aplications such as Drupal and Joomla.

Then, in your `index.php` and `cli.php` files change the line

```
include('settings.php');
```

to your new file name

```
include('iAmAnNotWhatYouAreLookingFor.php');
```

# Test your installation

The main way to use MyCala is probably as a REST Api, you make calls and get a
JSON response, but you can also use it via CLI (not very advanced at the moment)
or to generate plain text results.

## Rest

Just go like this:
`https://localhost/mycala/?w=cala_hello`

You should see an error message 'ERROR_BAD_REQUEST', that is fine!

Now try this:
`https://localhost/mycala/?w=cala_hello&test=aTEst`

Now you should see some results in the screen.

`test` is a required parameter for this call, you MUST send something.

`https://localhost/mycala/?w=cala_hello&default=ThisIsDefault&test=aTEst`

Now you should see some results in the screen with your custom default value.
`default` is an optional parameter for this call. You may send something or you
may leave it blank and MyCala will assign a `default` value.

You will learn more abouth them when you read the module development information.

## CLI
`/path/to/your/install/cli.php w=cala_hello`

You can also create a link to cli.php in order to have global access.

## Change the reply type

`https://localhost/mycala/?w=cala_hello&default=ThisIsDefault&test=aTEst&replyType=plain`

Not all calls handle this correctly, it really depends on the request. Some are
ment to reply only in JSON form.

Again, you will learn more in module develpment depending on your needs.

