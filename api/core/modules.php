<?php

global $modules;

/**
 * Gets the correct path for a module.
 */
function modules_getPath($which)
{
    $coreModules = conf_get('core', 'modules', array());

    grace_debug('Looking for module: ' . $which . ' in array: ' . JSON_encode($coreModules));

    if (in_array($which, $coreModules)) {
        $path = conf_get('corePath', 'modules', '') . $which . '/';
    } else {
        $path = conf_get('contribPath', 'modules', '') . $which . '/';
    }

    return $path;
}

/**
 * I load modules and|or parts of them.
 */
function modules_loader(
    $which,
    $file = 'module.php',
    $boot = true
) {
    static $modulesLoaded = array();

    # Full module path
    $fullPath = modules_getPath($which) . $file;

    grace_debug("Loading module in path: $fullPath");

    # Do not load it again if it was already loaded
    if (in_array($fullPath, $modulesLoaded)) {
        grace_info('Module already loaded, nothing else to do.');
        return true;
    }

    # Store the module in the loaded list
    $modulesLoaded[] = $fullPath;

    # Load the module and the config if one exists
    if (file_exists($fullPath)) {
        grace_info("Including file: $fullPath");
        include_once($fullPath);
          
        # Boot it up!
        if ($boot && $file == 'module.php') {
            if (function_exists($which . "_bootMeUp")) {
                grace_info("Module was booted too");
                call_user_func($which . "_bootMeUp");
            }
        }
        return true;
    } else {
        grace_error("File does not exist");
        return false;
    }
}

/**
 * Load the modules .ini file.
 */
function modules_loadIniFile(
    $module = '' /**< The module name */
) {
    grace_debug('Loadinig a module`s ini file: ' . $module);

    $path = modules_getPath($module) . 'config.ini';
    if (file_exists($path)) {
        $config = parse_ini_file($path);
        return $config;
    }
    grace_error('Unable to locate the ini file');
    return false;
}
