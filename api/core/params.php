<?php

/** @file core/params.php
 * Params tools
 */

/** \addtogroup Core
 *  @{
 */

/**
 * \defgroup Params
 * @{
 */

global $params;

/**
 * Get me a parameter
 */
function params_get(
    $p, //!< The parameter
    $def = false //!< Return value if nothing found
) {
    global $params;

    grace_debug("Getting param: $p with default $def");

    # Get them all
    if ($p === false) {
        return $params;
    }

    if (isset($params[$p]) && trim($params[$p]) != '') {
        $pp = strlen($params[$p]) > 50 ? substr($params[$p], 0, 50) : $params[$p];
        grace_debug("Found param: " . tools_textFilterSafe($pp));
        return $params[$p];
    } else {
        grace_debug("No param found, return default: $def");
        return $def;
    }
}

/**
 * Set parameters, usually sent via the request.
 */
function params_set(
    $p, //!< The param
    $val = false, //!< The value
    $override = false //!< Override the value if exists, default is false
) {
    global $params;

    if (is_array($val)) {
        foreach ($val as $vv => $v) {
            _params_set($vv, $v, $override);
        }
    } else {
        _params_set($p, $val, $override);
    }

    return $params[$p];
}

/**
 * Helper function to actually store the params.
 * DO NOT call directly use params_set()
 */
function _params_set(
    $p, //!< The param
    $val = false, //!< Value, default is false
    $override = false //!< Override the current value, default is false
) {
    global $params;

    if (isset($params[$p]) && $override) {
        $params[$p] = $val;
    } else {
        $params[$p] = $val;
    }
    return $params[$p];
}

/**
 * Verify request and set default values when required
 * @todo Verify possible options for each a|b|c etc...
 */
function params_verifyRequest(
    $keys //!< The keys to check
) {
    foreach ($keys as $key) {
        if (params_get($key["key"], '') === '') {
            grace_warn('Missing param: ' . $key['key']);
            if (isset($key['req']) && $key['req'] == true) {
                // I will stop when I encounter the first missing param and report the err
                tools_errSet(
                    'The param is required I shall die!',
                    'ERROR_BAD_REQUEST'
                    );
                return false;
            } else { # Set the default value
                grace_info('Using default');
                params_set($key['key'], $key['def']);
            }
        }
    }
}

/**
 * Loads the options from the command line
 */
function params_cliLoadOpts($allParams)
{
    grace_debug("Loading in CLI mode");

    # The actual params to be considered
    $params = array();

    $opts = "";
    $longOpts = array();

    # Extract only the params
    foreach ($allParams as $param) {
        if (isset($param['params'])) {
            $params[] = $param['params'];
        }
    }

    # For some reason they are stored in pos 0 of the array
    $params = $params[0];

    foreach ($params as $p) {
        $longOpt = $p['key'];
        $opts .= $p['cli'];
        # Is it mandatory?
        if ($p['req']) {
            $opts .= ":";
            $longOpt .= ":";
        } else {
            $opts .= "::";
            $longOpt .= "::";
        }
        $longOpts[] = $longOpt;
    }

    grace_info("Opts requested: " . $opts);

    # I need the basics in order to start
    $args = getopt($opts, $longOpts);

    # Now, lets extract them :)
    foreach ($params as $p) {
        # Was it sent as longOpt?
        if (isset($args[$p['key']])) {
            params_set($p['key'], $args[$p['key']]);
        } elseif (isset($args[$p['cli']])) {
            params_set($p['key'], $args[$p['cli']]);
        }
        # If it is optional, I will load the default value?
        elseif ($p['req'] == false) {
            params_set($p['key'], $p['def']);
        }
    }

    //params_verifyRequest($params);
}

/**
 * This are the core basic params required to function
 */
function core_getBasicParams()
{
    // @todo todo?
}

/**@}*/
/** @}*/
