<?php

/**
 * Call hooks.
 */
function hooks_call(
    $hook, /* The name of the hook */
    $args /* Array with the arguments */
) {
    grace_debug('Calling hook: ' . $hook);

    $paths = [];
    # Core modules
    # @todo Do this based in the core modules order, at least cala first
    # @todo Indicate if the module is enabled
    $paths[] = conf_get('corePath', 'modules', '/');

    # Contrib modules
    $paths[] = conf_get('contribPath', 'modules', '/');

    foreach ($paths as $path) {
        if ($handle = opendir($path)) {
            while (false !== ($entry = readdir($handle))) {
                if ($entry != "." && $entry != "..") {
                    if (is_dir($path.$entry)) {
                        if (file_exists("$path$entry/hooks.php")) {
                            include_once("$path$entry/hooks.php");
                            if (function_exists($entry.'_'.$hook)) {
                                $results = call_user_func_array($entry.'_'.$hook, $args);
                            }
                        } else {
                            $results = [];
                        }
                    }
                }
            }
            closedir($handle);
        }
    }

    return true;
}

/**
 * Call hooks. V2
 */
function hooks_meUp(
    $hook, /* The name of the hook */
    $args /* Array with the arguments */
) {
    grace_debug('Calling hook: ' . $hook);

    $paths = [];

    # Core modules
    # @todo Do this based in the core modules order, at least cala first
    # @todo Indicate if the module is enabled
    $paths[] = conf_get('corePath', 'modules', '/');

    # Contrib modules
    $paths[] = conf_get('contribPath', 'modules', '/');

    # Get a list of hooks
    $hooks = conf_get('hooks', 'core', []);

    foreach ($hooks as $_hook => $h) {
        if ($hook == $_hook) {
            foreach ($h as $_h) {
                modules_loader($_h, 'hooks.php', false);
                if (function_exists($_h.'_'.$hook)) {
                    $results = call_user_func_array($_h.'_'.$hook, $args);
                }
            }
        }
    }
    return true;
}
