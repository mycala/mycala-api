<?php

/** @file module.php
 *
 * I log stuff in the system. Do not use it for debugging really, that what
 * grace was created for.
 * This is more 'important' information, things like new user logins, hacking
 * attempts (if you can detect them) and other things.
 *
 * @todo Should I be a module? Or just an include?
 *
 * @author Twisted Head
 */

#####################################################################
#
# Logger (Logs activity in the system as per request)
#
#####################################################################

/** \addtogroup Modules
 *  @{
 */
/**
 * \defgroup Logger
 * @{
 */

/**
 * Boot up procedure
 */
function logger_bootMeUp()
{
    //
}

/**
 * Init function
 */
function logger_init()
{

    /*
    $paths = array(
        array(
            'r' => 'logger_get_log',
            'action' => 'logger_get_log',
            'access' => "users_loggedIn",
            'params' => array(
                //array("key" => "", "def" => "", "req" => false)
            )
        ),
      );
    return $paths;
     */
}

/**
 * I log stuff in the system.
 *
 * This is a new implementation, eventually all calls will come to me!
 * @ l an array with details about the log. Not everything is required and you
 * may skip a few and I will use the default values. Look at the $l array and
 * you should be able to figure it out.
 *
 * @param l the log
 */
function logger_l($l)
{
    global $myCala_request, $myCala_requestW, $user;

    $l = array(
        # Name of your module
        'who' => isset($l['who']) ? $l['who'] : 'system',
        # A short but descriptive title
        'title' => isset($l['title']) ? $l['title'] : 'No title (lazy)',
        # Some good information about what happened.
        'text' => isset($l['text']) ? $l['text'] : '',
        # Recomended actions to take
        'recom' => isset($l['recom']) ? $l['recom'] : '',
        # reg: regular, err: errors, inf: information, ale: alerts
        'type' => isset($l['type']) ? $l['type'] : 'reg',
        # 1-10 (10 is the lowest) according to your opinion
        'severity' => isset($l['severity']) ? $l['severity'] : 10,
        # Aditional information that you may want to include.
        'additional' => isset($l['additional']) ? $l['additional'] : '',

        # Do NOT send this information

        # The logged in user
        'idUser' => $user['idUser'],
        # Time of the log
        'timestamp' => time(),
        # The remote ip of the invoking party DO NOT send this information
        'ip' => $_SERVER['REMOTE_ADDR'],
        # The process requested
        'request' => $myCala_requestW,
        # The entire request
        'path' => JSON_encode($myCala_request)
    );

    $q = sprintf(
        'INSERT INTO `cala_logger`
		(`timestamp`,`module`,`title`,`text`,`type`,`idUser`,`severity`,`ip`,
		`additional`,`path`,`request`)
		VALUES(\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\',\'%s\')',
        $l['timestamp'],
        $l['who'],
        $l['title'],
        $l['text'],
        $l['type'],
        $l['idUser'],
        $l['severity'],
        $l['ip'],
        $l['additional'],
        $l['path'],
        $l['request']
    );

    if (db_exec($q)) {
        grace_debug('New log created');
        return true;
    }

    grace_error('Unable to insert log');

    return false;
}
