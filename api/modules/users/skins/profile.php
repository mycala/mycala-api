<div align="center">
	<div style="max-width: 600px" align="left">

		<form class="pure-form" onsubmit="return Cala.usersEdit()" id="form_userEdit">
			<fieldset>

				<div class="cala_formSection">
					<div class="cala_formTitle">
						{users_fullName}
					</div>
					<input type="text" placeholder="{users_fullName}" value="<?= $skin['fullName'] ?>" id="fullName" class="full xlarge" required validate />
				</div>

				<div class="cala_formSection">
					<div class="cala_formTitle">
						{users_userName}
					</div>
					<input type="text" placeholder="{users_userName}" value="<?= $skin['userName'] ?>" id="userName" class="full xlarge" required validate />
				</div>

				<div class="cala_formSection">
					<div class="cala_formTitle">
						{users_email}
					</div>
					<input type="text" placeholder="{users_email}" value="<?= $skin['email'] ?>" id="email" class="full xlarge" required validate style="margin-top: 5px;" />
				</div>

				<div class="cala_formSection">
					<div class="cala_formTitle">
						{users_about}
					</div>
					<textarea id="about" class="full" rows="5" placeholder="{users_about}"><?= $skin['about'] ?></textarea>
				</div>

				<div class="cala_formSection">
					<input type="password" placeholder="Password" id="pwd" class="full xlarge" style="margin-top: 5px;">
					<span class="icon-eye pwdEyes" onclick="return Cala.pwdToggle('pwd');" style="margin-top: -28px;"></span>
				</div>

				<input type="hidden" value="<?= $skin['idUser'] ?>">

				<button class="pure-button primary full" style="font-size: 1.2rem; padding: 7px; margin-top: 5px;">
					<span class="icon-floppy-disk"> </span><span class="buttonText">{users_edit}</span>
				</button>

			</fieldset>
		</form>

	</div>

	<!-- Avatar stuff -->
	<div id="loader_avatarUpload"></div>

	<img src="<?= $skin['avatar']?>" id="avatarView" class="users_avatar rounded" onClick="$('#users_avatarUpload').click()" onError="Cala.usersAvatarErr(this)" avatar-source="<?= $skin['idUser'] ?>" >

	<form action="#" method="post" enctype="multipart/form-data" id="avatarForm" onSubmit="return false;" style="display: none;">
		<input class="pure-button full warning" type="file" name="files[]" id="users_avatarUpload" onChange="Cala.usersAvatarUpload();">
	</form>	
	<!-- //Avatar stuff -->

</div>

<script>
	$(document).ready(function(){
		Cala.usersAvatarsSet();
	})


function users_menuSet(){
	var u = Cala.loginsGet();
	$('#menu_userAvatar').attr('src', Cala.randLink(u.avatar));
	$('#menu_userName').html(u.userName);
}



</script>

