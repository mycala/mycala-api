<?php

include_once('settings.php');

print '<div align="center" style="padding-bottom: 20px; border-bottom: 1px solid #e5e5e5;">';

foreach ($sitesLocal as $site => $s) {
    printf('<button onclick="window.location.href=\'?site=%s&t=l\'">%s</button> | ', $site, $site);
}
print '<hr />';
foreach ($sitesRemote as $site => $s) {
    printf('<button onclick="window.location.href=\'?site=%s&t=r\'">%s</button> | ', $site, $site);
}

print '</div>';

if (!isset($_GET['site'])) {
    die('<h2>I can not work like this.</h2>');
    exit;
}

$menu = 'menu.php?site=' . $_GET['site'] . '&t='.$_GET['t'];
$logs = 'logs.php?last=true&site=' . $_GET['site'] . '&t='.$_GET['t'];

?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<title>Grace Viewer</title>
		<link rel="stylesheet" href="css.css" />
	</head>
	
<body>
	<table style="width: 100%" border=0>
	<tr>
	<td width="20%" style="border-right: 1px solid #e5e5e5;">
		<iframe src="<?= $menu ?>" height="800" width="100%" id="menu" name="menu" frameborder="no"></iframe>
	</td>
	<td>
		<iframe src="<?= $logs ?>" height="800" width="100%" id="logs" name="logs" frameborder="no"></iframe>
	</td>
	</tr>
	</table>

	</body>
</html>
