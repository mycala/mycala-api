<div align="center">
	<div style="max-width: 600px" align="left">

		<!-- Register new account -->
		<form class="pure-form" onsubmit="return Cala.usersRegister()" id="form_userRegister">
			<fieldset>

				{users_fullName}
				<input type="text" placeholder="{users_fullName}" id="fullName" class="full xlarge" required validate />

				{users_email}
				<input type="text" placeholder="{users_email}" id="email" class="full xlarge" required validate style="margin-top: 5px;" />

				{users_password}
				<input type="password" placeholder="Password" id="pwd" class="full xlarge" style="margin-top: 5px;">

				<span class="icon-eye3 pwdEyes" onclick="return Cala.pwdToggle('pwd');"></span>

				<button class="pure-button primary full" style="font-size: 1.2rem; padding: 7px; margin-top: 5px;" id="users_registerButton">
				<span class="icon-user-plus2"> </span><span class="buttonText">{users_joinUs}</span>
				</button>

			</fieldset>
		</form>

		<div id="reg_termsAccept">{users_joinAccept}</div>

		<!-- //Register new account -->
	</div>
</div>
