<div align="center">
	<div style="max-width: 500px;">

		<!-- Login -->
		<div class="rounded calaPages" id="loginLogin">

			<h2>{users_loginTitle}</h2>

			<form class="pure-form" action="" method="POST" onSubmit="return Cala.usersLogin();">
				<fieldset>
					<input type="text" class="full xlarge" placeholder="{users_userNamePwd}" name="userName" id="userName" required validate style="margin-top: 5px;">

					<input type="password" class="full xlarge" placeholder="{users_password}" id="pwd" name="pwd" required validate style="margin-top: 5px;">
					<span class="icon-eye3 pwdEyes" onClick="return Cala.pwdToggle('pwd');" style="margin-top: -28px;"></span>

					<button class='pure-button full primary xlarge' style="margin-top: 5px;" id="users_loginButton">
						<span class="icon-user4"></span> <span class="buttonText">
							{users_loginButton}
						</span>
					</button>

					<input type="hidden" id="w" name="w" value="users_log_me_in">
					<input type="hidden" id="w" name="method" value="not_embed">

				</fieldset>
			</form>

			<hr class="separate"/>

			<a href="?w=users_register_page&ref=login_top" class="pure-button success xlarge" id="userRegisterLogin">
				<span class="icon-user-plus2"></span><span class="buttonText">{users_menuReg}</span>
			</a>
		</div>
		<!-- //Login -->

		<!-- Password -->
		<div id="loginPwdForgot" style="margin-top: 50px;">

			<div id="loginPwdForgot">

				<div class="rounded calaPages">

					<h2>{users_logPwdFg}</h2>
					<form class="pure-form pure-form-stacked" onClick="return false;">
						<fieldset>
							<input type="text" class="full" placeholder="{users_emailRecover}" id="users_userNameRecovery" required validate>
							<div class="help">
								{users_logPwdFgUnHelp}
							</div>
						</fieldset>
					</form>
					<br />
					<button class='pure-button full success' onClick='Cala.usersPwdRecover();'>
						<span class="icon-mail42"></span> <span class="buttonText">{users_logPwdFgButton}</span>
					</button>
				</div>
			</div>
		</div>
		<!-- //Password -->

	</div>
</div>


