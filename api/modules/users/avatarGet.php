<?php

/**
 *  Gets and returns the user avatar.
 *
 *  @todo this is a mess, with the new files module it will change a lot.
 */
function users_avatarGet()
{
    modules_loader('files', 'module.php');

    # Get the details about the person
    $user = users_load(array('userName' => params_get('userName', '')));

    if ($user->avatar == "") {
        $user->avatar = params_get('fall_back', '');
        files_presentFile($user->avatar, false);
    } else {
        # Where are they stored?
        $avatarPath = files_createPath($user->idUser, "avatar");

        # Get the file name
        $q = sprintf("SELECT * FROM files WHERE idFile = '%s'", $user->avatar);
        $avatarDets = db_query($q, 1);

        # Change the name according to the requested size
        $user->avatar = $avatarPath . str_replace("avatar_def", "avatar_def_" . params_get('size', '25'), $avatarDets->name);
        if (!file_exists($user->avatar)) {
            $user->avatar = params_get('fall_back', '');
        }
    }

    files_presentFile($user->avatar, false);
}

/**
 * Upload an avatar.
 *
 * External call
 *
 * @todo Move the avatar resize somewhere else and delete old avatars
 * @todo use private/public system, maybe?
 * @todo use settings
 */
function users_avatarUpload()
{
    global $user;

    grace_debug("Uploading a new avatar");

    # Use files
    modules_loader('files', 'module.php');

    if ($_SERVER['REQUEST_METHOD'] === 'POST') {
        if (isset($_FILES['image'])) {

            # @todo de-hardcode this path 'avatars'
            $path = users_avatarPath($user['idUser']);
            grace_info('The avatar will go to: ' . $path);

            # Get the information about the file
            $fInfo = pathinfo($_FILES['image']['name']);
            $avatarName = $user['idUser'] . '_avatar.';
            $avatarFullPath = $path . $avatarName .'_tmp_'. $fInfo['extension'];

            $upload = _files_upload(
                $avatarFullPath,
                $_FILES['image'],
                'jpg,png,jpeg',
                5,
                true
            );

            if ($upload !== true) {
                return tools_errSet(
                    'Error while trying to upload the avatar',
                    $upload
                    );
            }

            # Resize
            if (tools_useTool('php-image-resize/ImageResize.php')) {
                $image = new \Gumlet\ImageResize($avatarFullPath);
                $image->crop(150, 150);
                $image->save("{$path}{$avatarName}jpg", IMAGETYPE_JPEG);
                unlink($avatarFullPath);
            } else {
                return tools_errSet(
                    'Unable to resize image',
                    'ERR_ERR'
                    );
            }

            # @todo this might not be necessary, I can just look and 'see' if
            # there is an avatar saved.
            # But I would need to convert them always to a standard name
            # including extension.
            # @bug if the new avatar has a different ext than the one before
            # the last does not get deleted.
           // _users_update(array('avatar' => $avatarName));
        }
    }
    return $user['avatar'];
}

/**
 * Get the avatar path.
 *
 * @todo use private/public system, maybe?
 */
function users_avatarPath(
    $idUser, /* id of the user*/
    $public= false /* web (http) or internal (/path/...) */
) {
    if ($public) {
        return conf_get('avatarPublicPath', 'users', '');
    }

    return conf_get('avatarInternalPath', 'users', '');
}
