<?php

/**
 * Add user to role
 */
function _users_rolesAdd(
    $idUser,
    $idRole
) {
    $r = db_exec("INSERT INTO `cala_usersRoles` (idUser, idRole) VALUES($idUser, $idRole)");

    if ($r == 'ERROR_DB_ERROR' || $r == false) {
        return false;
    }

    return true;
}
