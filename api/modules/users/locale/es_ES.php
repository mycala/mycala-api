<?php

global $locale;

$locale['users'] = [

    'loginTitle' => 'Registro/Ingreso',

     'avatar' => 'Avatar',
    'logoutButton' => 'Salir',
    'loginButton' => 'Entrar',
    'logPwdFgUnHelp' => 'Indique su nombre de usuario o correo para obtener una nueva clave temporal.',
    'logPwdFg' => '¿Olvidó su clave?',
    'menuReg' => 'Registrarse',
     'joinUs' => 'Unirse',
     'logPwdFgButton' => 'Enviar clave de recuperación',
    'emailRecover' => 'Correo/Nombre de Usuario',
    'userNamePwd' => 'Correo/Nombre de Usuario',
    'userName' => 'Nombre de Usuario',
    'password' => 'Clave',
    'email' => 'Correo Electrónico',
    'fullName' => 'Nombre Completo',
    'edit' => 'Editar Perfil',
    'about' => 'Acerca de Usted',
    'joinAccept' => 'Al registrarse acepta los Términos y Concidiones así como la Política de Privacidad',
];
