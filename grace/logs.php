<?php

include_once('settings.php');

$log = isset($_GET['log']) ? $_GET['log'] : 'latest';

$connectedTo = "$server?w=cala_grace_log_view&log=$log&cronToken=$token&graceSkip";

$testInstall = "$server?w=cala_test_install&cronToken=$token";

$log = file_get_contents($connectedTo);

$log = JSON_decode($log);

$log = $log->resp;

# Formated log
$fileOut = "<hr />";
$logFormated = '';

if ($log) {
    //$log = str_replace("\n", "<br />", $log);
    //$log = colourMe($log);
    $logFormated = grace_formatLog($log);
    //$fileOut .= $log;
}

/**
 * Format the log
 */
function grace_formatLog($log)
{
    $logFormat = file_get_contents('logFormat.html');

    $formated = '';

    # I need an array
    $log = explode("[-n-]", $log);
     
    $logDateParts = explode('[|]', $log[0]);
    $logDate = date('Y.m.d_G.i.s', $logDateParts[1]);

    //$logParts

    foreach ($log as $l => $ll) {
        $logParts = explode('[|]', $ll);

        $date = date('G.i.s', @$logParts[1]);

        //$date = explode('_', $date);

        $formated .= sprintf(
            $logFormat,
            @colourMe($logParts[0]),
            $date,
            @$logParts[2],
            @colourMe(
                //str_replace("\n", "<br />", $logParts[3])
                $logParts[3]
             )
         );
    }
    //$logParts[3]

    return ['fullLog' => "<table>
		 <tr>
		 <th class='logHeader'>
		 Type
		 </th>
		 <th class='logHeader'>
		 Time
		 </th>
		 <th class='logHeader'>
		 Title
		 </th>
		 <th class='logHeader'>
		 Log
		 </th>
		 </tr>
		 $formated
		 </table>",
                     
        'logDate' => $logDate];
}

function colourMe($text)
{

    # Replies
    $p = '/\[resp\](.*)\[\/resp\]/iU';
    $r = '<textarea class="resp callout warning full">${1}</textarea>';
    $text = preg_replace($p, $r, $text);

    # Cli
    $p = '/\[cli\](.*)\[\/cli\]/iU';
    $r = '<code class="com callout secondary">${1}</code>';
    $text = preg_replace($p, $r, $text);

    # Requested process
    $p = '/\[proc\](.*)\[\/proc\]/iU';
    $r = '<button class="button small proc">${1}</button>';
    $text = preg_replace($p, $r, $text);

    # Requests
    $p = '/\[req\](.*)\[\/req\]/iU';
    $r = '<textarea class="full">${1}</textarea>';
    $text = preg_replace($p, $r, $text);

    # Details
    $p = '/\[dets\](.*)\[\/dets\]/iU';
    $r = '<code class="dets callout primary" style="word-break: break-all;">${1}</code>';
    $text = preg_replace($p, $r, $text);

    # Info
    $p = '/\[info\](.*)\[\/info\]/iU';
    //$r = '<code class="req callout primary"><a href="${1}" target="_blank">${1}</a></code>';
    $r = '<code class="req callout primary">${1}</code>';
    $text = preg_replace($p, $r, $text);

    # Queries
    $p = '/\[query\](.*)\[\/query\]/iU';
    //$r = '<code class="query callout secondary">${1}</code>';
    $r = '<textarea style="width:100%;" rows=10>${1}</textarea>';
    $text = preg_replace($p, $r, $text);

    // $p = '/\[query\](.*)\[\/query\]/iU';
    //$r = '<code class="query callout secondary">${1}</code>';
    //$text = str_replace('\[query\]', '<code class="query callout secondary">', $text);
    //$text = str_replace('[/query]', '</code>', $text);

    # Times
    $p = '/\](.*)@/iU';
    $r = ']<span style="color: brown;">${1}</span> @ ';
    $text = preg_replace($p, $r, $text);

    # Type of message
    $p = '/(\[d\])/i';
    $r = '<span style="color: blue;">${1}</span>';
    $text = preg_replace($p, $r, $text);

    $p = '/(\[e\])/i';
    $r = '<span style="color: red;">${1}</span>';
    $text = preg_replace($p, $r, $text);

    $p = '/(\[i\])/i';
    $r = '<span style="color: green;">${1}</span>';
    $text = preg_replace($p, $r, $text);

    $p = '/(\[w\])/i';
    $r = '<span style="color: yellow;">${1}</span>';
    $text = preg_replace($p, $r, $text);

    return $text;
}
?>

<!DOCTYPE html>
<html>
	 <head>
	 <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="stylesheet" href="css.css" />
	 <title>Grace Viewer</title>
	 </head>
	 <body>
<div id="logsPage">
<div id='connectedTo'>
Connected to: <a href='<?php print $connectedTo ?>' target='_blank'><?php print $connectedTo ?></a>
Test Install: <a href='<?php print $testInstall ?>' target='_blank'>Here</a>
</div>
		<?php print 'Log date: ' . $logFormated['logDate']; ?>
		<?php print $logFormated['fullLog']; ?>
</div>
	</body>
</html>
