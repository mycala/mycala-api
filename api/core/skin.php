<?php
/** @file core/skin.php
 * Skins
 */

/** \addtogroup Core
 *  @{
 */

/**
 * \defgroup Skin
 * @{
 */

global $mainSkin, //!< The main skin variables
$skinMsgs, //!< Messages to present to the user
$user;

# @todo make this configurable
include('form.php');

//! Paths to header file scripts
$mainSkin['header']['fileScriptsPaths'] = [];

//! Paths to footer file scripts
$mainSkin['footer']['fileScriptsPaths'] = [];

//! Header file scripts, DO NOT use this, all file scripts will be placed here later on
$mainSkin['header']['fileScripts'] = '';

//! Header text scripts
$mainSkin['header']['textScripts'] = '';

//! The page description to be used in the dead
$mainSkin['header']['description'] = 'A great website';

//! Page image
$mainSkin['header']['image'] = 'web/files/banner_grande.png';

//! Page title
$mainSkin['title'] = 'Hello world';

//! Footer file scripts, DO NOT use this, all file scripts will be placed here later on
$mainSkin['footer']['fileScripts'] = '';

//! Footer text scripts
$mainSkin['footer']['textScripts'] = '';

//! File Css in array form
$mainSkin['fileCss'] = [];

//! File CSS in list form, ready to be used in the skin, DO NOT use this
$mainSkin['fileCssList'] = '';

//!< Text Css
$mainSkin['textCss'] = '';

//!< Public path to the skin files
$mainSkin['publicPath'] = '';

//!< Public path for files in general
$mainSkin['publicPathFiles'] = conf_get('public', 'files');

//!< Extras to include in the skin, use your module as a pre_
$mainSkin['extras']['core_core'] = '';

# All message types
//! Information messages
$skinMsgs['info'] = '';
//! Error messages
$skinMsgs['err'] = '';
//! Warning messages
$skinMsgs['warning'] = '';

//! Error user is not logged in, this should be in users actually
define('USERS_ERR_NOT_LOGGED_IN', 'USERS_ERR_NOT_LOGGED_IN');

/**
 * This is where a skin looks more like a module, I need a boot function
 * \todo there should be a function by users to get this paths
 */

$mainSkin['user'] = $user;
# Default avatar
$mainSkin['extras']['users_defaultAvatar']  = conf_get('avatarDefault', 'users');
$mainSkin['extras']['users_avatarsPublicPath']  = conf_get('avatarPublicPath', 'users');

skin_scriptAdd(
    'Cala.users_avatar_default = "'.$mainSkin['extras']['users_defaultAvatar'].'";'.
    'Cala.users_paths_avatars = "'.$mainSkin['extras']['users_avatarsPublicPath'].'";',
    'text',
    'footer'
);

# Settings for current skin
include(skin_getCurrentPath() . 'settings.php');

/**
 * I skin you up
 */
function skin_me(
    $what
) {
    global $mainSkin, $user;

    //$skinPath = skin_getCurrentPath();

    # Settings
    //include($skinPath . 'settings.php');

    $mainSkin['publicPath'] = skin_getCurrentPath(true);
    $mainSkin['siteUrl'] = conf_get('thisDomain', 'core', '');

    // Make the current skin available to js
    skin_scriptAdd('Cala.skinCurrentPath = "'.$mainSkin['publicPath'].'"', 'text', 'footer');


    $mainSkin['body'] = $what;

    # Locale
    $mainSkin['locale'] = conf_get('locale', 'core', 'en_UK');

    # Template
    # Use a custom with $mainSkin['template'] = 'my_custom'; // There should be a file called my_custom.php
    $mainSkin['template_tmp'] = 'body';

    # All file scripts to be included
    $mainSkin['fileScripts'] = '';

    # @todo order them?
    $mainSkin['messages'] = skin_messagesGet();

    # Format all scripts
    foreach ($mainSkin['header']['fileScriptsPaths'] as $script) {
        $script .= '?cacheBeGone='.time();
        $mainSkin['header']['fileScripts'] .= "<script src='$script' type='text/javascript'></script>";
    }
    foreach ($mainSkin['footer']['fileScriptsPaths'] as $script) {
        $script .= '?cacheBeGone='.time();
        $mainSkin['footer']['fileScripts'] .= "<script src='$script' type='text/javascript'></script>";
    }

    # Text scripts
    //$mainSkin['footer']['textScripts'] = '<script><!--//--><![CDATA[// ><!--'.$mainSkin['footer']['textScripts'].' //--><!]]></script>';
    $mainSkin['footer']['textScripts'] = "<script type='text/javascript'>{$mainSkin['footer']['textScripts']}</script>";

    # Format all css files
    foreach ($mainSkin['fileCss'] as $css) {
        # Add public path if they are hosted locally
        if (strpos($css, '//') === false) {
            $css = $mainSkin['publicPath'] . $css;
        }
        # Random stuff to avoid cache
        $css .= '?cacheBeGone='.time();
        $mainSkin['fileCssList'] .= "<link rel='stylesheet' href='$css' />";
    }

    # Text css
    $mainSkin['textCss'] = '<style>'. $mainSkin['textCss'] .'</style>';

    # Template
    $mainSkin['template'] = isset($mainSkin['template']) ? $mainSkin['template'] : $mainSkin['template_tmp'];

    $mainSkin['user'] = $user;

    # Header, Body and Footer
    $skinH = skin_this($mainSkin, 'header', false);
    $skinB = skin_this($mainSkin, $mainSkin['template'], false);
    $skinF = skin_this($mainSkin, 'footer', false);

    return $skinH . $skinB . $skinF;
}

/**
 * Set the title of the page.
 */
function skin_setTitle($title)
{
    global $mainSkin;

    $mainSkin['title'] = $title;
}

/**
 * Add scripts.
 */
function skin_scriptAdd(
    $script, //!< The script you want to add, it can be text or a path
    $type = 'file', //!< text|file
    $loc = 'header'//!< Header of footer of the page
) {
    global $mainSkin;

    grace_info('Adding scripts...');

    if ($type == 'text') {
        $mainSkin[$loc]['textScripts'] .= $script;
    } else {
        $mainSkin[$loc]['fileScriptsPaths'][] = $script;
    }
}

/**
 * Add css
 */
function skin_cssAdd(
    $css, /* The css you want to add, it can be text or a path */
    $type = 'file' /* text|file */
) {
    global $mainSkin;

    grace_info('Adding css...');

    if ($type == 'text') {
        $mainSkin['textCss'] .= $css;
    } else {
        $mainSkin['fileCssPaths'][] = $css;
    }
}

/**
 * Generate a response according to an error reply.
 *
 * @todo I should do this. Load templates accordingly. 404, bad request...
 */
function skin_err(
    $err
) {
    return skin_errNotFound();
}

/**
 * Generate a 404 error response
 */
function skin_errNotFound()
{
    return  skin_load('', '404');
}

/**
 * Skin 404 not found, this will actually take you to the login page
 */
function skin_errLogin()
{
    tools_goto('users_login_page');
}

/**
 * Parse stuff in the files.
 */
function skin_template()
{
}

/**
 * Load a skin.
 */
function skin_load(
    $skin,
    $file, /* The file of the skin */
    $skinName = false /* Any in particular? or use default, if you suply your own it must be a full path is file */
) {
    if (!$skinName) {
        $skinName = conf_get('skin', 'core');
        $skinPath = conf_get('skinPath', 'core') . $skinName . '/' . $file.'.php';
    } else {
        $skinPath = $file.'.php';
    }

    grace_debug('Loading skin: ' . $skinPath);

    if (file_exists($skinPath)) {
        ob_start();
        include $skinPath;
        return ob_get_clean();
    } else {
        grace_error('Unable to load skin: ' . $skinPath);
        $skin = '';
    }

    return $skin;
}

/**
 * Skin specific content.
 * @todo use the module name to load the file
 * @todo conf translate yes|no
 */
function skin_this(
    $c, /* The content to skin, an array with values */
    $file, /* File with the skin, full path with trailing slash */
    $module = 'custom' /* For translations */
) {
    global $locale, $mainSkin;
    grace_debug('Skining content');

    # Add the skin variables, just to have them
    $c['skin'] = $mainSkin;
    $c = skin_load($c, $file, $module);

    # Translate
    locale_trans($module, $c);

    return $c;
}

/**
 * Sets messages that are going to be presented to the user.
 */
function skin_messagesSet(
    $msg,
    $type = 'info'
) {
    global $skinMsgs;

    $skinMsgs[$type] .= sprintf('<div class="cala_userMsg_%s">%s</div>', $type, $msg);
}

/**
 * Gets messages to be presented to the user.
 */
function skin_messagesGet($type = false)
{
    global $skinMsgs;

    if (!$type) {
        return $skinMsgs;
    } else {
        return $skinMsgs[$type];
    }
}

/**
 * Get current skin path
 */
function skin_getCurrentPath(
    $public = false
) {
    if ($public) {
        return conf_get('skinPublicPath', 'core') . conf_get('skin', 'core') . '/';
    } else {
        return conf_get('skinPath', 'core') . conf_get('skin', 'core') . '/';
    }
}

/** @} */
/** @} */
