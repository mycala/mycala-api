<?php

/** @ingroup Constants
 *  @{
 */

/**
 * You may configure Grace to print just some types of messages, just set those that you want to true.
 * Also, you may shut Grace completely by setting GRACE_PRINT_ALL = no
 * Do all the combinations that you want.
 *
 */

# Details about how I am going to be running
# This can not be set in the settings file since it is loaded before Grace

//! Is Grace alive? Set to false to stop EVERYTHING
define('GRACE_ALIVE', true);

//! Print stuff as you go, it can be a lot!
define('GRACE_PRINT_SCREEN', false);
//! Print absurd messages
define('GRACE_PRINT_ABSURD', true);
//! Print debug messages
define('GRACE_PRINT_DEBUG', true);
//! Print error messages
define('GRACE_PRINT_ERROR', true);
//! Print warning messages
define('GRACE_PRINT_WARN', true);
//! Print info messages
define('GRACE_PRINT_INFO', true);

/** @} */
/** @ingroup GlobalVars
 *  @{
 */
//! Debug messages stored in Grace
global $grace_logMsgs;
global $totalQueries;
global $filePath;
global $fileName;
global $buffer;

$buffer = '';

/** @} */

/**
 * I am the one who actually talks.
 *
 * DO NOT call me directly, use the other functions.
 */
function _grace_talk($msg, $type = 'i')
{
    global $buffer;
    static $mode;

    # Imprimir si no es debug y se está en cli
    if ($type != 'd') {
        if (!isset($mode)) {
            $mode = conf_get('mode', 'core', $mode);
        }
        if ($mode == 'cli') {
            print $msg . "\n";
        }
    }

    if (!is_array($msg)) {
        $msg = array('', $msg);
    }


    # In some cases grace can be too much, like when refreshing to
    # get new messages every 5 seconds.
    if (
         isset($_GET['graceSkip']) ||
        isset($_POST['graceSkip'])
     ) {
        //if (isset($_GET['graceSkip'])) {
        return false;
    } elseif (GRACE_ALIVE) {

        # Format the message
        $msg = sprintf(
            "[%s][|]%s[|]%s[|]%s[-n-]\n",
            $type,
            time(),
            $msg[0],
            $msg[1]
         );

        # The graceOnScreen is a special param to print things while loading
        # a page via url
        # HANDLE WITH CARE!
        # You need to provide a token for this to work.
        # You MUST set a token here, you need to actually come here and enable
        # this funcionality.
        if (GRACE_PRINT_SCREEN && isset($_GET['graceOnScreen'])) {
            print $msg . ($type == 'a' ? "" : "\n" . "<br />");
        }

        # Lets hold the messages until I have full name for the log
        if (conf_get('buffer', 'grace', false) === true) {
            $buffer .= $msg;
            return true;
        }

        # If you also want to store the messages, set a valid path
        if (conf_get('logs', 'grace', false)) {
            grace_writeLog($msg);
        }
    }
}

/**
 * I create the paths for the logs
 */
function grace_getPaths()
{
    global $myCala_requestW;

    static $fileName = '';

    $filePath = conf_get('logPath', 'grace', '');

    # I need a unique name so logs from different calls do not get mixed up
    # testing an md5 of a random thing
    if ($fileName == '') {
        $fileName = sprintf(
            '%s_(%s)_[%s]_%s.log',
            conf_get('mode', 'core', 'web'),
            date('ymd_H:i:s', time()),
            $myCala_requestW,
            md5(rand(6, 900)*time())
        );
    }

    return array('filePath' => $filePath, 'fileName' => $fileName);
}

/**
 * I write to logs.
 */
function grace_writeLog(
    $msg
) {

    # This should not have to be in two places, but for now, it will do
    if (isset($_GET['graceSkip']) || isset($_POST['graceSkip'])) {
        return false;
    }

    $paths = grace_getPaths();

    if ($paths['filePath'] != '') {
        if (!file_exists($paths['filePath'])) {
            mkdir($paths['filePath'], 0777, true);
            touch($paths['filePath'] . $paths['fileName']);
        }
    }
    # Store using php's error system
    error_log($msg, 3, $paths['filePath'] . $paths['fileName']);
}

/**
 * Call me so I can start storing the messages in the actual file.
 */
function grace_talkStart()
{
    global $buffer;

    conf_set('buffer', 'grace', false);

    if (conf_get('logs', 'grace', false)) {
        grace_writeLog($buffer);
    }
}

/**
 * Call me so I can stop storing the messages in the actual file.
 */
function grace_talkStop()
{
    global $buffer;

    conf_set('buffer', 'grace', true);
}

/**
 * Debuging messages
 */
function grace_debug($msg)
{
    if (GRACE_PRINT_DEBUG == true) {
        _grace_talk($msg, 'd');
    }
}

/**
 * Informational messages
 */
function grace_info($msg)
{
    if (GRACE_PRINT_INFO == true) {
        _grace_talk($msg, 'i');
    }
}

/**
 * Error messages
 */
function grace_error($msg)
{
    if (GRACE_PRINT_ERROR == true) {
        _grace_talk($msg, 'e');
    }
}

/**
 * Warnings
 */
function grace_warn($msg)
{
    if (GRACE_PRINT_WARN == true) {
        _grace_talk($msg, 'w');
    }
}

/**
 * Grace log
 */
function grace_log($msg, $who)
{
    if (GRACE_LOG == true) {
        _grace_talk($msg, 'l');
    }
}

/**
 * Query counter
 */
function grace_queries($w = 'add')
{
    global $totalQueries;

    if ($w === 'add') {
        $totalQueries++;
    } else {
        return $totalQueries;
    }
}

/**
 * Handle PHP`s error messages.
 *
 * Temporary solution.
 * https://www.php.net/manual/en/function.set-error-handler.php
 */
function grace_php_errors(
    int $errno,
    string $errstr,
    string $errfile,
    int $errline
) {
    $filePath = conf_get('phpErrDir', 'core', '');

    if ($filePath != '') {
        $msg = sprintf(
            'Errno: [%s] file [%s] line [%s] ==> %s ',
            $errno,
            $errfile,
            $errline,
            $errstr
        );

        # Store using php's error system
        error_log("$msg\n", 3, $filePath . 'php_err.log');
    }

    return false;
}
