I should really make a good readme page.

V: 1.1

# 'Invoke' MyCala

The main way to use MyCala is probably as a REST Api, you make calls and get a
JSON response, but you can also use it via CLI (not very advanced at the moment)
or to generate plain text results.

## Rest

Just go like this:
`https://localhost/mycala/?w=cala_hello`

You should see an error message 'ERROR_BAD_REQUEST', that is fine!

Now try this:
`https://localhost/mycala/?w=cala_hello&test=aTEst`

Now you should see some results in the screen.

`test` is a required parameter for this call, you MUST send something.

`https://localhost/mycala/?w=cala_hello&default=ThisIsDefault&test=aTEst`

Now you should see some results in the screen with your custom default value.
`default` is an optional parameter for this call. You may send something or you
may leave it blank and MyCala will assign a `default` value.

You will learn more abouth them when you read the module development information.

## Change the reply type

`https://localhost/mycala/?w=cala_hello&default=ThisIsDefault&test=aTEst&replyType=plain`

Not all calls handle this correctly, it really depends on the request. Some are
ment to reply only in JSON form.

Again, you will learn more in module develpment depending on your needs.

