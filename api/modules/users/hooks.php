<?php

/**
 * hook_testInstall.php
 */
function users_testInstall(&$tests){

# Find a good cost for password encryption

	$timeTarget = 0.05; // 50 milliseconds

	$cost = 8;

	do {
		$cost++;
		$start = microtime(true);
		password_hash("test", PASSWORD_BCRYPT, ["cost" => $cost]);
		$end = microtime(true);
	} while (($end - $start) < $timeTarget);

	$allTests['usersHashcost'] = array(
		'name' => 'PHP Hash function appropriate cost',
		'result' => conf_get('hash_cost', 'core', '10') > $cost ? false : true,
		'good' => 'The appropriate cost for password hashing is: ' . $cost . ' yours is ' . conf_get('hash_cost', 'core', '10'),
		'fail' => 'The appropriate cost for password hashing is: ' . $cost . ' yours is ' . conf_get('hash_cost', 'core', '10'),
	'extraMsg' => 'This value will usually change a bit if run the test again, do it and try to adjust accordingly.'
	);

	$tests['users'] = array(
		'config' => modules_loadIniFile('users'),
		'tests' => $allTests
	);

}
