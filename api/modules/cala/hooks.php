<?php

/**
 * hook_testInstall();
 */
function cala_testInstall(&$tests)
{

    #
    # Lets run the tests
    #

    # Core installation
    $allTests['coreInstall'] = array(
        // The name of the test
        'name' => 'Core Installation',
        // The result of the test
        'result' => true,
        // Message if good
        'good' => 'Your core installation is in: ' . conf_get("coreInstall", "modules", "/"),
        // Message if failed
        'fail' => ''
    );

    # PHP Version
    if (version_compare(PHP_VERSION, '7.1.0') >= 0) {
        $phpVersion = true;
    } else {
        $phpVersion = false;
    }

    $allTests['phpVersion'] = array(
        'name' => 'PHP Version',
        'result' => $phpVersion === true,
        'good' => 'I am at least PHP version 7.1.0, my version is: ' . PHP_VERSION,
        'fail' => 'You need at least PHP version 7.1.0, my version is:' . PHP_VERSION,
    );

    # Database connection
    $dbConn = db_allGood();
    $allTests['dbConn'] = array(
        'name' => 'Database connection',
        'result' => $dbConn === true,
        'good' => 'Your database seems to be working fantastic.',
        'fail' => 'I can not see your database, please check your connection settings.'
    );

    # Grace
    $allTests['grace'] = array(
        'name' => 'Grace (Debugging)',
        'result' => true,
        'good' => 'Grace handless debugging information. Is is currently: ' . (conf_get('logs', 'grace') ? 'on' : 'off'),
        'fail' => ''
    );

    $allTests['graceLogsPath'] = array(
        'name' => 'Grace logs path',
        'result' => is_writable(conf_get('logPath', 'grace')),
        'good' => 'Grace can write logs, if enabled here: ' . conf_get('logPath', 'grace'),
        'fail' => 'Grace is not able to write your logs, if enabled here: ' . conf_get('logPath', 'grace')
    );

    # Tmp
    $allTests['coreTmpPath'] = array(
        'name' => 'Tmp directory',
        'result' => is_writable(conf_get('tmpDir', 'core')),
        'good' => 'I have a tmp directory and I can write to it: ' . conf_get('tmpDir', 'core'),
        'fail' => 'I need a place to write tmp files such as error logs and other things, make this dir accesible with perms 0777:' . conf_get('tmpDir', 'core')
    );

    # PHP`s Error log (direcotry)
    $allTests['phpErrLog'] = array(
        'name' => 'PHP error log',
        'result' => is_writable(conf_get('phpErrDir', 'core')),
        'good' => 'I have a directory to write PHP`s error log: ' . conf_get('phpErrDir', 'core', ''),
        'fail' => 'I need a place to write PHP`s error log, make this dir accesible with perms 0777:' . conf_get('phpErrDir', 'core', '')
    );

    # PHP`s Error log (file)
    # Sometimes it may exist and has different permissions
    $errLogFile = conf_get('phpErrDir', 'core', '') . 'php_err.log';
    if (!file_exists($errLogFile)) {
        $resErrLogFile = true;
    } else {
        if (is_writable($errLogFile)) {
            $resErrLogFile = true;
        } else {
            $resErrLogFile = false;
        }
    }
    $allTests['phpErrFile'] = array(
        'name' => 'PHP error log (file)',
        'result' => $resErrLogFile,
        'good' => 'I can not write the actual error log, maybe it exists: ' . $errLogFile,
        'fail' => 'I can not write the error log, maybe it exists and I do not have permission to write to it, make it 0777:' . $errLogFile
    );

    # Files path
    $filesPath = conf_get('dir', 'files', '/');
    $filesGood = is_writable($filesPath);
    $allTests['filesGood'] = array(
        'name' => 'Files storage',
        'result' => $filesGood,
        'good' => 'Your files path is perfect.',
        'fail' => 'Your files storage was not found or the path is not accesible by me: ' . $filesPath
    );

    # File perms
    $filesPerms = @substr(sprintf('%o', fileperms($filesPath)), -4);
    $filesPermsGood = $filesPerms == '0777';
    $allTests['filesPermsGood'] = array(
        'name' => 'Files storage permissions',
        'result' => $filesPermsGood,
        'good' => 'Your file perms look good and secure.',
        'fail' => sprintf(
            'Remember to put your files in a NON WEB ACCESSIBLE path and to secure its permissions,
			it is best if they are only writable/readable by the web process which is usually www-root. Current perms are "%s".
	{_br_} They should be: 0644?
	{_br_} That does not seem to be very secure?',
            $filesPerms
    )
);

    # Public files path
    $publicFilesPath = conf_get('public', 'files', '/');
    $publicFilesGood = is_writable($publicFilesPath);
    $allTests['publicFilesGood'] = array(
        'name' => 'Public files location',
        'result' => $publicFilesGood,
        'good' => 'Your public files path is perfect: ' . $publicFilesPath,
        'fail' => 'Your public files path was not found or the path is not accesible by me: ' . $publicFilesPath,
        'extraMsg' => 'You may ignore this error if you are not going to be hosting public files'
    );

    # Contrib modules
    $contribPath = conf_get('contribPath', 'modules', '/');
    $contribGood = is_dir($contribPath);
    $allTests['contribGood'] = array(
        'name' => 'Contributed modules',
        'result' => $contribGood,
        'good' => 'The contributed modules path is good: ' . $contribPath,
        'fail' => 'Your contrib modules where not found or the path is not accessible: ' . $contribPath
    );

    # Resources
    $resourcesPath = conf_get('resourcesPath', 'core', '/');
    $resourcesGood = is_dir($resourcesPath);
    $allTests['resourcesGood'] = array(
        'name' => 'Resources path',
        'result' => $resourcesGood,
        'good' => 'The resources path looks good: ' . $resourcesPath,
        'fail' => 'Your resources path was not found or the path is not accessible: ' . $resourcesPath
    );

    # Cron token
    $cronToken = conf_get('cronToken', 'core', 'ItIsGoodIfThisIsBigAndHasW3irDLeeT3rsAnd$ymb0lz.IniT') == 'ItIsGoodIfThisIsBigAndHasW3irDLeeT3rsAnd$ymb0lz.IniT';
    $contribGood = is_dir($contribPath);
    $allTests['cronToken'] = array(
        'name' => 'Security Token',
        'result' => $contribGood,
        'good' => 'Your cronToken seems to be safe. Is the one you used to access this page.',
        'fail' => 'You really need to change your token! '
    );

    $tests['cala'] = array(
        'config' => modules_loadIniFile('cala'),
        'tests' => $allTests
    );

    // @todo add: list of core modules, database information, print_error_info, os
    // @todo add 'libraries', grace is there already
}
