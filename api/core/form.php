<?php
/** @file core/forms.php
 * Forms
 * Process forms
 */

/** \addtogroup Core
 *  @{
 */

/**
 * \defgroup Forms
 * @{
 */

/** \addtogroup Constants
 *  @{
 */
//!< Form was not submitted
define('FORM_SUBMITTED_NOT', 'no');
//!< Form was submitted as new
define('FORM_SUBMITTED_NEW', 'new');
//!< Form was submitted to edit
define('FORM_SUBMITTED_EDIT', 'edit');
//!< Form was submitted and it was good
define('FORM_SUBMITTED_GOOD', 'good');
//!< Form was submitted with errors
define('FORM_SUBMITTED_ERR', 'err');

/** @} */

/**
 * Get/parse a form.
 *
 * \sa _form_formName_get
 */
function forms_get(
    $form, /* An array with the form information */
    $skin = false,  /* The skin file to use for the form, if you want to */
    $module = false /* Locale, required if you will skin it */
) {
    grace_debug('Parse a form');

    $form['fields']['_formStatus'] = ['type' => 'hidden', 'value' => FORM_SUBMITTED_NEW, 'ignore' => true];

    # Add values if they come
    if (isset($form['values']) && $form['values']) {
        _forms_valuesAdd($form);
    }

    # Let's always check
    $form = _forms_check($form);

    if ($form['submitted'] == FORM_SUBMITTED_GOOD) {
        if (_forms_storeDb($form)) {

            # You ay also do something after the form was inserted
            if (function_exists($form['form']['id'].'_inserted')) {
                grace_info('Default form inserted function exists');
                $form = call_user_func_array($form['form']['id'].'_inserted', array($form));
            }

            //tools_goto(isset($form['form']['goOnSuccess']) ? $form['form']['goOnSuccess'] : '');
        }
    }

    # Ajax call and form submitted, no need for the rest
    if ($form['submitted'] == FORM_SUBMITTED_GOOD
        || $form['submitted'] == FORM_SUBMITTED_ERR
    ) {
        if (isset($form['form']['ajax'])) {
            params_set('replyType', 'json');
            if ($form['submitted'] == FORM_SUBMITTED_GOOD) {
                //if (count($form['errors']) == 0) {
                tools_reply('ALL_GOOD', true);
            } else {
                tools_errSet(
                    'Errors in the form',
                    JSON_encode($form['errors'])
                );
            }
        }
    } else {
        # Any messages
        if (isset($form['errors']) && count($form['errors']) > 0) {
            foreach ($form['errors'] as $err) {
                skin_messagesSet('Err: ' . $err, 'info');
            }
        }
    }

    $formParsed = [];

    $formParsed['head'] = _forms_parseHead($form['form']);

    foreach ($form['fields'] as $fieldName => $f) {

        # Some fields need to be defined even if not used to avoid PHP warnings
        # and even using undeclared variables.
        # @todo some fields will need to be defined on a per case basis
        $f['class'] = isset($f['class']) ? $f['class'] : '';
        $f['attributes'] = isset($f['attributes']) ? $f['attributes'] : '';
        $f['value'] = isset($f['value']) ? $f['value'] : '';
        $f['placeholder'] = isset($f['placeholder']) ? $f['placeholder'] : '';

        $f['class'] = $f['class'];
        $f['value'] = $f['value'];
        $f['id']    = $form['form']['id'] .'_'.$fieldName;
        $f['name']  = $f['id'];

        # Required
        if (isset($f['required'])) {
            $f['class'] .= ' cala_form_field_required';
            $f['attributes'] .= ' validate required';
        }
        if ($f['type'] == 'input') {
            $formParsed[$fieldName] = _forms_parseInput($f);
        }
        if ($f['type'] == 'submit') {
            $formParsed[$fieldName] = _forms_parseSubmit($f);
        } elseif ($f['type'] == 'textarea') {
            $formParsed[$fieldName] = _forms_parseTextArea($f);
        } elseif ($f['type'] == 'select') {
            $formParsed[$fieldName] = _forms_parseSelect($f);
        } elseif ($f['type'] == 'hidden') {
            $formParsed[$fieldName] = _forms_parseHidden($f);
        }
    }

    if ($skin) {
        return skin_this($formParsed, $skin, $module);
    }

    return $formParsed;
}

/**
 * Checks a form.
 */
function _forms_check(
    $form
) {
    grace_debug('Checking a form: ' . $form['form']['id']);

    # Store the information sent in a variable where I do not have to worry about later if it
    # is GET || POST
    $formSent = $form['form']['method'] == 'POST' ? $_POST : $_GET;

    # If the form was not submited, then, who cares?
    if (!isset($formSent['form'])
        || $formSent['form'] != $form['form']['id']) {
        $form['submitted'] = FORM_SUBMITTED_NOT;
        return $form;
    }
   
    # Editing a form?
    $form['status'] = FORM_SUBMITTED_NEW;
    if ($formSent['_formStatus'] == FORM_SUBMITTED_EDIT) {
        $form['status'] = FORM_SUBMITTED_EDIT;
    }

    # Lets assume it was submited good
    $form['submitted'] = FORM_SUBMITTED_GOOD;
    $form['errors'] = [];

    # @todo add custom validators
    foreach ($form['fields'] as $field => $f) {

        # Required fields
        if (isset($f['required']) && $f['required'] == true) {

            # Not sent, add an error mark
            if (!isset($formSent[$field])
                || $formSent[$field] == '') {
                $form['submitted'] = FORM_SUBMITTED_ERR;
                $form['fields'][$field]['errors'] = true;
                $form['fields'][$field]['class'] = isset($form['fields'][$field]['class']) ? $form['fields'][$field]['class'] . ' cala_form_err' : 'cala_form_err';
                $form['errors']['required'][] = $field;
            } else {
                # It was sent, put the value
                $form['fields'][$field]['value'] = $formSent[$field];
            }
        } else {
            # Not required, I will just add the value sent or default
            $form['fields'][$field]['value'] = isset($formSent[$field]) ? $formSent[$field] : $form['fields'][$field]['value'];
        }
    }

    # You may also check your form
    if (function_exists($form['form']['id'].'_check')) {
        grace_info('Default form check function exists');
        $form = call_user_func_array($form['form']['id'].'_check', array($form));
    }

    return $form;
}

/**
 * Add values to a form
 */
function _forms_valuesAdd(
    &$form
) {
    grace_debug('Add values to a form');

    foreach ($form['fields'] as $fieldName => $field) {
        if (isset($form['values'][$fieldName])) {
            $form['fields'][$fieldName]['value'] = $form['values'][$fieldName];
        }
    }
     
    $form['fields']['_formStatus']['value'] = FORM_SUBMITTED_EDIT;
}

/**
 * Helper function to parse an input field.
 */
function _forms_parseInput(
    $field
) {
    return sprintf(
        '<input type="text" name="%s" id="%s" maxsize="%s" class="%s" value="%s" placeholder="%s" %s>',
        $field['name'],
        $field['id'],
        isset($field['maxSize']) ? $field['maxSize'] : 100,
        isset($field['class']) ? $field['class'] : '',
        $field['value'],
        $field['placeholder'],
        $field['attributes']
    );
}

/**
 * Helper function to parse an submit
 */
function _forms_parseSubmit(
    $field
) {
    return sprintf(
        '<input type="submit" name="%s" id="%s" value="%s" class="%s" %s>',
        $field['name'],
        $field['id'],
        isset($field['value']) ? $field['value'] : 'Submit',
        $field['class'],
        $field['attributes']
    );
}

/**
 * Helper function to parse a text area.
 */
function _forms_parseTextArea(
    $field
) {
    return sprintf(
        '<textarea name="%s" id="%s" style="%s" class="%s" rows="%s" %s>%s</textarea>',
        $field['name'],
        $field['id'],
        isset($field['style']) ? $field['style'] : '',
        $field['class'],
        isset($field['rows']) ? $field['rows'] : '3',
        $field['attributes'],
        $field['value']
    );
}

/**
 * Helper function to parse a select
 */
function _forms_parseSelect(
    $field
) {
    $options = '';

    if (isset($field['options'])) {
        foreach ($field['options'] as $option) {
            $o = explode('|', $option);
            $selected = (isset($field['value']) && $field['value'] == $o[0]) ? 'selected' : '';
            $options .= "<option value='{$o[0]}' $selected>{$o[1]}</option>";
        }
    }

    return sprintf(
        '<select name="%s" id="%s" style="%s" class="%s" %s>%s</select>',
        $field['name'],
        $field['id'],
        isset($field['style']) ? $field['style'] : '',
        $field['class'],
        $field['attributes'],
        $options
    );
}

/**
 * Helper function to parse a hidden field
 */
function _forms_parseHidden(
    $field
) {
    return sprintf(
        '<input type="hidden" name="%s" id="%s" value="%s" %s>',
        $field['name'],
        $field['id'],
        $field['value'],
        $field['attributes']
    );
}

/**
 * Parse the header of the form.
 */
function _forms_parseHead(
    $header
) {
    return sprintf(
        '<form action="%s" method="%s" id="%s" class="%s" %s>',
        $header['action'],
        isset($header['method']) ? $header['method'] : 'POST',
        $header['id'],
        isset($header['class']) ? $header['class'] : '',
        isset($header['ajax']) ? 'onSubmit="return Cala.formSubmit(\''.$header['id'].'\')"' : ''
    );
}

/**
 * Submit the form.
 */
function _forms_storeDb(
    &$form
) {
    grace_debug('Storing form in db as: ' . $form['status']);

    # Edit a form
    if ($form['status'] == FORM_SUBMITTED_EDIT) {
        # Prepare the query
        $fields = [];
        $idColumn = $form['form']['idColumn'];
        $where = "`$idColumn` = '{$form['fields'][$idColumn]['value']}'";

        foreach ($form['fields'] as $name => $field) {
            # Not all fields go in the database
            # @todo add more exceptions or just put the ones that actually go in it
            if ($field['type'] != 'submit'
                 && !isset($field['ignore'])
             ) {
                $fields[] = '`'.$name.'` =  \''._forms_fieldFixValue($field).'\'';
            }
        }

        $q = sprintf(
            'UPDATE `%s` SET %s WHERE %s',
            $form['form']['table'],
            implode(',', $fields),
            $where
         );
    } else {

         # Prepare the query
        $fields = [];
        $values = [];

        foreach ($form['fields'] as $name => $field) {
            # Not all fields go in the database
            # @todo add more exceptions or just put the ones that actually go in it
            if ($field['type'] != 'submit'
                 && !isset($field['ignore'])
             ) {
                $fields[] = '`'.$name.'`';
                $values[] = '\''.db_escape($field['value']).'\'';
            }
        }

        $q = sprintf(
            'INSERT INTO `%s` (%s) VALUES(%s)',
            $form['form']['table'],
            implode(',', $fields),
            implode(',', $values)
         );
    }
     
    $r = db_exec($q);
     
    if (_db_queryGood($r)) {
        return true;
    }

    return false;
}

/**
 * Fix field's value
 */
function _forms_fieldFixValue(
    $field
) {
    # TMP
    $tags = '<br>';

    return tools_stripTags($field['value'], $tags, true);
}

/** @} */
/** @} */
