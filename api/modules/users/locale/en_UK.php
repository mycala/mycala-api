<?php

global $locale;

$locale['users'] = [

    'loginTitle' => 'Login/Register',

     'avatar' => 'Avatar',
    'logoutButton' => 'Logout',
    'loginButton' => 'Login/Join',
    'logPwdFgUnHelp' => 'Indicate your user name or your email to get a new temporary password.',
    'logPwdFg' => 'Forgot Your Password?',
     'menuReg' => 'Register/Join',
     'joinUs' => 'Join Us',
    'logPwdFgButton' => 'Send Recovery Password',
    'emailRecover' => 'Email or Username',
    'userNamePwd' => 'User Name or Email',
    'userName' => 'User Name',
    'password' => 'Password',
    'email' => 'Email',
    'fullName' => 'Your Full Name',
    'edit' => 'Edit Profile',
    'about' => 'About You',
    'joinAccept' => 'By joining us you agree to our Terms of Service and Privacy Policy',
];
