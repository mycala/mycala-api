<?php

/**
 * Generate a new temporary password for recovery.
 */
function users_recoverPwd()
{

    # This call probably won't have the user via iam, so, I will load it
    $userName = params_get('userName', '');
   
    $user = users_load($userName);

    if ($user['idUser'] == 0) {
        return tools_errSet(
            'User does not seem to exist',
            'ERROR_BAD_REQUEST'
         );
    }

    # Use mailer
    tools_loadLibrary('mailer.php');

    # Generate a new temporary password
    $user['pwd'] = tools_secretGenerate();
    grace_debug('New tmp pwd: ' . $user['pwd']);

    # Update account
    if (_users_update($user)) {
        # Send email
        grace_debug('I will send the email');
        $resp = mailer_sendEmail(array(
            'to' => $user['email'],
            'subject' => 'New password recovery - ' . conf_get('siteName', 'core', 'My site'),
            'replyTo' => 'no-repy@' . conf_get('domain', 'core', 'example.net'),
            'message' => 'Your new password is: ' . $user['pwd']
        ));
        if ($resp == true) {
            return 'ALL_GOOD';
        }
    }

    # If I reached this place there was an error
    return tools_errSet(
        'Unknown error',
        'ERR_ERR'
     );
}
